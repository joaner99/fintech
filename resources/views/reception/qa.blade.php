@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>問與答</h3>
						<p>Ｑ＆Ａ</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>							
						<li class="active">{{__('message.FrequentlyAskedQuestions')}}</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
			<div class="about-section container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>常見問與答</h3>
						<p>自己印，把記憶、創意，變成具紀念性的玩意兒。</p>
						<img src="images/section-seprator.png" alt="section-seprator" />
					</div><!-- Section Header /- -->
					
                    
                    <div class="col-md-12 col-sm-12 col-xs-12 description">
                    @foreach($problems as $problemss)
						<h5>{{$problemss->title}} <span class="glyphicon glyphicon-search"></span></h5>
						<p>
						{!!$problemss->body!!}
                        </p>
                        <br>
                        <br>
                    @endforeach
                        
                     
					</div>
                    
                    	
                    
                    
                    
					
				</div><!-- Container /- -->
			</div><!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop