@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>訂單完成</h3>
						<p>Order completed</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">訂單完成</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
			<div class="about-section container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>訂單完成</h3>
						<p>感謝您的訂購</p>
						<img src="images/section-seprator.png" alt="section-seprator" />
					</div><!-- Section Header /- -->
					<div class="col-md-6 col-xs-8 col-md-offset-3 col-xs-offset-2">
						<img src="images/cus_ok.jpg" alt="ok" />
					</div>
					<div class="col-md-6 col-xs-8 col-md-offset-3 col-xs-offset-2">
						<div class="about-content">
							<h5>[  <span>您的訂單{{$data['no']}}已經完成</span> ]</h5>
                            <p>你會在信箱"{{Auth::user()->email}}"收到一封訂單確認信，謝謝您！ </p>


						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop