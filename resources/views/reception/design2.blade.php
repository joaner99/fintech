@extends('layouts.reception.reception')
@section('content')
	<main>
		<!-- Page Banner -->
		<div class="page-banner container-fluid no-padding">
			<!-- Container -->
			<div class="container">
				<div class="banner-content">
					<h3>訂做紙膠帶</h3>
					<p>format</p>
				</div>
				<ol class="breadcrumb">
					<li><a href="index.html" title="Home">首頁</a></li>							
					<li class="active">訂做紙膠帶</li>
                    <li class="active">訂做紙膠帶</li>
				</ol>
			</div><!-- Container /- -->
		</div><!-- Page Banner /- -->
		<div id="main-container">
          	<h3 id="clothing">訂做紙膠帶</h3>
          	<div id="clothing-designer" class="fpd-container fpd-shadow-2 fpd-sidebar fpd-tabs fpd-tabs-side fpd-top-actions-centered fpd-bottom-actions-centered fpd-views-inside-left">
          		<?php $height=172;$y=0?>
          		@if($size=="")
					<?php $width=300; $x=0 ;?>
          		@elseif($size=="-2")
					<?php $width=660;$x=0;?>
          		@elseif($size=="-3")
					<?php $width=960;$x=0; ?>
				@elseif($size=="4")
					<?php $width=920;$x=0;$height=780; $size="-4";$y=0;?>
          		@endif
          		<div class="fpd-product" title="Shirt Front"  >
	    			<img src="images/1{{$size}}.png" title="Base" data-parameters='{"boundingBoxMode":"limitModify",
	    			"autoCenter":true ,"boundingBox":{"x":{{$x}},"y":{{$y}},"width":{{$width}},"height":{{$height}} }}' />
				</div>
		  	</div>
		  	<br />

		  	<p class="fpd-container">
			  	<span class="fpd-btn" id="save-image-php">預覽</span>
		  	</p>

    	</div>	
			
		</main>
@stop
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">

    <!-- The CSS for the plugin itself - required -->
	<link rel="stylesheet" type="text/css" href="{{asset("css/FancyProductDesigner-all.min.cs")}}s" />

@stop
@section('script')

    <!-- Include js files -->
	<script src="{{asset('js/jquery-ui.min.js')}}" type="text/javascript"></script>

	<!-- HTML5 canvas library - required -->
	<script src="{{asset("js/fabric.min.js")}}" type="text/javascript"></script>
	<!-- The plugin itself - required -->
    <script src="{{asset('js/FancyProductDesigner-all.min.js')}}" type="text/javascript"></script>
 <script type="text/javascript">
    jQuery(document).ready(function(){

    	var $yourDesigner = $('#clothing-designer'),
    		pluginOpts = {
	    		stageWidth: {{$width}},
	    		stageHeight: {{$height}},
	    		boundingBoxColor:"#AA0000",
	    		boundingBoxProps :{strokeWidth: 5},
	    		fonts: [
			    	{name: 'Helvetica'},
			    	{name: 'Times New Roman'},
			    	{name: 'Pacifico', url: 'Enter_URL_To_Pacifico'},
			    	{name: 'Arial'},
			    	{name: '微軟正黑體'},
		    		{name: 'Lobster', url: 'google'}
		    	],
	    		customTextParameters: {
		    		colors: "#fff",
		    		removable: true,
		    		resizable: true,
		    		draggable: true,
		    		rotatable: true,
		    		autoCenter: true,
		    		boundingBox: "Base",
		    		boundingBoxMode:"clipping",
		    		zChangeable :true,
		    	},
	    		customImageParameters: {
		    		draggable: true,
		    		removable: true,
		    		resizable: true,
		    		rotatable: true,
		    		colors: '#000',
		    		autoCenter: true,
		    		boundingBox: "Base",	
		    		boundingBoxMode:"clipping",
		    		maxH:1500,
	    			maxW:1500,
	    			minH:50,
	    			minW:50,
	    			zChangeable :true,
		    	},
		    	actions:  {
					'top': ['download','print', 'snap', 'preview-lightbox'],
					'right': ['magnify-glass', 'zoom', 'reset-product', 'qr-code', 'ruler'],
					'bottom': ['undo','redo'],
				}
    		},
    	yourDesigner = new FancyProductDesigner($yourDesigner, pluginOpts);
    	//print button
		$('#print-button').click(function(){
			yourDesigner.print();
			return false;
		});

		//create an image
		$('#image-button').click(function(){
			var image = yourDesigner.createImage();
			return false;
		});

		//checkout button with getProduct()
		$('#checkout-button').click(function(){
			var product = yourDesigner.getProduct();
			console.log(product);
			return false;
		});

		//event handler when the price is changing
		$yourDesigner.on('priceChange', function(evt, price, currentPrice) {
			$('#thsirt-price').text(currentPrice);
		});

		//save image on webserver
		$('#save-image-php').click(function() {

			yourDesigner.getProductDataURL(function(dataURL) {
				token = '{{csrf_token()}}';
				$.post("{{route('save_image')}}",{ base64_image: dataURL,_token: token},function(result){
			    document.location.href="{{route('review')}}?size={{$size}}";
			  });
			});


		});

		//send image via mail
		$('#send-image-mail-php').click(function() {

			yourDesigner.getProductDataURL(function(dataURL) {
				$.post( "php/send_image_via_mail.php", { base64_image: dataURL} );
			});

		});

    });
</script>
@stop