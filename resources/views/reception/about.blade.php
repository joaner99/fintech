@extends('layouts.reception.reception')
@section('content')
<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>關於我們</h3>
						<p>About us</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>							
						<li class="active">{{__('message.AboutUs')}}</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
			<div class="about-section container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<!-- Section Header -->
					<div class="section-header">
						<h3>關於自己印</h3>
						<p>自己印，把記憶、創意，變成具紀念性的玩意兒。</p>
						<img src="images/section-seprator.png" alt="section-seprator" />
					</div><!-- Section Header /- -->
					<div class="col-md-6 col-sm-6 col-xs-6">
						<img src="{{asset('upload/')}}/{{$companies->file1}}" alt="about" />
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<div class="about-content">
							<h5>[  <span>{{$companies->title1}}</span> ]</h5>
                          {!!$companies->body1!!}
							<h5>[  <span>{{$companies->title2}}</span> ]</h5>
							<ul>
								{!!$companies->body2!!}
							</ul>
						</div>
					</div>
				</div><!-- Container /- -->
			</div><!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>	
@stop