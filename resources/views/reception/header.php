<header class="header-section container-fluid no-padding">
			<!-- Top Header -->
			<div class="top-header container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="col-md-7 col-sm-7 col-xs-7 dropdown-bar">
						<h5>自己印・紙膠帶</h5>
						<div class="language-dropdown dropdown">
							<button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" title="languages" id="language" type="button" class="btn dropdown-toggle">English <span class="caret"></span></button>
							<ul class="dropdown-menu no-padding">
								<li><a title="chinese" href="#">中文</a></li>
								<li><a title="English" href="#">English</a></li>
								<li><a title="Japan" href="#">日本語</a></li>
							</ul>
						</div>						
					</div>
					<!-- Social -->
					<div class="col-md-5 col-sm-5 col-xs-5 header-social"> 
						<ul>
							<li>Tel：04-24739873</li>
						</ul>
					</div><!-- Social /- -->
				</div><!-- Container /- -->
			</div><!-- Top Header /- -->
			
			<!-- Menu Block -->
			<div class="container-fluid no-padding menu-block">
				<!-- Container -->
				<div class="container">
					<!-- nav -->
					<nav class="navbar navbar-default ow-navigation">
						<div class="navbar-header">
							<button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a href="index.php"><img src="images/logo.jpg" alt="logo"></a>
						</div>
						<!-- Menu Icon -->
						<div class="menu-icon">
							
							<ul class="cart">							
				
								<li><a href="login.php" title="User"><i class="icon icon-User"></i></a></li>
                                <li><a href="https://www.facebook.com/urtape/" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
							</ul>
						</div><!-- Menu Icon /- -->
						<div class="navbar-collapse collapse navbar-right" id="navbar">
							<ul class="nav navbar-nav">
								<li class="active dropdown">
									<a href="index.php" title="Home" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">首頁</a>
									<i class="ddl-switch fa fa-angle-down"></i>
									
								</li>
								<li class="dropdown">
								<a href="login.php" title="Blog" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">會員專區</a>
									<i class="ddl-switch fa fa-angle-down"></i>
									<ul class="dropdown-menu">		
                                        <li><a href="login.php" title="Blog Post">會員登入</a></li>
										<li><a href="m_record.php" title="Blog Post">查詢訂做紀錄</a></li>
										<li><a href="m_info.php " title="Blog Post">變更會員資料</a></li>
										<li><a href="m_change.php" title="Blog Post">變更密碼</a></li>
									</ul></li>
								<li class="dropdown">
									<a href="about.php" title="Home" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">關於我們</a>
									<i class="ddl-switch fa fa-angle-down"></i>
									
								</li>
								
								<li class="dropdown">
									<a href="shop.php" title="Shop" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">產品展示</a>
									<i class="ddl-switch fa fa-angle-down"></i>
									<ul class="dropdown-menu">				
										<li><a href="shop.php" title="Shop Single">紙膠帶</a></li>
										<li><a href="shop.php" title="Shop Single">明信片</a></li>
										<li><a href="shop.php" title="Shop Single">貼紙</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a href="#" title="customized" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">訂做紙膠帶</a>
									<i class="ddl-switch fa fa-angle-down"></i>
									<ul class="dropdown-menu">				
										<li><a href="#" title="customized">訂做紙膠帶</a></li>
                                        <li><a href="#" title="customized">訂做明信片</a></li>
                                        <li><a href="#" title="customized">訂做貼紙</a></li>
										<li><a href="#" title="process">製作流程</a></li>
									</ul>
								</li>
								<li><a href="qa.php" title="QA">常見問與答</a></li>
								<li><a href="contact-us.php" title="Contact Us">聯絡我們</a></li>
								
							</ul>
						</div><!--/.nav-collapse -->
					</nav><!-- nav /- -->
					<!-- Search Box -->
					<div class="search-box">
						<span><i class="icon_close"></i></span>
						<form><input type="text" class="form-control" placeholder="Enter a keyword and press enter..." /></form>
					</div><!-- Search Box /- -->
				</div><!-- Container /- -->
			</div><!-- Menu Block /- -->
		</header>