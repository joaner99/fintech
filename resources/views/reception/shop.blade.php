@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>商品介紹</h3>
						<p>Products</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">商品介紹</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- Products /- -->
            			<div id="product-section" class="product-section product-section1 container-fluid no-padding">
				<!-- Container -->
				    <div class="container">
                    <!-- Row -->
                    <div class="row">
                        <!-- Section Header -->
                        <div class="section-header">
                            <h3>自己印・產品展示</h3>
                            <p>不再是卡通圖案或者是花紋文字，而是具有紀念性的意義照片，那該有多特別。</p>
                            <img src="{{asset('images/section-seprator.png')}}" alt="section-seprator" />
                        </div><!-- Section Header /- -->
                        <ul id="filters" class="products-categories no-left-padding">
                            @if($id==0)
                                <li><a data-filter="*" class="ProdClasses active" href="#">全部產品</a></li>
                                @foreach($ProdClasses as $key =>$val)
                                <li><a data-filter=".class{{$val->id}}" class="ProdClasses " href="#">{{$val->name}}</a></li>
                            @endforeach
                            @endif
                            @foreach($ProdClasses as $key =>$val)
                                @if($id==$val->id)
                                <li><a data-filter=".class{{$val->id}}" class="ProdClasses " href="#">{{$val->name}}</a></li>
                                @endif
                            @endforeach
                        </ul>
                        <!-- Products -->
                        <ul class="products">
                            <!-- Product -->
                            @foreach($Prod as $key =>$val)
                            <li class="product class{{$val->class_id}}">
                                <a href="{{asset('shop_single')}}/{{$val->id}}">
                                    <img  style="width: 270px;height: 300px" src="{{asset('upload/'.$val->file1)}}" alt="Product" />
                                    <h5>{{strip_tags($val->ext)}}</h5>
                                    <span class="price">{{$val->class_name}}</span>
                                </a>
                                <a href="{{asset('shop_single')}}/{{$val->id}}" class="add-to-cart1" title="Add To Cart">{{$val->name}}</a>
                                <div class="wishlist-box">
                                    
                                    <a href="{{asset('shop_single')}}/{{$val->id}}"><i class="fa fa-search"></i></a>
                                </div>
                            </li><!-- Product /- -->
                            @endforeach
                        </ul><!-- Products /- -->
                    </div><!-- Row /- -->
                    
                </div><!-- Container /- -->
			</div><!-- Products /- -->
            
            
			
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop