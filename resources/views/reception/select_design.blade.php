@extends('layouts.reception.reception')
@section('content')
<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>產品材質</h3>
						<p>Material</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">產品材質</li>
                      
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
				<div id="product-section" class="product-section product-section1 container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<!-- Row -->
					<div class="row">
						<!-- Section Header -->
						<div class="section-header">
							<h3>選擇想要的材質</h3>
							<p>訂做您的獨一無二</p>
							<img src="{{asset('images/section-seprator.png')}}" alt="section-seprator" />
						</div><!-- Section Header /- -->
						
						<!-- Products -->
						<ul class="products">
							<!-- Product -->
							@foreach($Prod as $key =>$val)
							<li class="product design">
								<a href="{{ route('design',$id)}}">
									<img  style="width: 270px;height: 300px" src="upload/{{$val->file1}}" alt="Product" />
									<h5>{{strip_tags($val->name)}}</h5>
									<span class="price">{{strip_tags($val->ext)}}</span>
								</a>
								<a href="{{ route('design',$id)}}" class="add-to-cart1" title="Add To Cart">開始訂做</a>
							
							</li><!-- Product /- -->
                            @endforeach
						</ul><!-- Products /- -->
					</div><!-- Row /- -->
					
				</div><!-- Container /- -->
			</div><!-- Products /- -->
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
		
              
            
@stop