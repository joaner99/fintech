@extends('layouts.reception.reception')
@section('content')
            <!-- Slider Section 1 -->
            <div id="home-revslider" class="slider-section container-fluid no-padding">
                <!-- START REVOLUTION SLIDER 5.0 -->
                <div class="rev_slider_wrapper">
                    <div id="home-slider1" class="rev_slider" data-version="5.0">
                        <ul>
                            @foreach($IndexBanner as $IndexBanners)
                            <li data-transition="zoomout" data-slotamount="default"  data-easein="easeInOut" data-easeout="easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7">
                                <img src="upload/{{$IndexBanners->file1}}" alt="slider" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                <div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-layer-1" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-200','-130','-110','-80']" 
                                    data-fontsize="['38','30','25','16']"
                                    data-lineheight="['24','24','24','24']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="y:-50px;opacity:0;s:1000;e:Power4.easeOut;" 
                                    data-transform_out="opacity:0;s:3000;e:Power4.easeIn;s:3000;e:Power4.easeIn;"
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"  
                                    data-start="1000" 
                                    data-splitin="chars" 
                                    data-splitout="none" 
                                    data-responsive_offset="on"
                                    data-elementdelay="0.05" 
                                    style="z-index: 5; white-space: nowrap; letter-spacing: 3.04px; color:#333; font-weight: 700; font-family: 'Montserrat', sans-serif; text-transform: uppercase;">{{$IndexBanners->title1}}
                                </div>
                            
                                <div class="tp-caption NotGeneric-Title tp-resizeme rs-parallaxlevel-0" id="slide-layer-3" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-110','-50','-35','-30']" 
                                    data-fontsize="['18','18','18','14']"
                                    data-lineheight="['26','26','26','26']"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_in="x:[105%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                    data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                    data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                    data-start="1000" 
                                    data-splitin="chars" 
                                    data-splitout="none" 
                                    data-responsive_offset="on"
                                    data-elementdelay="0.05" 
                                    style="z-index: 5; white-space: nowrap; letter-spacing: 1.5px; color:#777; font-weight: normal; font-family: 'Lato', sans-serif;">{{$IndexBanners->title2}}
                                </div>
                                <div class="tp-caption NotGeneric-Button rev-btn  rs-parallaxlevel-0" id="slide-layer-4" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['-30','20','40','30']" 
                                    data-fontsize="['14','14','14','12']"
                                    data-lineheight="['24','24','24','24']"
                                    data-width="none"
                                    data-height="none"
                                    data-whitespace="nowrap"
                                    data-transform_idle="o:1;"
                                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:100;e:Power1.easeInOut;"
                                    data-style_hover="c:rgb(255, 255, 255);bg:rgba(182, 121, 95);"
                                    data-transform_in="x:[-100%];z:0;rX:0deg;rY:0deg;rZ:0deg;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2500;e:Power3.easeInOut;" 
                                    data-transform_out="auto:auto;s:1000;e:Power2.easeInOut;" 
                                    data-start="2000" 
                                    data-splitin="none" 
                                    data-splitout="none" 
                                    data-actions='[{"event":"click","action":"scrollbelow","offset":"0px"}]'
                                    data-responsive_offset="on" 
                                    data-responsive="off"
                                    style="z-index: 10; padding:8px 38px; letter-spacing:0.56px; color: #b6795f; border-color: #b6795f; font-family: 'Montserrat', sans-serif; text-transform:uppercase; background-color:transparent; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">馬上動手做
                                </div>
                            </li>
                            @endforeach 
                        </ul>
                    </div><!-- END REVOLUTION SLIDER -->
                </div><!-- END OF SLIDER WRAPPER -->
                <div class="goto-next"><a href="#category-section"><i class="icon icon-Mouse bounce"></i></a></div>
            </div><!-- Slider Section 1 /- -->
            
            <!-- Services Section -->
            <div class="services-section container-fluid">
                <!-- Container -->
                <div class="container">
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="srv-box">
                            <a href="{{asset('select_design')}}">
                            <i class="icon icon-Goto"></i><h5>設計自己的紙膠帶</h5><i class="icon icon-Dollars"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="srv-box">
                            <a href="{{asset('select_design2')}}">
                            <i class="icon icon-Goto"></i><h5>設計自己的明信片</h5><i class="icon icon-Dollars"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="srv-box">
                            <a href="{{asset('select_design3')}}">
                            <i class="icon icon-Goto"></i><h5>設計自己的貼紙</h5><i class="icon icon-Dollars"></i>
                            </a>
                        </div>
                    </div>
                    
                </div><!-- Container /- -->
            </div><!-- Services Section /- -->
            
    
            
            <!-- Product Section -->
                <div id="product-section" class="product-section product-section1 container-fluid no-padding">
                <!-- Container -->
                <div class="container">
                    <!-- Row -->
                    <div class="row">
                        <!-- Section Header -->
                        <div class="section-header">
                            <h3>自己印・產品展示</h3>
                            <p>不再是卡通圖案或者是花紋文字，而是具有紀念性的意義照片，那該有多特別。</p>
                            <img src="images/section-seprator.png" alt="section-seprator" />
                        </div><!-- Section Header /- -->
                        <ul id="filters" class="products-categories no-left-padding">
                            <li><a data-filter="*" class="ProdClasses active" href="#">全部產品</a></li>
                            @foreach($ProdClasses as $key =>$val)
                            <li><a data-filter=".class{{$val->id}}" class="ProdClasses href="#">{{$val->name}}</a></li>
                            @endforeach
                           
                            
                        </ul>
                        <!-- Products -->
                        <ul class="products">
                            <!-- Product -->
                            @foreach($Prod as $key =>$val)
                            <li class="product class{{$val->class_id}}">
                                <a href="{{asset('shop_single')}}/{{$val->id}}">
                                    <img  style="width: 270px;height: 300px" src="upload/{{$val->file1}}" alt="Product" />
                                    <h5>{{strip_tags($val->ext)}}</h5>
                                    <span class="price">{{$val->class_name}}</span>
                                </a>
                                <a href="{{asset('shop_single')}}/{{$val->id}}" class="add-to-cart1" title="Add To Cart">{{$val->name}}</a>
                                <div class="wishlist-box">
                                    
                                    <a href="{{asset('shop_single')}}/{{$val->id}}"><i class="fa fa-search"></i></a>
                                </div>
                            </li>
                            @endforeach
                        </ul><!-- Products /- -->
                    </div><!-- Row /- -->
                    
                </div><!-- Container /- -->
            </div>
        <!-- Latest Blog /- -->
@stop
@section('script')
<script type="text/javascript">
    $('.ProdClasses').click(function(){
        $('.ProdClasses').removeClass('active');
        $(this).addClass('active');
    });
</script>

@stop