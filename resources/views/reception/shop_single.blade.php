@extends('layouts.reception.reception')
@section('content')
<main>
            <!-- Page Banner -->
            <div class="page-banner container-fluid no-padding">
                <!-- Container -->
                <div class="container">
                    <div class="banner-content">
                        <h3>商品介紹-紙膠帶</h3>
                        <p>Products</p>
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="index.html" title="Home">首頁</a></li>                           
                        <li class="active">商品介紹</li>
                            
                        <li class="active">{{$Prod->class_name}}</li>
                    </ol>
                </div><!-- Container /- -->
            </div><!-- Page Banner /- -->
            
            <!-- Products /- -->
            
                            <div class="shop-single container-fluid no-padding">
                <!-- Container -->
                <div class="container">
                    <div class="product-views">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="carousel-item">
                                <div class="item">
                                    <img src="{{asset('upload/'.$Prod->file1)}}" alt="product" />
                                </div>
                                
                                <div class="item">
                                    <img src="{{asset('upload/'.$Prod->file2)}}" alt="product" />
                                </div>
                                
                                <div class="item">
                                    <img src="{{asset('upload/'.$Prod->file3)}}" alt="product" />
                                </div>
                            </div>
                        </div>
                        <!-- Entry Summary -->
                        <div class="col-md-6 col-sm-6 col-xs-12 entry-summary">
                            <h3 class="product_title">{{$Prod->name}}</h3>
                            <div class="product-rating">
                                <div class="star-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </div>
                            
                            </div>
                            <p class="stock in-stock"><span>數量：</span> {{$Prod->amount}}
                            <div>
                                {!!$Prod->body!!}
                            </div>
                            
                            <form>
                                
                                
                                <button title="Add To Cart" class="add_to_cart">自己印.紙膠帶</button>
                            </form>
                        
                        </div><!-- Entry Summary /- -->
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 description">
                        <h5>注意事項</h5>
                        <p>1. 要使相片紙膠帶可呈現出最好的效果，需注意下面4點:<br>
a. 建議材質：亮面日本和紙(厚度75um)，寬度選擇2.5cm，編輯長度10cm約可放4-5張相片。<br>
b. 盡量找橫式照片，整體亮度偏亮為佳。<br>
c. 解析度愈高越好，至少要達96DPI以上。<br>
d. 盡量是獨照或人數少的合照，不建議團體照。<br>
2. 上傳一張照片後，需調整照片上下至粉紅色框邊，再上傳其他張照片進行左右的編輯。<br>
3. 編輯時也能添加文字在相片上，每張照片之間盡量不要留白色空白。<br>
4. 照片內想要印出的部分(如人臉或其他重點)，必須調整大小在紅色虛線的出血線之內，這樣成品才不會被裁切。<br>
5. 客戶所要製作圖片版權皆為客戶所有，自己印非經客戶同意不得重製圖片。若客戶提供圖片有侵害版權的行為，本公司將不負任何法律責任。<br></p>
                    </div>
                    <!-- Product Section -->
                    <div class="product-section container-fluid no-padding">
                        <!-- Section Header -->
                        <div class="section-header">
                            <h3>相關產品</h3>
                            <p>我們的願景是成為地球上以客戶為中心的公司</p>
                            <img src="{{asset('images/section-seprator.png')}}" alt="section-seprator" />
                        </div><!-- Section Header /- -->
                        <!-- Products -->
                        <ul class="products">
                            <!-- Product -->
                            @for($i=0; $i < 4 ; $i++)
                            <li class="product">
                                <a href="{{asset('shop_single')}}/{{$Prod_demo[$i]->id}}">
                                    <img style="width: 270px;height: 300px" src="{{asset('upload/'.$Prod_demo[$i]->file1)}}" alt="Product" />
                                    <h5>{{$Prod_demo[$i]->name}}</h5>
                                    
                                </a>
                                <div class="wishlist-box">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="{{asset('shop_single')}}/{{$Prod_demo[$i]->id}}" class="addto-cart" title="Add To Cart">{{$Prod_demo[$i]->name}}</a>
                            </li><!-- Product /- -->
                            @endfor
                        </ul><!-- Products /- -->
                    </div><!-- Product Section /- -->
                    <nav class="ow-pagination">
                        <ul class="pager">
                            <li class="number"><a href="#">4</a></li>
                            <li class="load-more"><a href="#">觀看更多</a></li>
                            <li class="previous"><a href="#"><i class="fa fa-angle-right"></i></a></li>
                            <li class="next"><a href="#"><i class="fa fa-angle-left"></i></a></li>
                        </ul>
                    </nav>
                </div><!-- Container /- -->
            </div><!-- Products /- -->
            
            
            
            
            <!-- Team Section -->
        <!-- Team Section -->
            
            <!-- Testimonial Section -->
    <!-- Testimonial Section /- -->
            
            <!-- Clients -->
            <!-- Clients /- -->
        </main>
@stop