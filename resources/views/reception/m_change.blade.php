@extends('layouts.reception.reception')
@section('content')
<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>變更密碼</h3>
						<p>change</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>							
						<li class="active">{{__('message.SignIn')}}</li>
                        <li class="active">{{__('message.ResetPassword')}}</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<!-- Checkout -->
			<div class="container no-left-padding no-right-padding woocommerce-checkout" >
				<!-- Container -->
				<div class="container">
					<!-- Login -->
					<div class="col-md-8 col-md-offset-2 login-block">
						<div class="login-check">
							<h3>會員密碼變更</h3>
							<div class="col-md-8 col-md-offset-2 login-form">
								<form action="{{route('m_change_update')}}" method="POST" class="form-horizontal">									{{ csrf_field() }}
									<div class="form-group">
										<input name='old_password' class="form-control" placeholder="舊密碼*" type="text" required="">
									</div>
									<div class="form-group">
										<input name='password' class="form-control" placeholder="新密碼*" type="text" required="">
									</div>
                                    <div class="form-group">
										<input name='password2' class="form-control" placeholder="新密碼確認*" type="text" required="">
									</div>
                                    @if($errors->any())
										<h4>{{$errors->first()}}</h4>
									@endif
									<input value="確定更改密碼" type="submit">
									
								</form>
							</div>
							
						</div>
					</div><!-- Login /- -->
					
			
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
            
            
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
		
@stop