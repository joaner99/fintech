@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>產品規格</h3>
						<p>format</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">產品材質</li>
                        <li class="active">產品規格</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<!-- Checkout -->
					<div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
				<!-- Container -->
                        <div class="section-header">
							<h3>選擇想要的規格</h3>
							<p>訂做您的獨一無二</p>
							<img src="{{asset('images/section-seprator.png')}}" alt="section-seprator" />
						</div><!-- Section Header /- -->
				<div class="container">
					<!-- Cart Table -->
					<div class="col-md-12 col-sm-12 col-xs-12 cart-table">
							<table class="table table-bordered table-responsive">
								<thead>
									<tr>
										<th class="product-thumbnail">材質</th>
										<th class="product-subtotal">尺寸</th>
										<th class="product-quantity">數量/金額</th>
										<th class="product-unit-price">編輯器長度</th>
                                        <th class="product-unit-price">編輯</th>
										
										
									</tr>
								</thead>
								<tbody>
									@foreach($Prod as $Prods)
									<form action="{{route('add_cars')}}" method="POST" class="form-horizontal">
									{{ csrf_field() }}
									<tr class="cart_item">
										<input type="hidden" name="id" value="{{$Prods->id}}">
										<input type="hidden" name="ext" value="{!!$Prods->ext!!}">
										<input type="hidden" name="name" value="{!!$Prods->name!!}">
										<td data-title="Item" class="product-thumbnail"><a href="#">{{$Prods->name}}</a></td>
										
										<td data-title="Quantity" class="product-quantity">{!!$Prods->ext!!}  </td>
                                        <td data-title="Total" class="product-subtotal"><div class="col-md-12 form-group">
										
										<div class="select">
											<select class="form-control" name="amount">
												<option value="1,{!!$Prods->price1!!}">【1捲】{!!$Prods->price1!!}元</option>
												<option value="2,{!!$Prods->price2!!}">【2捲】{!!$Prods->price2!!}元</option>
												<option value="3,{!!$Prods->price3!!}">【3捲】{!!$Prods->price3!!}元</option>
												<option value="4,{!!$Prods->price4!!}">【4捲】{!!$Prods->price4!!}元</option>
												<option value="5,{!!$Prods->price5!!}">【5捲】{!!$Prods->price5!!}元</option>
												<option value="10,{!!$Prods->price6!!}">【10捲】{!!$Prods->price6!!}元</option>
											</select>
										</div>
									</div></td>
										<td data-title="Remove" class="product-remove"><div class="col-md-12 form-group">
										
										<div class="select">
											<select class="form-control" name="size">
												@if($id==1)
												<option value="1">10cm</option>
												<option value="2">20cm</option>
												<option value="3">30cm</option>
												@else
												<option value="4">明信片尺寸</option>
												@endif
											</select>
										</div>
									</div></td>
                                        
                                        <td data-title="Item" class="action"  colspan="6"> <input type="submit" title="Continue shopping" value="開始編輯"></td>
									</tr>
									</form>
									@endforeach
								</tbody>
							</table>
					</div><!-- Cart Table /- -->
					
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
		</main>

		
              
            
@stop