@extends('layouts.reception.reception')
@section('content')
			<!-- Page Banner -->
	<div class="page-banner container-fluid no-padding">
		<!-- Container -->
		<div class="container">
			<div class="banner-content">
				<h3>會員登入</h3>
				<p>Member Login</p>
			</div>
			<ol class="breadcrumb">
				<li><a href="index.html" title="Home">首頁</a></li>							
				<li class="active">會員登入</li>
			</ol>
		</div><!-- Container /- -->
	</div><!-- Page Banner /- -->
	
	<!-- About Section -->

    	<!-- Checkout -->
	<div class="container no-left-padding no-right-padding woocommerce-checkout" >
		<!-- Container -->
		<div class="container">
			<!-- Login -->
			<div class="col-md-8 col-md-offset-2 login-block">
				<div class="login-check">
					<h3>會員登入</h3>
					<div class="col-md-8 col-md-offset-2 login-form">
						<form action="login" mathod="POST" class="form-horizontal" >
							<div class="form-group">
								<input name="email" class="form-control" placeholder="Email 信箱*" type="text">
							</div>
							<div class="form-group">
								<input name="password" class="form-control" placeholder="Password 密碼*" type="password">
							</div>
                            <input value="FACEBOOK登入" type="submit">
							<input value="登入" type="submit">
							<a href="#" title="Forgot Password?">忘記密碼？</a>
						</form>
					</div>
					
				</div>
			</div><!-- Login /- -->
		</div><!-- Container /- -->
	</div><!-- Checkout /- -->
@stop
@section('script')
<script type="text/javascript">
</script>
@stop            
            
      