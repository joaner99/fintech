@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>聯絡我們</h3>
						<p>Contact us</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>							
						<li class="active">{{__('message.ContactUs')}}</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<div class="contact-us container-fluid no-padding">
				<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
					<div class="contact-detail">
						<!-- Section Header -->
						<div class="section-header">
							<h3>自己印聯絡資訊</h3>
							<p>客製化紙膠帶，少量或大量印製皆可。</p>
						</div><!-- Section Header /- -->
						<div class="contact-info">
							<i class="icon icon-Pointer"></i>
							<br>
							{!!$contactu_datas->name!!}
						</div>
						<div class="contact-info">
							<i class="icon icon-Phone2"></i>
							<a href="tel:{{$contactu_datas->tel}}" title="Phone" class="phone">{{$contactu_datas->tel}}</a>
						</div>
						<div class="contact-info">
							<i class="icon icon-Phone2"></i>
							<a href="mailto:{{$contactu_datas->email}}" title="urtapeprint@gmail.com">{{$contactu_datas->email}}</a>
						</div>
						<ul class="social">
							
						</ul>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 no-padding">
					<div class="form-detail">
						<!-- Section Header -->
						<div class="section-header">
							<h3>聯絡自己印・紙膠帶</h3>
							<p>＊為必填項目</p>
							@if($errors->any())
								<h4>{{$errors->first()}}</h4>
							@endif
						</div><!-- Section Header /- -->
						<form action="{{asset('contanct_post')}}" method="POST" class="form-horizontal">			
						{{ csrf_field() }}
							<div class="col-md-6 col-sm-6 col-xs-12 form-group">
								<input name="name" type="text" name="contact-name" class="form-control" id="input_name" placeholder="{{__('message.yourname')}} *" required/>
							</div>
							<div class="col-md-6 col-sm-6 col-xs-12 form-group">
								<input name="email" type="text" name="contact-email" class="form-control" id="input_email" placeholder="Email *" required/>
							</div>
                            <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                           
										<div class="select">
											<select name="type" class="form-control">
												<option>{{__('message.MakePayment-ATMTransfer')}}</option>
												<option>{{__('message.MassProduction')}}</option>
												<option>{{__('message.CustomerServices')}}</option>
												<option>{{__('message.CommentsCooperation')}}</option>
											</select>
										</div></div>
                            
							<div class="col-md-12 col-sm-12 col-xs-12 form-group">
								<input  type="text" required="" placeholder="{{__('message.Subjust')}} *" id="input_subject" class="form-control" name="theme">
							</div>
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<textarea name="body" placeholder=" {{__('message.Enquiry')}}. . ." id="textarea_message" name="contact-message" rows="5" class="form-control"></textarea>
							</div>
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<button title="Submit" type="submit" id="btn_submit" name="post">{{__('message.Send')}}</button>
							</div>
							<div id="alert-msg" class="alert-msg"></div>
						</form>
					</div>
				</div>
                <div class="form-group col-md-12 col-sm-12 col-xs-12">
								<br>
							</div>
				<!-- Map Section -->
				<div >
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3640.5568318538935!2d120.6601853153081!3d24.152196879224082!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34693d9c3a0e6677%3A0x3c603245709d1337!2z6Ieq5bex5Y2wIOe0meiGoOW4tg!5e0!3m2!1szh-TW!2stw!4v1533626987280" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div><!--  Map Section /- -->
			</div><!-- Contact Us /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop