@extends('layouts.reception.reception')
@section('content')
		<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>預覽紙貼紙</h3>
						<p>Preview</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">預覽紙貼紙</li>
                      
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Cart Table -->
					<div class="col-md-12 col-sm-12 col-xs-12 cart-table">
						<form>
							<table class="table table-bordered table-responsive">
								<thead>
									<tr>
										
										<th class="product-name">預覽</th>
										
									</tr>
								</thead>
								<tbody>
										<tr class="cart_item">
										@if($size==3||$size==4)
										<td data-title="Product Name" class="product-name">
                                            <img style="border:0px; padding:0px;" src="upload/images/product-{{$key}}.png" alt="Product" />
                                         </td>
                                         @elseif($size==2)
										<td data-title="Product Name" class="product-name">
                                            <img style="border:0px; padding:0px;" src="upload/images/product-{{$key}}.png" alt="Product" /><img style="border:0px; padding:0px;" src="upload/images/product-{{$key}}.png" alt="Product" />
                                         </td>
                                         @elseif($size==1)
                                         	<td data-title="Product Name" class="product-name">
                                         	<img style="border:0px; padding:0px;" src="upload/images/product-{{$key}}.png" alt="Product" /><img style="border:0px; padding:0px;" src="upload/images/product-{{$key}}.png" alt="Product" /><img style="border:0px; padding:0px;" src="upload/images/product-{{$key}}.png" alt="Product" />
                                            </td>
                                         @endif
									</tr>
									<tr>
										<td class="action" colspan="6">
											<a href="{{ route('design2')}}" title="Continue shopping">上一頁修改</a>
                                            <a href="{{ route('list_cars')}}" title="Continue shopping">前往結帳</a>
											
									</tr>
								</tbody>
							</table>
                           
						</form>
					</div><!-- Cart Table /- -->
					
			
				</div><!-- Container /- -->
			</div>
		</main>
@stop
@section('css')
	<style type='text/css'>
    
	    div.product-name {
	     font-size:0  !important;
	     letter-spacing:0  !important;
	    }
	    
	    .td{
	        border:0px !important;
	        color:red !important;
	    }
	    f0 img{
	    
	display: block !important;
	     
	    }
	    
	    .blue{
	        color:blue;
	    }
	</style>
@stop