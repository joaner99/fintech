@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>查詢訂做紀錄</h3>
						<p>INFO</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>							
						<li class="active">{{__('message.SignIn')}}</li>
                        <li class="active">{{__('message.ReviewOrderSummary')}}</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<!-- Checkout -->
					<div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Cart Table -->
					<div class="col-md-12 col-sm-12 col-xs-12 cart-table">
						<form>
							<table class="table table-bordered table-responsive">
								<thead>
									<tr>
										<th class="product-thumbnail">{{__('message.OrderNumber')}}</th>
										<th class="product-name">{{__('message.OrderDate')}}</th>
										<th class="product-quantity">{{__('message.PaymentOption')}}</th>
										<th class="product-unit-price">{{__('message.Total')}}</th>
										<th class="product-PaymentOption">{{__('message.Status')}}</th>
										
									</tr>
								</thead>
								<tbody>
									@foreach($Order as $Orders)
									<tr class="cart_item">
										<td data-title="Item" class="product-thumbnail"><a href="m_record_detail?id={{$Orders->id}}">{{$Orders->no}}</a></td>
										<td data-title="Product Name" class="product-name"><a href="#">{{date('Y/m/d',strtotime($Orders->created_at))}}</a></td>
										<td data-title="Quantity" class="product-quantity">{{$Orders->ext}}</td>
                                        <td data-title="Total" class="product-subtotal">{{number_format($Orders->total)}}</td>
										<td data-title="Remove" class="product-remove"><a href="#">{{$Orders->status}}</a></td>
									</tr>
									@endforeach
									<tr>
									</tr>
								</tbody>
							</table>
						</form>
					</div><!-- Cart Table /- -->
					
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
            
            
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
	<!-- Team Section -->
		
		<!-- Testimonial Section -->
<!-- Testimonial Section /- -->
		
		<!-- Clients -->
		<!-- Clients /- -->
	</main>
@stop