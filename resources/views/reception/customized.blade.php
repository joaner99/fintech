@extends('layouts.reception.reception')
@section('content')
<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>會員註冊</h3>
						<p>Member Login</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">會員註冊</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<!-- Checkout -->
			<div class="container no-left-padding no-right-padding woocommerce-checkout" >
				<!-- Container -->
				<div class="container">
					<!-- Login -->
					<div class="col-md-8 col-md-offset-2 login-block">
						<div class="login-check">
							<h3>會員註冊</h3>
							<div class="col-md-8 col-md-offset-2 login-form">
								<form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        			{{ csrf_field() }}
									<div class="form-group">
										<input  id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
										 @if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
									</div>

									<div class="form-group">
										<input id="password" type="password" class="form-control" name="password" required>
										@if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
									</div>
									<div class="form-group">
										<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
									</div>
									<input value="註冊" type="submit">
									
								</form>
							</div>
							
						</div>
					</div><!-- Login /- -->
					
			
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
            
            
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop