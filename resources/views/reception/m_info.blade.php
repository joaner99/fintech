@extends('layouts.reception.reception')
@section('content')
		<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>變更會員資料</h3>
						<p>INFO</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>							
						<li class="active">{{__('message.SignIn')}}</li>
                        <li class="active">{{__('message.AccountDetails')}}</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<!-- Checkout -->
			<div class="container no-left-padding no-right-padding woocommerce-checkout" >
				<!-- Container -->
				<div class="container">
					<!-- Login -->
					              
                    
                    		<div class="checkout-form">
						<form action="{{route('m_info_update')}}" method="POST" class="form-horizontal">
							{{ csrf_field() }}
							<div class="col-md-8 col-md-offset-2">
								<h3>會員資料變更</h3>
								<div class="billing-field">
									
									<div class="col-md-6 form-group">
										<label>姓名 *</label>
										<input name='name' class="form-control" type="text" value="{{Auth::user()->name}}">
									</div>
									
									<div class="col-md-6 form-group">
										<label>電話*</label>
										<input name='tel' class="form-control" type="text" value="{{Auth::user()->tel}}">
									</div>
                                    <div class="col-md-12 form-group">
										<label>Email 信箱 *</label>
										<input  name='email' class="form-control" type="text" value="{{Auth::user()->email}}">
									</div>
									<div class="col-md-12 form-group">
										<label>地  址 *</label>
										<input name='address' class="form-control" placeholder="Street Address" type="text" value="{{Auth::user()->address}}">
									</div>
									
									
                                    <div class="place-order">
										<input value="確認修改會員資料" type="submit">
									</div>
                                    
								</div>
                                
								
							</div>
							
						
						</form>
					</div>
                    
                    <!-- Login /- -->
					
			
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
            
            
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop