@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>查詢訂做紀錄</h3>
						<p>已訂購資訊</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>	
                        <li class="active">會員專區</li>
						<li class="active">查詢訂做紀錄</li>
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			<!-- Checkout -->
            
            
            	<div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Cart Table -->
					<div class="col-md-12 col-sm-12 col-xs-12 cart-table">
						<form>
							<table class="table table-bordered table-responsive">
								<thead>
									<tr>
										
										<th class="product-name">預覽</th>
										
									</tr>
								</thead>
								<tbody>
									<tr class="cart_item">
										
										<td data-title="Product Name" class="product-name">
                                            <img src="upload/{{$OrderDetail[0]->file1}}" alt="Product"  style="border:0px; padding:0px;"/></td>
									
										
									</tr>
									
								</tbody>
							</table>
                           
						</form>
					</div><!-- Cart Table /- -->
					
			
				</div><!-- Container /- -->
			</div>
            
            
            
			<div class="container-fluid no-left-padding no-right-padding woocommerce-checkout">
				<!-- Container -->
				<div class="container">
								<!-- Billing -->
					<div class="checkout-form">
						<form>
							@foreach ($Order as $Orders)
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h3>訂單資訊</h3>
								<div class="billing-field">
									<div class="col-md-12 form-group">
										<label>訂單編號:</label>
										<label>{{$Orders->no}}</label>
									</div>
									<div class="col-md-12 form-group">
										<label>訂做日期:</label>
										<label>{{$Orders->create_at}}</label>
									</div>
									<div class="col-md-12 form-group">
										<label>收件人:</label>
										<label>{{$Orders->name}}</label>
									</div>
									<div class="col-md-12 form-group">
										<label>收件人地址:</label>
										<label>{{$Orders->address}}</label>
									</div>
									<div class="col-md-12 form-group">
										<label>電話: </label>
										<label>{{$Orders->tel}}</label>
									</div>
									<div class="col-md-12 form-group">
										<label>E_mail:</label>
										<label>{{$Orders->email}}</label>
									</div>
									<div class="col-md-12 form-group">
										<label>發票類型:</label>
										<label>二聯式</label>
									</div>
                                </div>
							
							</div>
							@endforeach 
							
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h3>您的訂單</h3>
								<div class="shipping-fields">
									<div class="checkout-order-table">
										<table>
											@foreach($OrderDetail as $OrderDetails)
											<thead>
												<tr>
													<th>規格</th>
													<th>寬度 1.5cm X 長度 10米 (編輯長度9cm)</th>
												</tr>
											</thead>
											<tbody>
											
												<tr class="cart-subtotal">
													<th>數量</th>
													<td>{{$OrderDetails->amount}}</td>
												</tr>
                                                <tr class="cart-subtotal">
													<th>售價</th>
														<td>${{$OrderDetails->price}}</td>
												</tr>
											</tbody>
											@endforeach
											<tfoot>
												<tr>
													<th>運費</th>
													<td>0元</td>
												</tr>
												<tr>
													<th>總金額</th>
													<td>{{$Order[0]->total}}</td>
												</tr>
											</tfoot>
											
										</table>
									</div>
								
									<div class="place-order">
										<a href="{{ route('m_record') }}">
										<input value="回上一頁" type="button">
										</a>
									</div>
								</div>
							</div>
						</form>
					</div><!-- Billing /- -->
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
			
		</main>
@stop