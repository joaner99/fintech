<!-- Footer Main -->
		<footer id="footer-main" class="footer-main container-fluid">
			<!-- Container -->
			<div class="container">
				<div class="row">
					<!-- Widget About -->
					<aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_about">
						<a href="index.php" title="Max Shop">自己印 <span>紙膠帶</span></a>
						<div class="info">
							<p><i class="icon icon-Pointer"></i>台中市南屯區永春東路193號</p><p></p>
							<p><i class="icon icon-Phone2"></i><a href="tel:(04)24739873" title="Phone" class="phone">04-24739873</a></p>
							<p><i class="icon icon-Imbox"></i><a href="mailto:info@maxshop.com" title="info@maxshop.com">urtapeprint@gmail.com</a></p>
						</div>
					</aside><!-- Widget About /- -->
					<!-- Widget Links -->
					<aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_links">
						<h3 class="widget-title">網站連結</h3>
						<ul>
							<li><a href="shop.php" title="Best Selling">產品展示</a></li>
							<li><a href="#" title="About Us">訂做紙膠帶</a></li>
							<li><a href="qa.php" title="Deal Of The Day">常見問與答</a></li>
							<li><a href="about.php" title="Product Collections">關於我們</a></li>
							<li><a href="contact-us.php" title="Contact Us">聯絡我們</a></li>
                            <li><a href="#" title="Popular Products">製作流程教學</a></li>
						</ul>
					</aside><!-- Widget Links /- -->
					<!-- Widget Account -->
					<aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_links widget_account">
						<h3 class="widget-title">會員專區</h3>
						<ul>
							<li><a href="m_record.php" title="My Order Details">我的訂購紀錄</a></li>
							<li><a href="m_change.php" title="My credit Offers">更改訂單</a></li>
							<li><a href="m_info.php" title="My addresses">更改會員資料</a></li>
							<li><a href="m_change.php" title="My Personal Details">更改密碼</a></li>
							<li><a href="#" title="My Account Details">會員登出</a></li>
						</ul>
					</aside><!-- Widget Account /- -->
					<!-- Widget Newsletter -->
					<aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_newsletter">
						
               <h3 class="widget-title">自己印粉絲專頁</h3>
						
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Furtape%2F&tabs&width=200&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=126241997456571" width="500" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

						
					
					</aside><!-- Widget Newsletter /- -->
				</div>
				<div class="copyright-section">
					<div class="coyright-content">
						<p>&copy; Copyright © 2018 自己印紙膠帶 All Rights Reserved.</p>
					</div>	
					
				</div>
			</div><!-- Container /- -->
		</footer>