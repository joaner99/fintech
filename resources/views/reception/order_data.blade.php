@extends('layouts.reception.reception')
@section('content')
	<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>填寫訂單資料</h3>
						<p>收件人資訊請確實填寫以便寄送！</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">購物車</li>
                        <li class="active">填寫訂單資料</li>
                      
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Cart Table -->
					<div class="col-md-12 col-sm-12 col-xs-12 cart-table">
						<form action="{{route('order_data')}}" method="POST" class="form-horizontal">
							{{ csrf_field() }}
							<table class="table table-bordered table-responsive">
								<thead>
									
									<tr>
										
										<th class="product-name">縮圖</th>
										<th class="product-quantity">規格</th>
										<th class="product-unit-price">數量</th>
										<th class="product-subtotal">售價</th>
										<th class="product-remove">移除</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $datas)
									<tr class="cart_item">
										<td data-title="Product Name" class="product-name"><img src="upload/images/product-{{$datas->id}}.png" alt="Product" /></td>
									
										<td data-title="Unit Price" class="product-unit-price">{{$datas->name}}<br>{{$datas->ext}}</td>
                                        	<td data-title="Quantity" class="product-quantity">
											<div class="prd-quantity" data-title="Quantity">
												<input name="quantity1" value="{{$datas->qty}}" class="qty" type="text">
											</div>
										</td>
										<td data-title="Total" class="product-subtotal">${{$datas->price *$datas->qty }}</td>
										<td data-title="Remove" class="product-remove"><a href="{{ route('remove',$datas->rowId) }}"><i class="icon icon-Delete"></i></a></td>
									</tr>
									@endforeach
									<tr>
										<td class="action" colspan="6">
											<a href="{{ route('select_design') }}" title="Continue shopping">繼續做紙膠帶</a>
											
									</tr>
								</tbody>
							</table>
						</form>
					</div><!-- Cart Table /- -->
					
			
				</div><!-- Container /- -->
			</div>
            
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
        <main>
        
        			<!-- Checkout -->
			<div class="container-fluid no-left-padding no-right-padding woocommerce-checkout">
				<!-- Container -->
				<div class="container">
			
					
					<!-- Billing -->
					<div class="checkout-form">
						<form action="{{route('cars_pay')}}" method="POST" class="form-horizontal">
				{{ csrf_field() }}
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h3>收件人資訊</h3>
								<div class="billing-field">
									
									<div class="col-md-12 form-group">
										<label>姓名 *</label>
										<input class="form-control"  type="text" name="name" value="收件人">
									</div>	
                                    <div class="col-md-6 form-group">
										<label>Email *</label>
										<input class="form-control" type="email" name="email" value="email">
									</div>
									<div class="col-md-6 form-group">
										<label>手機號碼 *</label>
										<input class="form-control" type="tel" name="tel" value="電話">
									</div>
                                    <div class="col-md-6 form-group">
										<label>郵遞區號 *</label>
										<input class="form-control" type="text" name="zipcode" value="郵遞區號">
									</div>
									<div class="col-md-12 form-group">
										<label>寄送地址 *</label>
										<input class="form-control" type="text" name="address" value="地址">
									</div>							
																	
									
								</div>
								
							</div>
							
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h3>您的訂單</h3>
								<div class="shipping-fields">
									<div class="checkout-order-table">
										<table>
											<thead>
												<tr>
													<th>付款方式</th>
													<th>	<div class="col-md-12 form-group">
										
										<div class="select">
											<select class="form-control">
												<option >台灣區/宅配  ATM臨櫃轉帳NT.60</option>

												<option >台灣區/宅配  WebATM轉帳(需裝讀卡機)NT.60</option>

												<option>台灣區/宅配 線上刷卡NT.60</option>

												<option>台灣區/宅配 三大超商代碼繳費NT.60</option>

												<option>台灣區/全家便利超商 取貨付款NT.80</option>

												<option>中國/港澳地區(順豐速捷) 線上刷卡NT.150</option>

												<option>自己印實驗室門市NT.0</option>

												<option>國資圖櫃檯專用(非一般客戶使用)NT.0</option>


											</select>
										</div>
									</div></th>
												</tr>
											</thead>
											<tbody>
												<tr class="cart_item">
													<td class="product-name">運費</td>
													<td>需要與金流配合</td>
												</tr>
												
											</tbody>
											<tfoot>
											
												<tr>
													<th>總計</th>
													<td>{{Cart::subtotal()}}</td>
												</tr>
											</tfoot>
										</table>
									</div>
									
									<div class="place-order">
										<input value="前往結帳" type="submit">
                                        
									</div>
								</div>
							</div>
						</form>
					</div><!-- Billing /- -->
				</div><!-- Container /- -->
			</div><!-- Checkout /- -->
        </main>
@stop