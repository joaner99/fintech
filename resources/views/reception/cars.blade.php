@extends('layouts.reception.reception')
@section('content')

		<main>
			<!-- Page Banner -->
			<div class="page-banner container-fluid no-padding">
				<!-- Container -->
				<div class="container">
					<div class="banner-content">
						<h3>購物車</h3>
						<p>CAR</p>
					</div>
					<ol class="breadcrumb">
						<li><a href="index.html" title="Home">首頁</a></li>							
						<li class="active">購物車</li>
                      
					</ol>
				</div><!-- Container /- -->
			</div><!-- Page Banner /- -->
			
			<!-- About Section -->
		
            	<div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
				<!-- Container -->
				<div class="container">
					<!-- Cart Table -->
					<div class="col-md-12 col-sm-12 col-xs-12 cart-table">
						<form action="{{route('order_data')}}" method="POST" class="form-horizontal">
							{{ csrf_field() }}
							<table class="table table-bordered table-responsive">
								<thead>
									
									<tr>
										
										<th class="product-name">縮圖</th>
										<th class="product-quantity">規格</th>
										<th class="product-unit-price">數量</th>
										<th class="product-subtotal">售價</th>
										<th class="product-remove">移除</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data as $datas)
									<tr class="cart_item">
										<td data-title="Product Name" class="product-name"><img src="upload/images/product-{{$datas->id}}.png" alt="Product" /></td>
									
										<td data-title="Unit Price" class="product-unit-price">{{$datas->name}}<br>{{$datas->ext}}</td>
                                        	<td data-title="Quantity" class="product-quantity">
											<div class="prd-quantity" data-title="Quantity">
												<input name="quantity1" value="{{$datas->qty}}" class="qty" type="text">
											</div>
										</td>
										<td data-title="Total" class="product-subtotal">${{$datas->price *$datas->qty }}</td>
										<td data-title="Remove" class="product-remove"><a href="{{ route('remove',$datas->rowId) }}"><i class="icon icon-Delete"></i></a></td>
									</tr>
									@endforeach
									<tr>
										<td class="action" colspan="6">
											<a href="{{ route('select_design') }}" title="Continue shopping">繼續做紙膠帶</a>
                                            <a href="{{ route('order_data') }}" title="Continue shopping">前往結帳</a>
											
									</tr>
								</tbody>
							</table>
						</form>
					</div><!-- Cart Table /- -->
					
			
				</div><!-- Container /- -->
			</div>
            
            
            
            
            <!-- About Section /- -->
			
			<!-- Team Section -->
		<!-- Team Section -->
			
			<!-- Testimonial Section -->
	<!-- Testimonial Section /- -->
			
			<!-- Clients -->
			<!-- Clients /- -->
		</main>
@stop