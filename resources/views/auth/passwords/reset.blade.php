@extends('layouts.reception.reception')
@section('content')
    <main>
            <!-- Page Banner -->
            <div class="page-banner container-fluid no-padding">
                <!-- Container -->
                <div class="container">
                    <div class="banner-content">
                        <h3>忘記密碼</h3>
                        <p>RESET PASSWORD</p>
                    </div>
                    <ol class="breadcrumb">
                        <li><a href="index.html" title="Home">首頁</a></li>                           
                        <li class="active">會員登入</li>
                        <li class="active">忘記密碼</li>
                    </ol>
                </div><!-- Container /- -->
            </div><!-- Page Banner /- -->
            
            <!-- About Section -->
        
                <!-- Checkout -->
                    <div class="woocommerce-cart container-fluid no-left-padding no-right-padding">
                <!-- Container -->
                <div class="container">
                    <!-- Cart Table -->
                    <div class="col-md-12 col-sm-12 col-xs-12 cart-table">
                        <form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">密碼</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">確認密碼</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    更新密碼
                                </button>
                            </div>
                        </div>
                    </form>
                    </div><!-- Cart Table /- -->
                    
                </div><!-- Container /- -->
            </div><!-- Checkout /- -->
            
            
            
            
            
            <!-- About Section /- -->
            
            <!-- Team Section -->
    <!-- Team Section -->
        
        <!-- Testimonial Section -->
<!-- Testimonial Section /- -->
        
        <!-- Clients -->
        <!-- Clients /- -->
    </main>
@stop
