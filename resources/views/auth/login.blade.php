@extends('layouts.reception.reception')

@section('content') 
    <div class="page-banner container-fluid no-padding">
        <!-- Container -->
        <div class="container">
            <div class="banner-content">
                <h3>會員登入</h3>
                <p>Member Login</p>
            </div>
            <ol class="breadcrumb">
                <li><a href="index.html" title="Home">{{__('message.Home')}}</a></li>                           
                <li class="active">{{__('message.SignIn')}}</li>
            </ol>
        </div><!-- Container /- -->
    </div><!-- Page Banner /- -->
    
    <!-- About Section -->
        <!-- Checkout -->
    <div class="container no-left-padding no-right-padding woocommerce-checkout" >
        <!-- Container -->
        <div class="container">
            <!-- Login -->
            <div class="col-md-8 col-md-offset-2 login-block">
                <div class="login-check">
                    <h3>會員登入</h3>
                    <div class="col-md-8 col-md-offset-2 login-form">
                        <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                            <div class="form-group">
                                <input name="email" class="form-control" placeholder="Email 信箱*" type="text">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input name="password" class="form-control" placeholder="Password 密碼*" type="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <input value="FACEBOOK登入" type="submit">
                            <input value="{{__('message.SignIn')}}" type="submit">

                            <br>
                            <br>
                            <a href="{{asset('register')}}" title="Forgot Password?">立即註冊</a>
                            <a href="{{ route('password.request') }}" title="Forgot Password?">{{__('message.ForgottenPassword')}}</a>
                        </form>
                    </div>
                    
                </div>
            </div><!-- Login /- -->
        </div><!-- Container /- -->
    </div>
@endsection
