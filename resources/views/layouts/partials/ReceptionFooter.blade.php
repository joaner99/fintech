<!-- Footer Main -->
        <footer id="footer-main" class="footer-main container-fluid">
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <!-- Widget About -->
                    <aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_about">
                        <a href="index.php" title="Max Shop">自己印 <span>紙膠帶</span></a>
                        <div class="info">
                            <p><i class="icon icon-Pointer"></i>台中市南屯區永春東路193號</p><p></p>
                            <p><i class="icon icon-Phone2"></i><a href="tel:(04)24739873" title="Phone" class="phone">04-24739873</a></p>
                            <p><i class="icon icon-Imbox"></i><a href="mailto:info@maxshop.com" title="info@maxshop.com">urtapeprint@gmail.com</a></p>
                        </div>
                    </aside><!-- Widget About /- -->
                    <!-- Widget Links -->
                    <aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_links">
                        <h3 class="widget-title">{{__('message.MaskingTape')}}</h3>
                        <ul>
                            <li><a href="{{asset('shop/0')}}" title="Best Selling">{{__('message.ProductDisplay')}}</a></li>
                            <li><a href="#" title="About Us">{{__('message.SiteMap')}}</a></li>
                            <li><a href="{{asset('qa')}}" title="Deal Of The Day">{{__('message.FrequentlyAskedQuestions')}}</a></li>
                            <li><a href="{{asset('About')}}" title="Product Collections">{{__('message.AboutUs')}}</a></li>
                            <li><a href="{{asset('contanct')}}" title="Contact Us">{{__('message.ContactUs')}}</a></li>
                            <li><a href="{{asset('flow')}}" title="Popular Products">{{__('message.HowToMakeTheMaskingTape')}}</a></li>
                        </ul>
                    </aside><!-- Widget Links /- -->
                    <!-- Widget Account -->
                    <aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_links widget_account">
                        <h3 class="widget-title">{{__('message.MyAccount')}}</h3>
                        <ul>
                            <li><a href="{{asset('m_record')}}" title="My Order Details">{{__('message.OrderSummary')}}</a></li>
                            <li><a href="{{asset('m_record')}}" title="My credit Offers">{{__('message.EditMyOrder')}}</a></li>
                            <li><a href="{{asset('m_info')}}" title="My addresses">{{__('message.EditAccountDetails')}}</a></li>
                            <li><a href="{{asset('m_change')}}" title="My Personal Details">{{__('message.EditPassword')}}</a></li>
                            <li><a href="#" title="My Account Details">{{__('message.SignOut')}}</a></li>
                        </ul>
                    </aside><!-- Widget Account /- -->
                    <!-- Widget Newsletter -->
                    <aside class="col-md-3 col-sm-6 col-xs-6 ftr-widget widget_newsletter">
                        
               <h3 class="widget-title">{{__('message.URTapeFanPage')}}</h3>
                        
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Furtape%2F&tabs&width=200&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=126241997456571" width="500" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>

                        
                    
                    </aside><!-- Widget Newsletter /- -->
                </div>
                <div class="copyright-section">
                    <div class="coyright-content">
                        <p>&copy; Copyright © 2018 自己印紙膠帶 All Rights Reserved.</p>
                    </div>  
                    
                </div>
            </div><!-- Container /- -->
        </footer>