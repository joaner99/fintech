<header class="header-section container-fluid no-padding">
            <!-- Top Header -->
            <div class="top-header container-fluid no-padding">
                <!-- Container -->
                <div class="container">
                    <div class="col-md-7 col-sm-7 col-xs-7 dropdown-bar">
                        <h5>自己印・紙膠帶</h5>
                        <div class="language-dropdown dropdown">
                            <button aria-expanded="true" aria-haspopup="true" data-toggle="dropdown" title="languages" id="language" type="button" class="btn dropdown-toggle">
                            @if(App::getLocale()=='en')
                            English
                            @elseif(App::getLocale()=='cn')
                            中文
                            @elseif(App::getLocale()=='jp')
                            日文
                            @endif
                             <span class="caret"></span></button>
                            <ul class="dropdown-menu no-padding">
                                <li><a title="chinese" href="{{asset('Home?lang=cn')}}">中文</a></li>
                                <li><a title="English" href="{{asset('Home?lang=en')}}">English</a></li>
                                <li><a title="Japan" href="{{asset('Home?lang=jp')}}">日本語</a></li>
                            </ul>
                        </div>                      
                    </div>
                    <!-- Social -->
                    <div class="col-md-5 col-sm-5 col-xs-5 header-social"> 
                        <ul>
                            <li>Tel：04-24739873</li>
                        </ul>
                    </div><!-- Social /- -->
                </div><!-- Container /- -->
            </div><!-- Top Header /- -->
            
            <!-- Menu Block -->
            <div class="container-fluid no-padding menu-block">
                <!-- Container -->
                <div class="container">
                    <!-- nav -->
                    <nav class="navbar navbar-default ow-navigation">
                        <div class="navbar-header">
                            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a href="{{asset('Home')}}"><img src="{{asset('images/logo.jpg')}}" alt="logo"></a>
                        </div>
                        <!-- Menu Icon -->
                        <div class="menu-icon">
                            
                            <ul class="cart">                           
                
                                <li><a href="{{asset('login')}}" title="User"><i class="icon icon-User"></i></a></li>
                                <li><a href="https://www.facebook.com/urtape/" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            </ul>
                        </div><!-- Menu Icon /- -->
                        <div class="navbar-collapse collapse navbar-right" id="navbar">
                            <ul class="nav navbar-nav">
                                <li class="active dropdown">
                                    <a href="{{asset('Home')}}" title="Home" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{{__('message.Home')}}</a>
                                    <i class="ddl-switch fa fa-angle-down"></i>
                                    
                                </li>
                                <li class="dropdown">
                                <a href="{{asset('login')}}" title="Blog" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{{__('message.MyAccount')}}</a>
                                    <i class="ddl-switch fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">
                                        @if(!Auth::check())      
                                        <li><a href="{{asset('login')}}" title="Blog Post">{{__('message.SignIn')}}</a></li>
                                        @else
                                        <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{__('message.SignOut')}}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                        </li>
                                        @endif
                                        <li><a href="{{asset('m_record')}}" title="Blog Post">{{__('message.ReviewOrderSummary')}}</a></li>
                                        <li><a href="{{asset('m_info')}}" title="Blog Post">{{__('message.ChangeData')}}</a></li>
                                        <li><a href="{{asset('m_change')}}" title="Blog Post">{{__('message.ResetPassword')}}</a></li>
                                    </ul></li>
                                <li class="dropdown">
                                    <a href="{{asset('About')}}" title="Home" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{{__('message.AboutUs')}}</a>
                                    <i class="ddl-switch fa fa-angle-down"></i>
                                    
                                </li>
                                
                                <li class="dropdown">
                                    <a href="{{asset('shop/0')}}" title="Shop" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{{__('message.ProductDisplay')}}</a>
                                    <i class="ddl-switch fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">              
                                        <li><a href="{{asset('shop/1')}}" title="Shop Single">{{__('message.MaskingTape')}}</a></li>
                                        <li><a href="{{asset('shop/2')}}" title="Shop Single">{{__('message.Postcard')}}</a></li>
                                        <li><a href="{{asset('shop/3')}}" title="Shop Single">{{__('message.Sticker')}}</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" title="customized" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">{{__('message.CustomPrintedMaskingTape')}}</a>
                                    <i class="ddl-switch fa fa-angle-down"></i>
                                    <ul class="dropdown-menu">              
                                        <li><a href="{{asset('select_design')}}" title="customized">{{__('message.CustomPrintedMaskingTape')}}</a></li>
                                        <li><a href="{{asset('select_design2')}}" title="customized">{{__('message.CustomPrintedPostcard')}}</a></li>
                                        <li><a href="#" title="customized">{{__('message.CustomPrintedSticker')}}</a></li>
                                        <li><a href="{{asset('flow')}}" title="process">{{__('message.HowToMakeTheMaskingTape')}}</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{asset('qa')}}" title="QA">{{__('message.FrequentlyAskedQuestions')}}</a></li>
                                <li><a href="{{asset('contanct')}}" title="Contact Us">{{__('message.ContactUs')}}</a></li>
                                
                            </ul>
                        </div><!--/.nav-collapse -->
                    </nav><!-- nav /- -->
                    <!-- Search Box -->
                    <div class="search-box">
                        <span><i class="icon_close"></i></span>
                        <form><input type="text" class="form-control" placeholder="Enter a keyword and press enter..." /></form>
                    </div><!-- Search Box /- -->
                </div><!-- Container /- -->
            </div><!-- Menu Block /- -->
        </header>