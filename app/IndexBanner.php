<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class IndexBanner extends Model
{

    protected $fillable = [
        'file1', 'title1', 'title2',
    ];


}
