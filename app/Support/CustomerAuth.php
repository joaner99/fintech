<?php

namespace App\Support;
use Session;
use App\Models\Customer\Customer;

class CustomerAuth
{
    public static function info()
    {
        $info = Session::get('customerAuth', null);
        if (!is_null($info)) {
            $info = json_decode($info);
            $info = Customer::find($info->id);
        }
        
        return $info;
    }
        
    public static function check()
    {
        $info = Session::get('customerAuth', null);

        return !is_null($info);
    }
    
    public static function login($auth_type, $auth_id)
    {
        $info = Customer::where('oauth_type', $auth_type)
            ->where('oauth_id', $auth_id)
            ->first();
        
        if (!is_null($info)) {
            Session::put('customerAuth', json_encode($info));
        }
    }
    
    public static function create($info)
    {
        if (Customer::create($info)) {
            return true;
        }
        return false;
    }
    
    public static function update($data)
    {
        $info = self::info();
        if (is_null($info)) {
            return false;
        }
        
        Customer::where('oauth_type', $info->oauth_type)
            ->where('oauth_id', $info->oauth_id)
            ->update($data);
        
        self::login($info->oauth_type, $info->oauth_id);
    }
    
    public static function logout()
    {
        Session::forget('customerAuth');
    }
}
