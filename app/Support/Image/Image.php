<?php
namespace App\Support\Image;

class Image
{
    public $image  = null;
    public $mime   = '';
    public $width  = 0;
    public $height = 0;

    public function __construct($file)
    {
        // 判斷是檔案還是 dataUrl
        if (is_file($file)) {
            if ($size = getimagesize($file)) {
                // 判斷圖片的類型，建立不同的圖片元件
                switch ($size['mime']) {
                    case 'image/jpeg':
                        $this->image = imagecreatefromjpeg($file); //jpeg file
                        break;
                    case 'image/gif':
                        $this->image = imagecreatefromgif($file); //gif file
                        imagealphablending($this->image, false);
                        imagesavealpha($this->image, true);
                        break;
                    case 'image/png':
                        $this->image = imagecreatefrompng($file); //png file
                        imagealphablending($this->image, false);
                        imagesavealpha($this->image, true);
                        break;
                    default:
                        $this->image = null;
                        break;
                }
                if ($this->image !== null) {
                    $this->mime   = $size['mime'];
                    $this->height = imagesy($this->image);
                    $this->width  = imagesx($this->image);
                }
            }
        } else {
            // 嘗試以 dataUrl 作解析的動作
            if (preg_match('/^data:([^;]*);base64/i', $file, $match)) {
                try {
                    $this->mime   = $match[1];
                    $base64       = base64_decode(str_replace('data:'.$this->mime.';base64,', '', $file));
                    $this->image  = imagecreatefromstring($base64);
                    $this->width  = imagesx($this->image);
                    $this->height = imagesy($this->image);
                } catch (Exception $e) {

                }
            }
        }
    }

    // 裁切
    public function crop($x, $y, $width, $height)
    {
        if (is_null($this->image)) {
            return false;
        }

        if ($x < 0) {
            if (($x + $width) < 0) {
                return false;     
            }
            $x = 0;
        }
        if ($y < 0) {
            if (($y + $height) < 0) {
                return false;   
            }
            $y = 0;
        }
        if ($x > $this->width) {
            $x = $this->width;
        }
        if ($y > $this->height) {
            $y = $this->height;
        }
        if (($x + $width) > $this->width) {
            $width = $this->width - $x;
        }
        if (($y + $height) > $this->height) {
            $height = $this->height - $y;
        }

        $this->image  = imagecrop($this->image, ['x' => $x, 'y' => $y, 'width' => $width, 'height' => $height]);
        $this->width  = $width;
        $this->height = $height;
        
        return true;
    }

    // 設定最大大小
    public function maxSize($width, $height)
    {
        // 計算出要縮放的比例
        if ($this->width > $width && $this->height > $height) {
            return $this->resize($width, $height);
        }
        return true;
    }
    
    // 調整大小
    public function resize($width, $height, $keepRange = true)
    {
        if (is_null($this->image)) {
            return false;
        }
        
        if ($keepRange) {
            $percent = 1;
            // 計算出要縮放的比例

            if ($width > $height) {
                $percent = $height / $this->height;
            } else {
                $percent = $width / $this->width;
            }

            // 取得新的長寬
            $newWidth  = $this->width * $percent;
            $newHeight = $this->height * $percent;
        } else {
            $newWidth  = $width;
            $newHeight = $height;
        }

        $thumb = imagecreatetruecolor($newWidth, $newHeight);
        
        if ($this->mime == 'image/gif' || $this->mime == 'image/png') {
            imagealphablending($thumb, false);
            imagesavealpha($thumb, true);
        }
        
        // 調整大小
        if (imagecopyresized($thumb, $this->image, 0, 0, 0, 0,
                             $newWidth, $newHeight, $this->width, $this->height)) {
            $this->image  = $thumb;
            $this->width  = $newWidth;
            $this->height = $newHeight;
            return true;
        }
        return false;
    }
    
    // 轉存圖片
    public function save($file, $quality = 85)
    {
        $result = false;
        if (!is_null($this->image)) {
            // 判斷圖片的類型，印出不同的圖片
            switch ($this->mime) {
                case 'image/jpeg':
                    $result = imagejpeg($this->image, $file, $quality);
                    break;
                case 'image/gif':
                    $result = imagegif($this->image, $file);
                    break;
                case 'image/png':
                    $result = imagepng($this->image, $file, $quality / 10);
                    break;
            }
        }
        
        return $result;
    }
    
    // 印出圖片
    public function output($expire = null)
    {
        if (!is_null($this->image)) {
            ob_clean();
            if (!is_null($expire)) {
                $expire = strtotime($expire);
                header('Pragma: private');
                header('Cache-Control: private, pre-check='.($expire - time()).', max-age='.($expire - time()));
                header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', $expire));
            }
            
            // 判斷圖片的類型，印出不同的圖片
            switch ($this->mime) {
                case 'image/jpeg':
                    header('Content-Type: image/jpeg');
                    imagejpeg($this->image);
                    break;
                case 'image/gif':
                    header('Content-Type: image/gif');
                    imagegif($this->image);
                    break;
                case 'image/png':
                    header('Content-Type: image/png');
                    imagepng($this->image);
                    break;
            }
            imagedestroy($this->image);
            exit;
        }
    }
}
