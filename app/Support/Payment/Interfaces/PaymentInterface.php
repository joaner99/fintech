<?php
namespace App\Support\Payment\Interfaces;

interface PaymentInterface
{
    public static function setup($config);
    public static function sendOrder($order);
    public static function getFeedback();
    public static function returnResult($success = true, $message = '');
    public static function getMerchantTradeNo($request);
    public static function newMerchantTradeNo();
    public static function queryOrder($tradeNo, $amt = 0);
    public static function noIsThePaymentType($tradeNo);
}