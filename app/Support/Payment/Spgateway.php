<?php
namespace App\Support\Payment;

use App\Support\Payment\Interfaces\PaymentInterface;
use Request;

class Spgateway implements PaymentInterface
{
    protected static $config;
    
    public static function setup($config)
    {
        $defaultConfig = [
            'hash_key'       => '',
            'hash_iv'        => '',
            'merchant_id'    => '',
            'return_url'     => '',
            'back_url'       => '',
            'result_url'     => '',
            'payment_method' => 'credit',
            'staging_number' => '3,6',
        ];
        
        $config       = array_merge($defaultConfig, $config);
        self::$config = $config;    
    }
    
    public static function sendOrder($order)
    {
        $paymentMethod = 'CREDIT';
        switch (self::$config['payment_method']) {
            case 'credit':
                $paymentMethod = 'CREDIT';
                break;
            case 'staging':
                $paymentMethod = 'InstFlag';
                break;
            case 'atm':
                $paymentMethod = 'VACC';
                break;
        }
        
        try
        {
            $serviceUrl = env('PAYMENT_IS_TEST', true) ? 'https://ccore.spgateway.com/MPG/mpg_gateway' : 'https://core.spgateway.com/MPG/mpg_gateway';

            $params                                = ['params' => [], 'public' => []];
            
            /* 服務參數 */
            $params['ServiceURL']                = $serviceUrl;
            $params['params']['HashKey']         = trim(self::$config['hash_key']);
            $params['params']['HashIV']          = trim(self::$config['hash_iv']);
            $params['params']['Version']         = '1.4';
            $params['params']['CVS']             = '0';
            
            /* 基本參數 */
            $params['params']['MerchantID']      = trim(self::$config['merchant_id']);
            $params['params']['RespondType']     = 'JSON';
            $params['params']['TimeStamp']       = time();
            $params['params']['LangType']        = 'zh-tw';
            $params['params']['MerchantOrderNo'] = $order['merchant_trade_no'];
            $params['params']['Amt']             = $order['total'];
            $params['params']['Email']           = self::$config['email'];
            $params['params']['LoginType']       = '0';
            
            /* 加入選購商品資料。*/
            $itemDesc = '';
            if (isset($order['items'])) {
                foreach ($order['items'] as $item) {
                    $itemDesc .= $item['prod_name'].(empty($item['size']) ? '' : '-'.$item['size']).' $'.$item['price'].' * '.$item['qty'].';';  
                }    
            }
            $params['params']['ItemDesc']        = mb_substr($itemDesc, 0, 50);
            $params['params']['CustomerURL']     = self::$config['result_url'];
            $params['params']['ClientBackURL']   = self::$config['back_url'];
            $params['params']['NotifyURL']       = self::$config['notify_url'];
            $params['params']['ReturnURL']       = self::$config['result_url'];
            $params['params']['CREDIT']          = ($paymentMethod == 'CREDIT') ? '1' : '0';
            $params['params']['InstFlag']        = ($paymentMethod == 'InstFlag') ? self::$config['staging_number'] : '0';
            $params['params']['WEBATM']          = '0';
            $params['params']['VACC']            = ($paymentMethod == 'VACC') ? '1' : '0';
            $params['params']['CVS']             = '0';

            /* 處理驗證碼 */
            $params['params']['TradeInfo']       = self::encryptTradeInfo($params['params']);
            $params['params']['TradeSha']        = self::getTradeSha($params['params']['TradeInfo']);

            /* 產生訂單 */
            self::CheckOut($params);
        }
        catch (Exception $e)
        {
            // 例外錯誤處理。
            throw $e;
        }
    }
    
    public static function getFeedback()
    {
        $tradeInfo = json_decode(self::decryptTradeInfo(Request::input('TradeInfo')), true);
        return [
            'result'          => $tradeInfo['Status'] == 'SUCCESS',
            'MerchantTradeNo' => $tradeInfo['Result']['MerchantOrderNo'],
            'TradeNo'         => '',
            'BankCode'        => isset($tradeInfo['Result']['BankCode']) ? $tradeInfo['Result']['BankCode'] : '',
            'CodeNo'          => isset($tradeInfo['Result']['CodeNo']) ? $tradeInfo['Result']['CodeNo'] : '',
        ];
    }
    
    public static function getMerchantTradeNo($request)
    {
        $tradeInfo = json_decode(self::decryptTradeInfo($request->TradeInfo));

        return $request->MerchantTradeNo;
    }
    
    public static function returnResult($success = true, $message = '')
    {
        return $success ? 'SUCCESS' : 'ERROR';
    }
    
    protected static function CheckOut($params)
    {           
        //生成表單，自動送出
        $szHtml =  '<!DOCTYPE html>';
        $szHtml .= '<html>';
        $szHtml .=     '<head>';
        $szHtml .=         '<meta charset="utf-8">';
        $szHtml .=     '</head>';
        $szHtml .=     '<body>';
        $szHtml .=         "<form id=\"__pay2goForm\" method=\"post\" action=\"{$params['ServiceURL']}\">";
        foreach ($params['params'] as $keys => $value) {
            if ($keys == 'HashKey' || $keys == 'HashIV') {
                continue;
            }
            $szHtml .=         "<input type=\"hidden\" name=\"{$keys}\" value='{$value}' />";
        }

        $szHtml .=         '</form>';
        $szHtml .=         '<script type="text/javascript">document.getElementById("__pay2goForm").submit();</script>';
        $szHtml .=     '</body>';
        $szHtml .= '</html>';

        echo $szHtml ;
        exit;
    }
    
    protected static function encryptTradeInfo($params)
    {
        /*$params = [
            'MerchantID'      => $params['MerchantID'],
            'TimeStamp'       => $params['TimeStamp'],
            'Version'         => $params['Version'],
            'MerchantOrderNo' => $params['MerchantOrderNo'],
            'Amt'             => $params['Amt'],
            'ItemDesc'        => $params['ItemDesc']  
        ];*/

        return self::create_mpg_aes_encrypt(trim(self::$config['hash_key']),
                                            trim(self::$config['hash_iv']),
                                            $params);   
    }
    
    protected static function decryptTradeInfo($info)
    {
        return self::aes_decrypt(trim(self::$config['hash_key']),
                                 trim(self::$config['hash_iv']),
                                 $info);   
    }
    
    protected static function create_mpg_aes_encrypt($key = "", $iv = "", $parameter = "") {
        $return_str = '';
        if (!empty($parameter)) {
            $return_str = http_build_query($parameter);
        }

        return trim(bin2hex(self::AESEncrypt($key, $iv, self::addpadding($return_str))));
    }
    
    protected static function addpadding($string, $blocksize = 32) {
        $len     = strlen($string);
        $pad     = $blocksize  - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
        return $string;
    }
    
    protected static function getTradeSha($tradeInfo)
    {
        $str = 'HashKey='.trim(self::$config['hash_key']).'&'.$tradeInfo.'&HashIV='.trim(self::$config['hash_iv']);
        return strtoupper(hash("sha256", $str));
    }
    
    protected static function AESEncrypt($key, $iv, $rawString)
    {
        // Set the method
        $method = 'AES-256-CBC';

        // Encrypt the data
        $encrypted = openssl_encrypt($rawString, $method, $key, OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
        return $encrypted;
    }
    
    protected static function AESDecrypt($key, $iv, $encrypted)
    {
        // Set the method
        $method = 'AES-256-CBC';

        // Decrypt the data
        $decrypted = openssl_decrypt($encrypted, $method, $key, OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);

        return $decrypted;
    }
    
    protected static function aes_decrypt($key, $iv, $aes_str)
    {
        $aes_str = str_replace(' ', '+', $aes_str);

        $str     = self::strippadding(self::AESDecrypt($key, $iv, hex2bin($aes_str)));
        return $str;
    }
    
    protected static function strippadding($string)
    {
        $slast  = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
            return $string;
        } else {
            return false;
        }
    }
    
    public static function newMerchantTradeNo()
    {
        return 'SGW'.date('ymd').sprintf('%06d', rand(1, 99999));
    }
    
    public static function queryOrder($tradeNo, $amt = 0)
    {
        $serviceUrl = env('PAYMENT_IS_TEST', true) ? 'https://ccore.spgateway.com/API/QueryTradeInfo' : 'https://core.spgateway.com/API/QueryTradeInfo'; 

        /* 基本參數 */
        $params['MerchantID']      = trim(self::$config['merchant_id']);
        $params['MerchantOrderNo'] = $tradeNo;
        $params['Amt']             = $amt;
        ksort($params);
        
        $params['CheckValue']      = strtoupper(hash('sha256', 'IV='.trim(self::$config['hash_iv']).'&'.trim(http_build_query($params)).'&Key='.trim(self::$config['hash_key'])));
        
        /* 服務參數 */
        $params['Version']         = '1.1';
        $params['TimeStamp']       = time();
        $params['RespondType']     = 'JSON';
        
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serviceUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $feedBack = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($feedBack);
        if ($result->Status == 'SUCCESS') {
            $paymentStatus = 0;
            switch($result->Result->TradeStatus) {
                case 1:
                    $paymentStatus = 1;  // 已付款
                    break;
            }
            return [
                'products'      => '',
                'paymentTime'   => $result->Result->PayTime,
                'paymentStatus' => $paymentStatus,
            ]; 
        }
        
        return null;
    }
    
    public static function noIsThePaymentType($tradeNo)
    {
        return preg_match('/^SGW[0-9]{14}$/i', $tradeNo) || preg_match('/^SGW[0-9]{14}0[1-9]$/i', $tradeNo);
    }
}