請在 .env 加入下列設定
PAYMENT_ECPAY_IS_TEST=true                 // 測試旗標(當測試時，會用歐付寶的測試環境)
PAYMENT_ECPAY_MERCHANTID=2000132           // 廠商編號 廠商給
PAYMENT_ECPAY_HASH_KEY=5294y06JbISpM5x9    // HASH KEY 廠商給
PAYMENT_ECPAY_HASH_IV=v77hoKGq4kWxNNIS     // HASH IV 廠商給
PAYMENT_ECPAY_RETURN_URL=http://localhost  // 訂單付款完成後的回覆網址 