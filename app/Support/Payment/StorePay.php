<?php
namespace App\Support\Payment;

use App\Support\Payment\Interfaces\PaymentInterface;

class StorePay implements PaymentInterface
{
    protected static $config;
    
    public static function setup($config)
    {
        $defaultConfig = [
            'hash_key'             => '',
            'hash_iv'              => '',
            'merchant_id'          => '',
            'return_url'           => '',
            'back_url'             => '',
            'result_url'           => '',
            'no_third_process_url' => '',
            'payment_method'       => '',
        ];
        
        $config       = array_merge($defaultConfig, $config);
        self::$config = $config;  
    }
    
    public static function sendOrder($order)
    {
        return redirect(self::$config['storepay_url']);
    }
    
    public static function getFeedback()
    {

    }
    
    public static function getMerchantTradeNo($request)
    {
        return null;
    }
    
    public static function returnResult($success = true, $message = '')
    {
        
    }
    
    public static function newMerchantTradeNo()
    {
        return 'SPAY'.date('Ymd').sprintf('%06d', rand(1, 99999));
    }
    
    public static function queryOrder($tradeNo, $amt = 0)
    {
        
    }
    
    public static function noIsThePaymentType($tradeNo)
    {
        return preg_match('/^SPAY[0-9]{14}$/i', $tradeNo) || preg_match('/^SPAY[0-9]{14}0[1-9]$/i', $tradeNo);
    }
}