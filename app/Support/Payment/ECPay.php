<?php
namespace App\Support\Payment;

use App\Support\Payment\Interfaces\PaymentInterface;

class ECPay implements PaymentInterface
{
    protected static $config;
    
    public static function setup($config)
    {
        $defaultConfig = [
            'hash_key'       => '',
            'hash_iv'        => '',
            'merchant_id'    => '',
            'return_url'     => '',
            'back_url'       => '',
            'result_url'     => '',
            'payment_method' => 'credit',
            'staging_number' => '3,6',
        ];
        
        $config       = array_merge($defaultConfig, $config);
        self::$config = $config;  
    }
    
    public static function sendOrder($order)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Payment.Integration.php');
        
        $useStaging    = false;
        $paymentMethod = \ECPay_PaymentMethod::Credit;
        switch (self::$config['payment_method']) {
            case 'credit':
                $paymentMethod = \ECPay_PaymentMethod::Credit;
                break;
            case 'staging':
                $useStaging    = true;
                $paymentMethod = \ECPay_PaymentMethod::Credit;
                break;
            case 'atm':
                $paymentMethod = \ECPay_PaymentMethod::ATM;
                break;
        }

        try
        {
            $serviceUrl = env('PAYMENT_IS_TEST', true) ? 'https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5' : 'https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5';
            
            $oPayment = new \ECPay_AllInOne();
            /* 服務參數 */
            $oPayment->ServiceURL                = $serviceUrl;
            $oPayment->HashKey                   = trim(self::$config['hash_key']);
            $oPayment->HashIV                    = trim(self::$config['hash_iv']);
            $oPayment->MerchantID                = trim(self::$config['merchant_id']);
            /* 基本參數 */
            $oPayment->Send['ReturnURL']         = self::$config['notify_url'];
            $oPayment->Send['ClientBackURL']     = self::$config['back_url'];
            $oPayment->Send['OrderResultURL']    = self::$config['result_url'];
            $oPayment->Send['MerchantTradeNo']   = $order['merchant_trade_no'];
            $oPayment->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');
            $oPayment->Send['TotalAmount']       = $order['total'];
            $oPayment->Send['TradeDesc']         = $order['desc'];
            $oPayment->Send['ChoosePayment']     = $paymentMethod;
            $oPayment->Send['Remark']            = $order['remark'];

            if ($paymentMethod == \ECPay_PaymentMethod::Credit) {  
                $oPayment->EncryptType                     = '1';
                $oPayment->SendExtend['CreditInstallment'] = 0;       //分期期數，預設0(不分期)
                $oPayment->SendExtend['InstallmentAmount'] = $order['total'];       //使用刷卡分期的付款金額，預設0(不分期)
                $oPayment->SendExtend['Redeem']            = false ;  //是否使用紅利折抵，預設false
                $oPayment->SendExtend['UnionPay']          = false;   //是否為聯營卡，預設false;
            } elseif ($paymentMethod == \ECPay_PaymentMethod::ATM) {
                $oPayment->SendExtend['ClientRedirectURL'] = self::$config['result_url'];
            }
            
            // 分期付款設定
            if ($useStaging) {
                $oPayment->SendExtend['CreditInstallment'] = self::$config['staging_number']; 
                //$oPayment->SendExtend['InstallmentAmount'] = ceil($order['total'] / $order['installments']);
            }
            
            // 加入選購商品資料。
            $items = [];
            if (isset($order['items'])) {
                foreach ($order['items'] as $item) {
                    $items[] = [
                        'Name'     => $item['prod_name'].(empty($item['size']) ? '' : '-'.$item['size']),
                        'Price'    => $item['price'],
                        'Currency' => "NTD",
                        'Quantity' => $item['qty'],
                        'URL'      => ""
                    ];    
                }    
            }
            $oPayment->Send['Items'] = $items;
            /*
                array('Name' => "[產品A]", 'Price' => (int)"[單價]",'Currency' => "[幣別]", 'Quantity' => (int) "[數量]", 'URL' => "[產品說明位址]"));
                */

            /* 產生訂單 */
            $oPayment->CheckOut();
            /* 產生產生訂單 Html Code 的方法 */
            $szHtml = $oPayment->CheckOutString();
        }
        catch (Exception $e)
        {
            // 例外錯誤處理。
            throw $e;
        }
    }
    
    public static function getFeedback()
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Payment.Integration.php');
        
        $oPayment          = new \ECPay_AllInOne();
        $oPayment->HashKey = trim(self::$config['hash_key']);
        $oPayment->HashIV  = trim(self::$config['hash_iv']);

        if (self::$config['payment_method'] == 'credit' || self::$config['payment_method'] == 'staging') {
            $oPayment->EncryptType = '1';
        } else {
            $oPayment->EncryptType = '0';
        }

        $data = $oPayment->CheckOutFeedback();

        $result = (isset($data['BankCode']) && isset($data['vAccount']) && $data['RtnCode'] == 2) || $data['RtnCode'] == 1;
        return [
            'result'          => $result,
            'MerchantTradeNo' => $data['MerchantTradeNo'],
            'TradeNo'         => $data['MerchantTradeNo'],
            'BankCode'        => isset($data['BankCode']) ? $data['BankCode'] : '',
            'CodeNo'          => isset($data['vAccount']) ? $data['vAccount'] : '',
        ];
    }
    
    public static function getMerchantTradeNo($request)
    {
        return $request->MerchantTradeNo;
    }
    
    public static function returnResult($success = true, $message = '')
    {
        return $success ? '1|OK' : '0|付款失敗';
    }
    
    public static function newMerchantTradeNo()
    {
        return 'ECP'.date('Ymd').sprintf('%06d', rand(1, 99999));
    }
    
    public static function queryOrder($tradeNo, $amt = 0)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Payment.Integration.php');
        
        $serviceUrl = env('PAYMENT_IS_TEST', true) ? 'https://payment-stage.ecpay.com.tw/Cashier/QueryTradeInfo/V5' : 'https://payment.ecpay.com.tw/Cashier/QueryTradeInfo/V5';
        
        $oPayment = new \ECPay_AllInOne();
        
        /* 服務參數 */
        $oPayment->ServiceURL               = $serviceUrl;
        $oPayment->HashKey                  = trim(self::$config['hash_key']);
        $oPayment->HashIV                   = trim(self::$config['hash_iv']);
        $oPayment->MerchantID               = trim(self::$config['merchant_id']);
        $oPayment->Query['MerchantTradeNo'] = $tradeNo;
        
        $result = $oPayment->QueryTradeInfo();
        if (!empty($result['TradeNo'])) {
            $paymentStatus = 0;
            switch ($result['TradeStatus']) {
                case 1:
                    $paymentStatus = 1;     // 已付款
                    break;
                case 'v342':
                    $paymentStatus = 99;    // 付款逾期
                    break;
            }
            return [
                'products'      => $result['ItemName'],
                'paymentTime'   => $result['PaymentDate'],
                'paymentStatus' => $paymentStatus,
            ];
        }
        
        return null;
    }
    

    public static function noIsThePaymentType($tradeNo)
    {
        return preg_match('/^ECP[0-9]{14}$/i', $tradeNo) || preg_match('/^ECP[0-9]{14}0[1-9]$/i', $tradeNo);
    }
}