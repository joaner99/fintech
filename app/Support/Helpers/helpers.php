<?php
function ArrayToLaravelFormSelectOption($Data)
{
    $array = array();
    foreach ($Data as $key => $value) {
         $array = array_add($array, $Data[$key]->id, $Data[$key]->name);

    }

    return $array;
}
function ArrayToSelectOption($Data)
{
    $Option ="";
    foreach ($Data as $key => $value) {
        $Option .= "<option value=" . $Data[$key]->id . ">" . $Data[$key]->name . "</option>";
    }
    return $Option;
}
function DataTableJs($Id = "tableWithSearch", $BodyArray, $ModelData, $Option = 1, $ControllerName = 1, $Print = 1, $Hand = 1)
{

    ini_set('memory_limit', '256M');
    $NewModelData = "";
    $Data         = "";
    $Data .= '<table class="table table-striped table-hover dt-responsive" id="' . $Id . '">';
    $Data .= "</table>";
    foreach ($ModelData as $key => $value) {
        $Str = "";
        foreach ($value as $key2 => $value2) {
            $Str .= "'" . str_replace("\r\n","",str_replace("'","’",$value->$key2)) . "',";
        }

        if ($Option != 1) {
            $Str .= DataTableJsOption($Option, $value, $ControllerName);
        }
        $NewModelData .= '[' . $Str . '],';
    }

    //下面開始是js
    $Data .= "";
    $Data .= '<script>';
    $Data .= '$(document).ready(function(){';
    $Data .= 'var opt={';
    $Data .= '"bAutoWidth": false,';
    $Data .= '"aaSorting":[],';
    $Data .= '"aoColumns":[';

    foreach ($BodyArray as $key => $value) {
        //,"bVisible": false,"bSearchable": false
        $Data .= '{"sTitle":"' . $BodyArray[$key]['title'] . '","sWidth": "' . $BodyArray[$key]['width'] . '%"},';
    }

    $Data .= '],"aaData":[' . $NewModelData . '],';
    if ($Print != 1) {
        $Data .= "dom: 'Bfrtip',";
        $Data .= "buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],";
    }
    if ($Hand != 1) {
        $Data .= "searching: false,";
        $Data .= "paging: false,";
        $Data .= "bInfo : false";
    }
    $Data .= '};';
    $Data .= '$("#' . $Id . '").dataTable(opt)';
    $Data .= '});';
    $Data .= '</script>';

    return $Data;
}

function FormTtile($LayoutTitle, $Title, $Body)
{
    $Data = "";
    $Data .= '<div class="container-fluid container-fixed-lg">';
    $Data .= '  <div class="panel panel-transparent">';

    $Data .= '<div class="panel-body">';
    $Data .= '<div class="row">';
    $Data .= '<div class="col-sm-10">';
    $Data .= "<h3>" . $Title . "</h3>";
    $Data .= "<p>";
    $Data .= $Body;
    $Data .= "</p>";
    $Data .= "<br>";

    return $Data;
}

function FormBody($BodyList, $Url, $Status = "",$method)
{
    $Data = "";
    $Data .= Form::open(array('url' => $Url, 'files' => 'true', 'class' => 'form-horizontal', 'files' => true,'method'=>$method));
    //echo print_r($BodyList);exit;
    foreach ($BodyList as $key => $value) {
        if($value["Type"] == 'Hidden'){
            $Data .= ByCase($value);
        }else{
            $Data .= '<div class="form-group">';
            $Data .= '<label for="fname" class="col-sm-3 control-label">';
            $Data .= $BodyList[$key]['CNname'];
            $Data .= "</label>";
            $Data .= '<div class="col-sm-9">';
            $Data .= ByCase($value);
            $Data .= '</div>';
            $Data .= '</div>';
        }
        
    }
    $Data .= '<div class="row">';
    $Data .= '<div class="col-sm-3">';
    $Data .= '<p>';
    $Data .= '送出前，請詳細檢查是否有遺漏的資料';
    $Data .= '</p>';
    $Data .= '</div>';
    $Data .= '<div class="col-sm-9">';
    $Data .= FormBodyStatus($Status);
    $Data .= '</button>';
    $Data .= '</div>';
    $Data .= '</div>';
    $Data .= Form::close();
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";
    $Data .= "</div>";

    return $Data;
}

function ByCase($Value)
{
    $Data = "";
    switch ($Value["Type"]) {
        case 'Email':
            $Data .= Form::email($Value['name'], $Value["Value"], ['class' => $Value['class'], 'placeholder' => $Value['placeholder'], 'required' => $Value['Option']]);
            break;
        case 'Select':
            if ($Value['Option'] == null) {
                $Data .= Form::select($Value['name'], array('' => ''), 'default', array('id' => $Value["id"], 'class' => $Value['class'],$Value['readonly']));
            } else {
                $Data .= Form::select($Value['name'], $Value['Option'], $Value["Value"], array('id' => $Value["id"], 'class' => $Value['class'],$Value['readonly']));
            }
            break;
        case 'Number':
            $Data .= Form::input('number', $Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            //$Data .= '<input value="' . $Value['Value'] . '"' . $Value['Option'] . ' type="text" class="' . $Value['class'] . '" id="' . $Value['id'] . '" placeholder="' . $Value['placeholder'] . '" " name="' . $Value['name'] . '">';
            break;
        case 'Text':
            $Data .= Form::text($Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'], $Value['Option'], 'id' => $Value['id']));
            //$Data .= '<input value="' . $Value['Value'] . '"' . $Value['Option'] . ' type="text" class="' . $Value['class'] . '" id="' . $Value['id'] . '" placeholder="' . $Value['placeholder'] . '" " name="' . $Value['name'] . '">';
            break;
        case 'Password':
            $Data .= Form::password($Value['name'], array('class' => $Value['class'], 'placeholder' => $Value['placeholder'], $Value['Option'], 'id' => $Value['id']));
            break;
        case 'Textarea':
            $Data .= Form::textarea($Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            break;
        case 'Hidden':
            $Data .= Form::hidden($Value['name'], $Value["Value"], array('class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            break;
        case 'checkbox':
            $Str = "0" . $Value['Value'];
            foreach ($Value['Option'] as $key => $value) {
                $Data .= Form::checkbox($Value['name'], $Value['Option'][$key], (strpos($Str, $Value['Option'][$key]) > 0) ? true : false);
                $Data .= Form::Label($Value['Option'][$key]);
            }
            break;
        case 'radio':
            foreach ($Value['Option'] as $key => $value) {
                $Data .='<label>'.$value.'</label>';
                $Data .= Form::radio($Value['name'], $key,($Value['Value']==$key)?'true':'');
            }
            break;
        case 'FileBox':
            $Data .= '<div class="fallback">';
            $Data .= Form::file($Value['name']);
            $Data .= '</div>';
            $Str="";
            if(!empty($Value['Value']) & $Value['Option']!="url"){
                if(!is_file($Value['Path'] . $Value['Value'])){
                    $Data .=  getImagerLightbox('images/no_image_thumb.gif',300);
                }else{
                    $Data .=  getImagerLightbox($Value['Path'] . $Value['Value'],300);
                }
                $Str="請注意只能上傳'jpg'、'jpeg'、'png'格式，大小限制1MB";
            }elseif(!empty($Value['Value']) & $Value['Option']=="url"){
                $Data .=  '<a href="'.$Value['Path']. $Value['Value'].'">請下載</a>';
                $Str="";
            }else{
                $Data .=  getImagerLightbox('images/no_image_thumb.gif',300);
            }
            $remark = '';
            if($Value['placeholder'] != ''){
               $remark= "<font color='red'>備註：".$Value['placeholder']."</font>";
            }
            $Data .= "<div style='margin-top: 2px;'>".$remark.$Str."</div>";
            break;
        case 'InputDate':
            $Data .= '<div id="datepicker-component" class="input-group date col-sm-8">';
            $Data .= Form::text($Value['name'], $Value["Value"], array('id' => $Value['id'], 'class' => $Value['class'], 'placeholder' => $Value['placeholder']));
            $Data .= '<span class="input-group-addon">';
            $Data .= '<i class="fa fa-calendar"></i></span>';
            $Data .= '</div>';
            $Data .='<script>';
            $Data .='$(function() {';
            $Data .='$.fn.datepicker.defaults.format = "yyyy-mm-dd";';
            $Data .='showSecond: true,';
            $Data .='$("#'.$Value['id'].'").datepicker();';
            $Data .='});';
            $Data .='</script>';
            break;
        case 'InputTime':
            $Data .= '<div id="timepicker-component" class="time ui-timepicker-input">';
            $Data .= '<input type="time" name="'.$Value['name'].'" class="'.$Value['class'].'" value="'.$Value['Value'].'">';
            $Data .= '</div>';
            $Data .='<script>';
            $Data .='$(function() {';
            $Data .='$("#'.$Value['id'].'").timepicker();';
            $Data .='});';
            $Data .='</script>';

            break;
        case 'Label':
            $Data .= '<div class="input-group date col-sm-8">';
            $Data .= '<Label id="'.$Value['id'].'" name="'.$Value['name'].'" class="'.$Value['class'].'">';
            $Data .= $Value['Value'];
            $Data .= '</Label>';
            $Data .= '</div>';
            break;
        case 'EmptyRow':
            $Data .= '<div id="'.$Value['id'].'" name="'.$Value['name'].'" class="input-group date col-sm-8"></div>';
            break;
        
    }

    return $Data;
}
//DataTableOption中的Check判斷
function DataTableCheck($value, $ControllerName)
{
    //Additional purchase
    $Str = "";
    $Str .= '<a href="BackAdditionalPurchase?id=' . $value->id . '" class="btn btn-danger">加購</a>';
    return $Str;
}
function FormBodyStatus($Status=7)
{   
    $Data = '';
    switch ($Status) {
        case '0':
            $Data .= '<button class="btn btn-success" type="submit" disabled>';
            $Data .= '送審中';
            break;
        case '1':
            $Data .= '<button class="btn btn-success" type="submit" disabled>';
            $Data .= '審核完畢';
            break;
        case '2':
            $Data .= '<button class="btn btn-success" type="submit">';
            $Data .= '修改';
            break;
        case '3':
            $Data .= '<button class="btn btn-success" type="submit" >';
            $Data .= '請求發布';
            break;
        default:
            $Data .= '<button class="btn btn-success" type="submit">';
            $Data .= '送出';
            break;
    }
    return $Data;
}
function DataTableJsOption($Option, $data, $ControllerName)
{
    $Str = "'";
    foreach ($Option as $key => $value) {
        if ($Option[$key] != 1) {
            continue;
        }
        switch ($key) {
            case 'Img':
                break;
            case 'Add':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"create"])) {
                    $Str .= '<a href="' . $ControllerName . 'Add?id=' . $data->id . '"class="btn btn-tag">新增</a>';
                }
                break;
            case 'Edit':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '/edit "class="btn btn-info" title="修改" style="margin-right: 10px;"><i class="fa fa-pencil"></i></a>';
                }
                break;
            case 'Read':
                if(array_key_exists('ReadHint', $Option))
                    $hint = $Option['ReadHint'];
                else
                    $hint = '詳細內容';

                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . asset($ControllerName . 'Read/' . $data->id) . '"class="btn btn-primary" title="'.$hint.'" style="margin-right: 10px;"><i class="fa fa-align-justify"></i></a>';
                }
                break;
            case 'ReadDetail':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . $ControllerName . 'ReadDetail?id=' . $data->id . '"class="btn btn-primary" title="詳細內容" style="margin-right: 10px;"><i class="fa fa-align-justify"></i></a>';
                }
                break;
            case 'Delete':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"delete"])) {
                    $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '" class="btn btn-success ask-delete jquery-postback"  data-method="DELETE"  title="刪除" style="margin-right: 10px;"><i class="fa fa-trash-o"></i></a>';
                }
                break;
            case 'DeleteApi':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"delete"])) {
                    $Str .= '<a href="' . asset($ControllerName . '/' . $data->id ). '" class="btn btn-success ask-delete  jquery-postback"  data-method="DELETE"  title="清除API" style="margin-right: 10px;"><i class="fa fa-trash-o"></i></a>';
                }
                break;
            case 'Check':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"update"])) {
                    (!empty($data->start)) ? $Str .= DataTableCheck($data,$ControllerName) : "Exit";
                }
                break;
            case 'Detail':
                if (Gate::forUser(auth('admin')->user())->allows ('ACL_view', [Route::currentRouteName(),"read"])) {
                    $Str .= '<a href="' . $ControllerName . 'ItemDetail?id=' . $data->id . '" class="btn btn-success" title="商品名細" style="margin-right: 10px;"><i class="fa fa-table"></i></a>';
                }
                break;
            case 'Shelf':
                $Str .= '<a href="BackWareHouseShelf?wh_id=' . $data->id . '" class="btn btn-success" title="儲位編輯" style="margin-right: 10px;"><i class="fa fa-table"></i></a>';
                break;
            case 'RecvBody':
                $Str .= '<a href="BackRecvBody?form_id=' . $data->id . '" class="btn btn-success" title="進貨商品" style="margin-right: 10px;"><i class="fa fa-table"></i></a>';
                break;
            case 'Down':
                $Str .= '<a href="' . $ControllerName . 'Down?id=' . $data->id . '" class="btn btn-success" title="下載原始檔案" style="margin-right: 10px;"><i class="fa fa-cloud-download"></i></a>';
                break;
            case 'Excel':
                $Str .= '<a href="' . $ControllerName . 'Excel?id=' . $data->id . '" class="btn btn-info" title="Excel" style="margin-right: 10px;"><i class="fa fa-file-excel-o"></i></a>';
                break;
        }
    }
    $Str .= "',";
    return $Str;
}
/**
 * 获取当前控制器名
 *
 * @return string
 */
function getCurrentControllerName()
{
    return getCurrentAction()['controller'];
}

/**
 * 获取当前方法名
 *
 * @return string
 */
function getCurrentMethodName()
{
    return getCurrentAction()['method'];
}

/**
 * 获取当前控制器与方法
 *
 * @return array
 */
function getCurrentAction()
{
    $action = \Route::current()->getActionName();
    list($class, $method) = explode('@', $action);

    return ['controller' => $class, 'method' => $method];
}

function getImagerLightbox($url,$width=200)
{
    $str='<a href="'.asset($url).'" data-lightbox="roadtrip">';
    $str.=Html::image($url,'',array('style'=>'width:'.$width.'px'));
    $str.='</a>';

    return $str;
}

function copyS3Folder($path, $newPath)
{
    $path    = rtrim($path, '/\\');
    $newPath = rtrim($newPath, '/\\');
    
    try {
        if (env('UPLOAD_TO_S3', false)) {
            $s3     = Storage::disk('s3');
            $images = $s3->allFiles('Letsgo/'.$path);

            foreach ($images as $image) {
                $s3->copy($image, 'Letsgo/'.$newPath.'/'.basename($image), 'public');    
            }
        } else {
            if (!is_dir(public_path().'/'.$newPath)) {
                mkdir(public_path().'/'.$newPath);    
            }
            if (is_dir(public_path().'/'.$path)) {
                $files = scandir(public_path().'/'.$path);
                foreach ($files as $file) {
                    if ($file != "." && $file != "..") {
                        copy(public_path().'/'.$path.'/'.$file, public_path().'/'.$newPath.'/'.$file);
                    }
                }
            }
        }
        return true;
    } catch (Exception $e) {
        return false;
    }
}

// 上傳處理
function getUploadFile($path, $file, $oldFile = '',$width,$height,$q=85,$noRemove=false)
{
    $random_sn = date('ymdHis').rand(11111, 99999); 
    if (!empty($oldFile)) {
        if (is_file($path.$oldFile)) {
            unlink($path.$oldFile); 
        }
    }
    $fileName = $random_sn.".".$file->getClientOriginalExtension();
    $file->move($path, $fileName);
    
    if (strtolower($file->getClientOriginalExtension()) != 'gif') {
        $image = new \App\Support\Image\Image($path.'/'.$fileName);
        $image->maxSize($width, $height);
        $image->save($path.'/'.$fileName, $q);
    }
    
    if (env('UPLOAD_TO_S3', false)) {
        Storage::disk('s3')->put('Letsgo/'.$path.'/'.$fileName, file_get_contents($path.'/'.$fileName), 'public');
        if (env('DELETE_LOCAL_S3_FILES', false) && $noRemove) {
            unlink($path.'/'.$fileName);
        }
    }
    /*chmod($path.'/'.$fileName, 0777);
    $img = Image::make($path.'/'.$fileName)->resize($width, null, function ($constraint){
        $constraint->aspectRatio();
        $constraint->upsize();
    });
    $img->save($path.'/'.$fileName,$q);
    */
    return $fileName;
}


function getDataTableImages($Data,$path)
{
    foreach($Data as $idx => $keys){
        foreach ($keys as $key => $value) {
            if ((false !== ($rst = strpos($value,'jpg')))||(false !== ($rst = strpos($value,'gif')))||(false !== ($rst = strpos($value,'jpeg')))||(false !== ($rst = strpos($value,'png')))||(false !== ($rst = strpos($value,'bmp')))) {
                $filename = $Data[$idx]->$key;
                if(!is_file("Upload/".$path."/".$filename)){
                    $Data[$idx]->$key = '<img src="images/no_image_thumb.gif" style=" width:50px;" />';
                }else{
                    $Data[$idx]->$key = getImagerLightbox("Upload/".$path."/".$filename,50);
                }    
            }
        }
    }
    return $Data;
}




function OrmDelete($Model){
    if ($Model->trashed()) {
        $Model->restore();
         return response()->json(0);
    }else{
        $Model=ImgRotation::withTrashed()->where('id',$id)->delete();
         return response()->json(1);
    }
}
function RequestForeach($data,$Path){
    foreach ($data as $key => $value) {
        switch (gettype($data[$key])) {
            case 'string':
                if($key=="password"){
                    if (Hash::needsRehash($data[$key]))
                    {
                        $data[$key] = Hash::make($data[$key]);
                    }
                }
                break;
            case 'object':
                $data[$key]=getUploadFile($Path,$data[$key]);
                break;
            case 'integer':
                break;
            default:
                if(empty($data[$key])){
                    $data[$key]=$data[$key];
                }else{
                    $data[$key]=implode(",",$data[$key]);
                }
                break;
        }
    }
    return $data;
}

function SearchRequestForeach($data){
    $Str="";
    foreach ($data as $key => $value) {
        switch (gettype($data[$key])) {
            case 'string':
               if($key!="_token" and $key!="_method" and $key!="id"){
                $Str.=$data[$key].",";
               }
                break;
            case 'object':
                break;
            default:
                # code...
                break;
        }
    }
    return substr($Str,0,-1);
}

function SearchData($data,$id,$RouteName){
    $Search =\App\Models\Search\Search::firstOrCreate(['search_id' => $id,'controller'=>$RouteName]);
    $Search->string=SearchRequestForeach($data);
    $Search->save();
}
function mail_action($request,$toEmail){
    foreach ($request as $key => $value) {
        $mail[$key] = $value;
        }
        $mail['toEmail'] = $toEmail;
        //去除token
        unset($mail['_token']);
        unset($mail['id']);
        Mail::send('reception.reception.email_connection', ['mail' => $mail], function ($message) use ($mail) {
            $message->from($mail['receivers']);
            $message->to($mail['receivers'])->subject($mail['content']);
        });
}
//審核機制用
function FormApprove($user,$status,$event_log='',$Data){
    $audit = array(1 => "通過" ,2=>'退回',3=>'管理員禁止發佈');
   if ($user->role_id==2 || $user->role_id==3) {
        $BodyArray[]=array('id' => 'approve', 'name' => 'approve', 'CNname' => '審核：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' =>   $audit, 'Type' => 'radio', "Value" => (is_object($Data)) ? $Data['approve'] : null);
        $BodyArray[]=array('id' => 'comment', 'name' => 'comment', 'CNname' => '審核意見：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Textarea', "Value" => (is_object($event_log)) ? $event_log->comment : null);
    }else{
        $BodyArray[]=array('id' => 'approve', 'name' => 'approve', 'CNname' => '審核：', 'placeholder' => "", 'class' => "form-control", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? 0 : 0);
        $BodyArray[]=array('id' => 'comment', 'name' => 'comment', 'CNname' => '審核意見：', 'placeholder' => "", 'class' => "ckeditor", 'readonly' => 'readonly', 'Option' => '', 'Type' => 'Hidden', "Value" => (is_object($Data)) ? $user->name.'遞呈，請參閱' :$user->name.'遞呈，請參閱');

    }
    return $BodyArray;

}
//紀錄審核LOG
function ApproveLog($ModelName,$id,$request,$ModelName2,$table_name){
    $table_name=$table_name.'_id';
    if($ModelName::withTrashed()->find($id)->approve!=$request['approve']){
        $ModelName2=new $ModelName2;
        $ModelName2->user_id=$request['user_id'];
        $ModelName2->comment=$request['comment'];
        $ModelName2->$table_name=$request['id'];
        $ModelName2->save();
    }
}

// 取得使用者的圖庫 ID
function getGalleryId($userId)
{
    return md5('gallery_'.$userId);
}

// 檢查是不是自訂網域
function checkIsCustomDomain()
{
    $domains = explode(',', env('SUNSTAR_DOMAINS', 'www.twletsgo.com,gogolol.twletsgo.com,letsgo.local,letsgo.local,www.freeon.shop'));
    foreach ($domains as $domain) {
        if (trim($domain) == trim(Request::server('SERVER_NAME'))) {
            return false;    
        }    
    }
    return true;
}

// 檢查 FB 的程式碼是不是正確
function checkFBCode($code)
{
    $code = preg_replace('/<!--(.*)-->/i', '', $code);
    $code = preg_replace('/ \/\/ (.*)\r/i', '', $code);
    $code = preg_replace('/ \/\/ (.*)\n/i', '', $code);
    $code = str_replace("\r", '', $code);
    $code = str_replace("\n", '', $code);
    $code = str_replace("\t", '', $code);
    $code = str_replace(' ', '', $code); 

    return preg_match('/<script>!function\(f,b,e,v,n,t,s\)\{if\(f\.fbq\)return;n=f\.fbq=function\(\)\{n\.callMethod\?n\.callMethod\.apply\(n,arguments\):n\.queue\.push\(arguments\)\};if\(!f\._fbq\)f\._fbq=n;n\.push=n;n\.loaded=!0;n\.version=\'2\.0\';n\.queue=\[\];t=b\.createElement\(e\);t\.async=!0;t\.src=v;s=b\.getElementsByTagName\(e\)\[0\];s\.parentNode\.insertBefore\(t,s\)\}\(window,document,\'script\',\'https:\/\/connect\.facebook\.net\/en_US\/fbevents\.js\'\);fbq\(\'init\',\'[0-9]*\'\);fbq\(\'track\',\'PageView\'\);<\/script><noscript><imgheight="1"width="1"style="display:none"src="https:\/\/www\.facebook\.com\/tr\?id=[0-9]*&ev=PageView&noscript=1"\/><\/noscript>/i', $code) || preg_match('/<script>!function\(f,b,e,v,n,t,s\)\{if\(f\.fbq\)return;n=f\.fbq=function\(\)\{n\.callMethod\?n\.callMethod\.apply\(n,arguments\):n\.queue\.push\(arguments\)\};if\(!f\._fbq\)f\._fbq=n;n\.push=n;n\.loaded=!0;n\.version=\'2\.0\';n\.queue=\[\];t=b\.createElement\(e\);t\.async=!0;t\.src=v;s=b\.getElementsByTagName\(e\)\[0\];s\.parentNode\.insertBefore\(t,s\)\}\(window,document,\'script\',\'https:\/\/connect\.facebook\.net\/en_US\/fbevents\.js\'\);fbq\(\'init\',\'[0-9]*\',\{em:\'insert_email_variable,\'\}\);fbq\(\'track\',\'PageView\'\);<\/script><noscript><imgheight="1"width="1"style="display:none"src="https:\/\/www\.facebook\.com\/tr\?id=[0-9]*&ev=PageView&noscript=1"\/><\/noscript>/i', $code);
}

// 檢查 GA 的程式碼是不是正確
function checkGACode($code)
{
    $code = preg_replace('/<!--(.*)-->/i', '', $code);
    //$code = preg_replace('/ \/\/ (.*)\r/i', '', $code);
    //$code = preg_replace('/ \/\/ (.*)\n/i', '', $code);
    $code = str_replace("\r", '', $code);
    $code = str_replace("\n", '', $code);
    $code = str_replace("\t", '', $code);
    $code = str_replace(' ', '', $code);        
    
    return preg_match('/<scriptasyncsrc="https:\/\/www\.googletagmanager\.com\/gtag\/js\?id=[^"\']*"><\/script><script>window\.dataLayer=window\.dataLayer\|\|\[\];functiongtag\(\)\{dataLayer\.push\(arguments\)\};gtag\(\'js\',newDate\(\)\);gtag\(\'config\',\'[^"\']*\'\);<\/script>/i', $code);
}

// 取得檔案大小的格式
function getByteFormat($bytes, $precision = 1)
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

    if ($bytes < 1024) {
        return '0 MB';
    }

    if ($bytes < (1024 * 1024)) {
        return round($bytes / (1024 * 1024), 2).' MB';    
    }

    $bytes = max($bytes, 0); 
    $pow   = floor(($bytes ? log($bytes) : 0) / log(1024)); 
    $pow   = min($pow, count($units) - 1); 

    return round($bytes / pow(1024, $pow), $precision) . ' ' . $units[$pow]; 
}

// 轉換成粉絲團網址
function convertToFansUrl($clubId, $postId, $commentId = '')
{
    if (strpos($postId, '_') !== false) {
        $tmp    = explode('_', $postId);
        $postId = $tmp[1];
    }
    if (strpos($commentId, '_') !== false) {
        $tmp       = explode('_', $commentId);
        $commentId = $tmp[1];
    }

    $url = 'https://www.facebook.com/permalink.php?story_fbid='.$postId.'&id='.$clubId;
    if (!empty($commentId)) {
        $url .= '&comment_id='.$commentId; 
    }
    return $url;
}

// 轉換成社團網址
function convertToGroupUrl($clubId, $postId, $commentId = '')
{
    if (strpos($postId, '_') !== false) {
        $tmp    = explode('_', $postId);
        $postId = $tmp[1];
    }
    if (strpos($commentId, '_') !== false) {
        $tmp       = explode('_', $commentId);
        $commentId = $tmp[1];
    }

    $url = 'https://www.facebook.com/groups/'.$clubId.'/permalink/'.$postId;
    if (!empty($commentId)) {
        $url .= '?comment_id='.$commentId; 
    }
    return $url;
}

// 取得資源的路徑
function s3Asset($path)
{
    if (is_file(public_path().'/'.$path) || !env('UPLOAD_TO_S3', false)) {
        return asset($path);   
    }
    
    return 'https://cdn.twletsgo.com/Letsgo/'.$path;
}

// 取得S3的檔案大小
function s3Filesize($path)
{
    if (is_file(public_path().'/'.$path) || !env('UPLOAD_TO_S3', false)) {
        return filesize(public_path().'/'.$path);   
    }
    
    return Storage::disk('s3')->size('Letsgo/'.$path, 'public');
}