<?php

namespace App\Support\MobileValidator;

use Session;

class MobileValidator
{
    public static function newVerify($county, $phone)
    {
        $code    = sprintf('%05d', rand(0, 99999)); 
        $expired = strtotime('+5 minutes');
        Session::put('mobile_verify_code', $code);
        Session::put('mobile_verify_phone', $phone);
        Session::put('mobile_verify_expired', $expired);
        
        return [
            'code'    => $code,
            'expired' => $expired,
            'phone'   => $phone,
        ];
    }
    
    public static function verify($verifyPhone, $verifyCode)
    {
        $code    = Session::get('mobile_verify_code', null); 
        $phone   = Session::get('mobile_verify_phone', null); 
        $expired = Session::get('mobile_verify_expired', null);
        
        if (is_null($verifyCode) || is_null($expired) || is_null($verifyPhone)) {
            return false;
        }

        if ($phone != $verifyPhone) {
            return false;    
        }
        
        if ($expired < time()) {
            return false;
        }
        
        return ($verifyCode == $code);
    }
    
    public static function sendCode($county, $phone)
    {
        $verify  = self::newVerify($county, $phone);
        $message = sprintf('這是您的全國軍公教福利中心手機驗證碼%s，該驗證碼將於%s之後失效。', $verify['code'], date('Y-m-d H:i:s', $verify['expired']));
        $url     = 'http://smsmo.smse.com.tw/STANDARD/SMS_FU.ASP';
        $phone   = $county.trim(preg_replace('/^0/i', '', str_replace('-', '', $phone)));
        $params  = [
            'yhy'     => env('SMS_SMSE_USER', ''),
            'dc2a'    => env('SMS_SMSE_PWD', ''),
            'movetel' => $phone,
            'bf'      => date('Y/m/d H:i:s'),
            'sb'      => mb_convert_encoding($message, 'BIG5', 'UTF-8'),
        ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = mb_convert_encoding(curl_exec($ch), 'UTF-8', 'BIG5');
        curl_close($ch);
        return preg_match('/^OK/i', $result);
    }
}
