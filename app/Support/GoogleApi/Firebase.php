<?php

namespace App\Support\GoogleApi;

class Firebase
{
    public static function send($json)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://fcm.googleapis.com/fcm/send");
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            'Authorization:key='.env('GOOGLE_FCM_AUTHORIZATION_KEY')
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json); 
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }
}
