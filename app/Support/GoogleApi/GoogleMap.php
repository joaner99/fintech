<?php

namespace App\Support\GoogleApi;

class GoogleMap
{
    public static function getLocationByAddress($address)
    {
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key='.env('GOOGLE_MAP_API_KEY');
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($json);
        
        if (isset($result->status)) {
            if ($result->status == 'OK') {
                return $result->results[0]->geometry->location;
            }
        }
        
        return null;
    }
}
