<?php

namespace App\Support\GoogleApi;

class ShortUrl
{
    public static function get($address)
    {
        $url = 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyAQAF5IP6ZO_IGvdW-0FgH47opsf-KK8PQ';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['longUrl' => $address]));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($json);

        if (isset($result->id)) {
            return $result->id;
        }
        
        return '';
    }
}
