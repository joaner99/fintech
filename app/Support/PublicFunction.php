<?php
/*
 * jQuery File Upload Plugin PHP Class
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/**
*計算運費
*para1:Location ID
*para2:購物車
*地區：台灣=0
*類型: 郵局掛號:1/宅配:2/貨到付款:3
*/
function CountDeliFee($LocationId,$way,$cart){
	$fee = 0;
	//台灣地區
	if($LocationId == 0){
		//計算訂單總金額
		$TotalPrice = 0;
        foreach($cart as $idx => $keys){
            $TotalPrice += $keys['amount']*$keys['UnitPrice'];
        }

		switch ($way) {
			case 1:
				$sql = 'select post_office_less_amount,post_office_less_amount_fee,post_office_over_amount,post_office_over_amount_fee from domestic_ship where id=1';
				$rlt = DB::select($sql);
				if($TotalPrice < $rlt[0]->post_office_less_amount)
					$fee = $rlt[0]->post_office_less_amount_fee;
				elseif($TotalPrice > $rlt[0]->post_office_over_amount)
					$fee = $rlt[0]->post_office_over_amount_fee;
				break;
			case 2:
				$sql = 'select home_deli_below_amount,home_deli_below_amount_fee,home_deli_over_amount,home_deli_over_amount_fee from domestic_ship where id=1';
				$rlt = DB::select($sql);
				if($TotalPrice < $rlt[0]->home_deli_below_amount)
					$fee = $rlt[0]->home_deli_below_amount_fee;
				elseif($TotalPrice > $rlt[0]->home_deli_over_amount_fee)
					$fee = $rlt[0]->home_deli_over_amount_fee;
				break;
			case 3:
				$sql = 'select cash_on_deli_amount_percent,cash_on_deli_amount_extra_fee from domestic_ship where id=1';
				$rlt = DB::select($sql);
				$fee = round(($TotalPrice*(($rlt[0]->cash_on_deli_amount_percent)/100))+$rlt[0]->cash_on_deli_amount_extra_fee);
				break;	
		}
	}else{
		//計算訂單總重量(克)
		$TotalWeight = 0;
        foreach($cart as $idx => $keys){
            $TotalWeight += $keys['weight']*$keys['amount'];
        }
        $sql = 'select below_gram,below_gram_fee,each_extra_gram,each_extra_gram_fee from foreign_ship where country_id=? and id=?';
        $result = DB::select($sql,[$LocationId,$way]);
        if($TotalWeight < $result[0]->below_gram){
        	$fee = $result[0]->below_gram_fee;
        }elseif($result[0]->below_gram == 0){
        	$fee = $result[0]->below_gram_fee;
        }else{
        	$fee = $result[0]->below_gram_fee;
        	$WFlag = $TotalWeight-$result[0]->below_gram;

        	$i = $result[0]->each_extra_gram;
        	
        	while($WFlag > 0){
        		$fee += $result[0]->each_extra_gram_fee;
        		$WFlag -= $i;
        	}
        }
	}
	return $fee;
}

/**
*檢查電子郵件
*para1:email
*return:
*true:無重覆
*false:有重覆
*/
function VerifyEmail($email){
	$sql = "select count(1) as count from members where account = ?";
	$result = DB::select($sql,[$email]);
	if($result[0]->count>0)
		return false;
	else
		return true;
}

/**
*檢查電子郵件
*para1:email
*/
function ClearSession(){
	Session::put('way','');			//運送方式
    Session::put('DeliFee','');		//運費
    Session::put('PayWay','');		//付款方式
    Session::put('location','');	//運送國家
    Session::put('BasicData','');	//基本資料
    Session::put('cart','');	//基本資料
}

/**
*檢查庫存
*para1:array('id'=>amount)
*回傳值：
*null:扣庫存成功
*not null:扣庫失敗
*/
function CheckStock($prod){
	$flag = '';
	$updateSQL = array();
	foreach($prod as $idx => $keys){
		$sql = "select stock from product where id=?";
		$result = DB::select($sql,[$idx]);
		if(!empty($result)){
			if($result[0]->stock < $keys['amount'])
				$flag .= '*['.$keys['name'].']購買數量大於庫存數('.$result[0]->stock.')，請調整。\\n';
			else{
				$updateSQL[$idx] = 'update product set stock=(stock-'.$keys['amount'].') where id = '.$idx;
			}
		}else{
			$flag .= '無法查詢庫存!!\\n';
		}
	}
	if($flag === ''){
		DB::beginTransaction();
		foreach($updateSQL as $sql_idx => $sql_cmd){
			$cmd = DB::update($sql_cmd);
			if(!$cmd){
				$flag .= '庫新無法更新('.$sql_idx.')\\n';
			}
		}
	}
	return $flag;

}

/**
*
*/
function TWCashOnDeli(){
	//------- S:計算總金額
    $TotalPrice = 0;
    $cart = Session::get('cart');
    foreach($cart as $idx => $keys){
        $TotalPrice += $keys['amount']*$keys['UnitPrice'];
    }
    //------- E:計算總金額

    //抓取貨到付款金額限制
    $sql = 'select count(1) as count from domestic_ship where cash_on_deli_limit_amount>?';
    $result = DB::select($sql,[$TotalPrice]);
    if($result[0]->count == 1)
        $cash_on_delivery = true;
    else
        $cash_on_delivery = false;

    return $cash_on_delivery;
}

?>