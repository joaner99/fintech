<?php

namespace App\Support;

class ExcelReader
{
    protected $data           = [];
    protected $worksheet   = null;
    
    public function __construct($file, $settings = [])
    { 
        $defaultSettings = [
            'delimiter'  => ',',
            'enclosure'  => '"',
            'lineEnding' => "\r\n",
        ];
        
        $settings  = array_merge($defaultSettings, $settings);
        $worksheet = null;

        switch($this->getMime($file)) {
            case 'text/html':
                libxml_use_internal_errors(true);
                
                $content = file_get_contents($file);
                if (mb_detect_encoding($content, 'UTF-8, UTF-16, BIG5') != 'UTF-8') {
                    $content = mb_convert_encoding($content, 'UTF-8', 'UTF-16, BIG5');
                    $content = str_ireplace('charset=Big5', 'charset=Utf-8', $content);
                }

                file_put_contents($file, $content);
                
                $objReader   = \PHPExcel_IOFactory::createReader('HTML');
                $objPHPExcel = $objReader->load($file);

                //$objPHPExcelWriter = PHPExcel_IOFactory::createWriter($objPHPExcel,$outputFileType);
                //$objPHPExcel = $objPHPExcelWriter->save($outputFileName);
                
                //$objPHPExcel = PHPExcel_IOFactory::load($file);
                $worksheet   = $objPHPExcel->setActiveSheetIndex(0);
                break;
            case 'text/plain':
            case 'text/csv':
                $content = file_get_contents($file);
                if (!mb_check_encoding($content, 'UTF-8')) {
                    if (mb_check_encoding($content, 'UTF-16')) {
                        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-16');    
                    } elseif (mb_check_encoding($content, 'UTF-16LE')) {
                        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-16LE'); 
                    } elseif (mb_check_encoding($content, 'UTF-16BE')) {
                        $content = mb_convert_encoding($content, 'UTF-8', 'UTF-16BE'); 
                    } elseif (mb_check_encoding($content, 'BIG5')) {
                        $content = mb_convert_encoding($content, 'UTF-8', 'BIG5'); 
                    }
                }
                $content = str_replace([chr(255).chr(254), chr(254).chr(255)], '', $content);
                
                file_put_contents($file, $content);
                
                $objReader = \PHPExcel_IOFactory::createReader('CSV')
                    ->setDelimiter($settings['delimiter'])
                    ->setEnclosure($settings['enclosure'])
                    ->setSheetIndex(0);
                $worksheet = $objReader->load($file)->getActiveSheet();
                break;
            case 'application/octet-stream':
            case 'application/vnd.ms-office':
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                $cacheMethod   = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
                $cacheSettings = array('memoryCacheSize' => '32MB');
                \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
                
                $objPHPExcel = \PHPExcel_IOFactory::load($file);
                $worksheet   = $objPHPExcel->setActiveSheetIndex(0);
                break;
        }
        
        if ($worksheet) {
            $this->worksheet = $worksheet;
        }
    }
    
    public function getRowCount()
    {
        return $this->worksheet->getHighestRow(); 
    }
    
    public function getMime($file)
    {
        if (!is_file($file)) {
            return '';
        }
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime  = finfo_file($finfo, $file);
        finfo_close($finfo);
        
        return $mime;
    }
    
    public function getColumnCount()
    {
        return \PHPExcel_Cell::columnIndexFromString($this->worksheet->getHighestColumn());   
    }
    
    public function getCell($row, $col)
    {
        $cell = $this->worksheet->getCellByColumnAndRow($col, $row);
        if (\PHPExcel_Shared_Date::isDateTime($cell)) {
            return date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($cell->getValue()));   
        }

        return $cell->getValue();
    }
}