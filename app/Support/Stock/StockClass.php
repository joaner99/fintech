<?php
namespace App\Support\Stock;
use App\Models\Shop\Shop;
use App\Models\Order\Order;
use App\Models\Stock\Stock;
use App\Models\Product\Product;

class StockClass
{
    protected $config;
    protected $StockType;


    public function setup($config)
    {
        $defaultConfig = [
            'shop_id'       => '',
            'order_id'        => '',
            'prod_id'     => '',
            'qty'     => '',
            'ext' => '',
        ];
        
        $config       = array_merge($defaultConfig, $config);
        $this->config = $config;  
        
    }

    //倉型類別
    public function StockTypeCh($Type)
    {
    	switch ($Type) {
    		case 0:
    			$msg="入倉";
    			break;
    		case 1:
    			$msg="保留倉";
    			break;
    		case 2:
    			$msg="出倉";
    			break;
    		case 3:
                $msg="調整";
                break;
    	}
        return $msg;
    }

    //判斷是入庫或調整
    public function JudgmentType()
    {
        if($this->config['qty'] - $this->SelectStockSpecify()>0){
            $this->StockType=0;
        }else{
            $this->StockType=3;
        }
    }
    //出入庫單
    public function insert($type='')
    {   
        /*2017.12.14暫時關閉庫存
        if(Shop::find($this->config['shop_id'])->shop_type!=1){
            return false;
        }
        //判斷是否有訂單編號，若沒有則認為是入庫/調整庫存
        //等於0代表沒有調整,大於0代表是正確資料
    	if(empty($this->config['order_id']) && ($this->config['qty'] - $this->SelectStockSpecify()!=0) && ($this->config['qty']>=0)){
            $this-> JudgmentType();
            Stock::create([
            'no'=>'ST'.date('ymd').rand(0,999999),
            'shop_id'=>$this->config['shop_id'],
            'prod_id'=>$this->config['prod_id'],
            'qty'=>($type==1)? $this->config['qty'] : $this->config['qty'] - $this->SelectStockSpecify(),
            'type'=>$this->StockType,
            'ext'=>($this->config['ext'])? $this->config['ext']: $this->StockTypeCh($this->StockType)
            ]);
        //若有訂單編號，則加入訂單，並將庫存轉移到保留倉等待出貨
    	}elseif(!empty($this->config['order_id'])){
            //若庫存大於0才可以寫入
            if($this->HowStore() > 0){
                Stock::create([
                    'no'=>'ST'.date('ymd').rand(0,999999),
                    'shop_id'=>$this->config['shop_id'],
                    'order_id'=>$this->config['order_id'],
                    'prod_id'=>$this->config['prod_id'],
                    'qty'=>$this->config['qty'] *-1,
                    'type'=>1,
                    'ext'=>($this->config['ext'])? $this->config['ext'] :'訂單成功，移動至保留倉等待出貨'
                    ]);
                Product::where('id',$this->config['prod_id'])
                       ->update(['stock'=>$this->HowStore()]);
            }

    	}
        */
    	
    }
    //指定產品現有庫存數
    public function SelectStockSpecify()
    {
    	return Stock::where('shop_id',$this->config['shop_id'])
    			 ->where('prod_id',$this->config['prod_id'])
    			 ->sum('qty');
    }

    //判斷是否有庫存(扣除保留倉)
    public function HowStore()
    {
    	return Stock::where('prod_id',$this->config['prod_id'])->sum('qty');
    }
    //保留倉剩餘數
    public function TemporarilylStore()
    {
        return -1* Stock::where('prod_id',$this->config['prod_id'])->where('type',1)->sum('qty');
    }

    //真實庫存(包含保留倉)
    public function TotelStore()
    {
        return $this->HowStore() + $this->TemporarilylStore();
    }

    //出貨
    public function ShipmentsStore()
    {   
        if(Shop::find($this->config['shop_id'])->shop_type!=1){
            return false;
        }
        Stock::where('prod_id',$this->config['prod_id'])->where('order_id',$this->config['order_id'])->where('shop_id',$this->config['shop_id'])->update(['type'=>2,'ext'=>date('ymd').'出貨']);
    }
    //退貨
    public function ReturnStore()
    {
        $this->config['ext']="退貨後入庫";
        $this->insert(1);
    }
}