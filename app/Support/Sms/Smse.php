<?php

namespace App\Support\Sms;

class Smse
{
    protected $message = '';
    protected $user    = '';
    protected $pwd     = '';
        
    public function __construct($user, $pwd)
    {
        $this->user = $user;
        $this->pwd  = $pwd;
    }
    
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }
    
    public function send($county, $phone)
    {
        $url     = 'http://smsmo.smse.com.tw/STANDARD/SMS_FU.ASP';
        $phone   = $county.trim(preg_replace('/^0/i', '', str_replace('-', '', $phone)));
        $params  = [
            'yhy'     => $this->user,
            'dc2a'    => $this->pwd,
            'movetel' => $phone,
            'bf'      => date('Y/m/d H:i:s'),
            'sb'      => mb_convert_encoding($this->message, 'BIG5', 'UTF-8'),
        ];
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($params));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = mb_convert_encoding(curl_exec($ch), 'UTF-8', 'BIG5');
        curl_close($ch);
        return preg_match('/^OK/i', $result);
    }
}
