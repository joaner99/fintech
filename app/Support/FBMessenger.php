<?php

namespace App\Support;

class FBMessenger
{
    protected $token   = '';
    protected $version = 'v2.10';
    
    public function __construct($token, $version = 'v2.10')
    { 
        $this->version = $version;
        $this->token   = $token;
    }
    
    // 傳送訊息
    public function sendMessage($psid, $message)
    {
        $url    = 'https://graph.facebook.com/'.$this->version.'/me/messages';
        $params = [
            'access_token' => $this->token,
            'message'      => json_encode([
                'text' => $message
            ]),
            'recipient'    => json_encode([
                'id' => $psid    
            ])
        ];
        
        $ch     = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params)); 
        $json = json_decode(curl_exec($ch));
        curl_close($ch);

        return isset($json->message_id) ? : false;
    }
}