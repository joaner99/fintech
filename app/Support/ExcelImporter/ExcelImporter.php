<?php

namespace App\Support\ExcelImporter;

use DB;

class ExcelImporter
{
    protected static $worksheet = null;
    protected static $rows      = null;
    protected static $type      = '';
    protected static $columns   = array();
    protected static $file      = '';
    
    public static function import($file, $table, $settings = array())
    {   
        $defaultSettings = array(
            'delimiter'  => ',',
            'enclosure'  => '"',
            'lineEnding' => "\r\n",
        );
        
        $settings = array_merge($defaultSettings, $settings);

        if (!is_file($file)) {
            return false;
        }
        
        self::$file = $file;
        $mime       = self::getMime();

        switch($mime) {
            case 'text/plain':
                if (self::getExtName() != 'csv') {
                    return false;
                }
            case 'text/csv':
                self::$type = 'csv';
                $objReader  = \PHPExcel_IOFactory::createReader('CSV')
                    ->setDelimiter($settings['delimiter'])
                    ->setEnclosure($settings['enclosure'])
                    ->setLineEnding($settings['lineEnding'])
                    ->setSheetIndex(0);
                self::$worksheet = $objReader->load(self::$file)->getActiveSheet();
                break;
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                self::$type    = 'xls';
                $cacheMethod   = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
                $cacheSettings = array('memoryCacheSize' => '32MB');
                \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
                
                $objPHPExcel     = \PHPExcel_IOFactory::load(self::$file);
                $worksheet       = $objPHPExcel->setActiveSheetIndex(0);
                self::$worksheet = $worksheet;
                break;
            default:
                return false;
                break;
        }
        
        $count   = self::getColumnCount();
        $columns = array();
        for ($i = 0; $i < $count; $i++) {
            $columns[] = self::getCell(1, $i);    
        }
        
        $count = self::getRowCount();
        $data  = array();
        for ($i = 2; $i <= $count; $i++) {
            foreach ($columns as $index => $column) {
                $data[$i - 2][$column] = self::getCell($i, $index);
            }
        }
        
        DB::beginTransaction(); 
        if (!DB::table($table)->insert($data)) {
            DB::rollback();
            return false;
        }
        
        DB::commit();
        return true;
    }
    
    public static function getMime()
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime  = finfo_file($finfo, self::$file);
        finfo_close($finfo);
        
        return $mime;
    }
    
    public static function getExtName()
    {
        return substr(strrchr(self::$file, '.'), 1);
    }
    
    public static function getRowCount()
    {
        return self::$worksheet->getHighestRow(); 
    }
    
    public static function getColumnCount()
    {
        return \PHPExcel_Cell::columnIndexFromString(self::$worksheet->getHighestColumn());   
    }
    
    public static function getCell($row, $col)
    {
        $cell = self::$worksheet->getCellByColumnAndRow($col, $row);
        if (\PHPExcel_Shared_Date::isDateTime($cell)) {
            return date('Y-m-d H:i:s', \PHPExcel_Shared_Date::ExcelToPHP($cell->getValue()));   
        }

        return $cell->getValue();
    }
}
