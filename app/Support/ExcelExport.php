<?php
namespace App\Support;

class ExcelExport
{
    protected $fileName = '';
    protected $data     = [];
    
    public function setFileName($name)
    {
        $this->fileName = $name;
        return $this;
    }
    
    public function setCellValue($row, $column, $value)
    {
        if (!isset($this->data[$row])) {
            $this->data[$row] = [];
        }    
        
        $this->data[$row][$column] = $value;
    }
    
    public function download()
    {
        $objPHPExcel = new \PHPExcel();       
        
        $data  = [];
        foreach ($this->data as $rowNo => $row) {
            foreach ($row as $colNo => $value) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($colNo, $rowNo, $value);
            }
        }

        // 下載 Excel
        ob_clean();
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$this->fileName.'"');
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}