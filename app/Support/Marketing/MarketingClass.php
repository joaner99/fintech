<?php
namespace App\Support\Stock;
use App\Models\Shop\Shop;
use App\Models\Order\Order;
use App\Models\Marketing\Code;
use App\Models\Marketing\AddBuy;
use App\Models\Marketing\NumberOfPieces;

class MarketingClass
{
    protected $config;
    public function setup($config)
    {
        $defaultConfig = [
            'shop_id'       => '',
            'order_id'        => '',
            'prod_id'     => '',
            'qty'     => '',
            'ext' => '',
        ];
        
        $config       = array_merge($defaultConfig, $config);
        $this->config = $config;  
        
    }

    //倉型類別
    public function StockTypeCh($Type)
    {
    	switch ($Type) {
    		case 0:
    			$msg="入倉";
    			break;
    		case 1:
    			$msg="保留倉";
    			break;
    		case 2:
    			$msg="出倉";
    			break;
    		case 3:
                $msg="調整";
                break;
    	}
        return $msg;
    }

}