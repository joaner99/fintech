<?php

namespace App\Support\BitlyApi;

class ShortUrl
{
    public static function get($address)
    {
        $url = 'https://api-ssl.bitly.com/v3/shorten?access_token=55be6c2c03775ca97880bca51bde4aed5b2c49c2&longUrl='.urlencode($address);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($json);
        if (isset($result->status_code) && $result->status_code == '200') {
            return $result->data->url;
        }
        
        return '';
    }
}
