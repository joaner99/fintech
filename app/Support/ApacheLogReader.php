<?php
namespace App\Support;

class ApacheLogReader
{
    protected $format           = '(?P<ip>[^ ]+) (?P<logname>[^ ]+) (?P<user>[^ ]+) \[(?P<time>[^\]]+)\] \"(?P<method>[^ ]+) (?P<file_url>[^ ]+) (?P<protocol>[^ ]+)\" (?P<return_code>[^ ]+) (?P<size>[^ ]+) \"(?P<request_url>[^\"]+)\" \"(?P<agent>[^\"]+)\"';
    protected $timeFormat       = '(?P<day>[0-9]{1,2})\/(?P<month>[a-zA-Z]{3})\/(?P<year>[0-9]{4}):(?P<hour>[0-9]{2}):(?P<minute>[0-9]{2}):(?P<second>[0-9]{2}) \+[0-9]{4}';
    protected $filterFile       = [];
    protected $filterRequestUrl = [];
    protected $filterAgent      = [];
    protected $filterReturnCode = [];
    protected $logFile          = [];
    protected $logStartDate     = null;
    protected $logEndDate       = null;
    
    protected function convertTimeFormat($time)
    {
        if (preg_match('/'.$this->timeFormat.'/i', $time, $match)) {
            return date('Y-m-d H:i:s', strtotime($match['year'].'-'.$match['month'].'-'.$match['day'].' '.$match['hour'].':'.$match['minute'].':'.$match['second']));    
        } 
        
        return date('Y-m-d H:i:s', strtotime($match['time']));
    }
    
    public function __construct($logFile, $startDate = null, $endDate = null)
    {
        if (is_array($logFile)) {
            $this->logFile   = $logFile; 
        } else {
            $this->logFile[] = $logFile;
        }
        
        if (is_null($startDate)) {
            $startDate = date('Y-m-d', strtotime('-1 days'));    
        }
        
        if (is_null($endDate)) {
            $endDate = date('Y-m-d', strtotime('-1 days'));     
        }
        
        $this->logStartDate = $startDate;
        $this->logEndDate   = $endDate;
    }
    
    public function addFilterFile($filter)
    {
        $this->filterFile[] = $filter;
        return $this;
    }
    
    public function addFilterRequestUrl($filter)
    {
        $this->filterRequestUrl[] = $filter;
        return $this;
    }
    
    public function addFilterAgent($filter)
    {
        $this->filterAgent[] = $filter;
        return $this;
    }
    
    public function addFilterReturnCode($filter)
    {
        $this->filterReturnCode[] = $filter;
        return $this;
    }
    
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }
    
    public function setTimeFormat($format)
    {
        $this->timeFormat = $format;
        return $this;
    }
    
    public function resolve()
    {
        $result = [];
        foreach ($this->logFile as $logFile) {
            if (!file_exists($logFile)) {
                continue;    
            }   
            $fp = fopen($logFile, 'r');
            while($line = fgets($fp)) {
                if (preg_match('/'.$this->format.'/i', $line, $match)) {
                    // 檢查時間
                    if (!isset($match['time'])) {
                        continue;
                    }
                    $match['time'] = $this->convertTimeFormat($match['time']);
                    $checkDate     = date('Y-m-d', strtotime($match['time']));
                    if ($checkDate < $this->logStartDate || $checkDate > $this->logEndDate) {
                        continue;   
                    }
                                     
                    // 過瀘 Agent
                    if (count($this->filterAgent) > 0) {
                        if (!isset($match['agent'])) {
                            continue;
                        }
                        $pass = true;
                        foreach ($this->filterAgent as $agent) {
                            if (preg_match('/'.$agent.'/i', $match['agent'])) {
                                $pass = false;
                                break;
                            }      
                        }
                        if (!$pass) {
                            continue;
                        }
                    }
                    
                    // 過瀘回覆碼
                    if (count($this->filterReturnCode) > 0) {
                        if (!isset($match['return_code'])) {
                            continue;
                        }
                        $pass = false;
                        foreach ($this->filterReturnCode as $returnCode) {
                            if ($returnCode == $match['return_code']) {
                                $pass = true;
                                break;
                            }        
                        }
                        if (!$pass) {
                            continue;
                        }
                    }
                    
                    // 檢查過濾檔案
                    if (count($this->filterFile) > 0) {
                        if (!isset($match['file_url'])) {
                            continue;
                        }
                        $pass = false;
                        foreach ($this->filterFile as $file) {
                            if (preg_match('/'.$file.'/i', $match['file_url'])) {
                                $pass = true;
                                break;
                            }       
                        }
                        if (!$pass) {
                            continue;
                        }
                    }
                    
                    // 檢查過瀘網址
                    if (count($this->filterRequestUrl) > 0) {
                        if (!isset($match['request_url'])) {
                            continue;
                        }
                        $pass = false;
                        foreach ($this->filterRequestUrl as $url) {
                            if (preg_match('/'.$url.'/i', $match['request_url'])) {
                                $pass = true;
                                break;
                            }       
                        }
                        if (!$pass) {
                            continue;
                        }
                    } 
                    
                    if (isset($match['size'])) {
                        $match['size'] = (!is_numeric($match['size'])) ? 0 : $match['size'];    
                    }
                    
                    foreach ($match as $key => $value) {
                        if (is_int($key)) {
                            unset($match[$key]);
                        }   
                    }
                    $result[] = $match;
                }
            }
            fclose($fp);
        } 
        
        return $result;
    }
}
