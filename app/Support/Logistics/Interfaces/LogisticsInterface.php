<?php
namespace App\Support\Logistics\Interfaces;

interface LogisticsInterface
{
    public static function setup($config);
    public static function sendForm($order);
    public static function sendStoreShipForm($order);
    public static function getFeedback();
    public static function getStores();
    public static function gotoSelectStore($order);
    public static function getSelectStoreFeedback();
    public static function answerC2CReply($result = false);
    public static function getFeedBackErrorMessage($feedBack);
    public static function queryOrder($shipNo);
    public static function printOrder($shipNo, $store_ship_no, $type);
}