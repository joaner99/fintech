/******************************************************************************
 * 送出選擇超商的範例(請記得 use App\Support\Logistics\EZShip;)
 *****************************************************************************/

        $ezship = new EZShip();
        return $ezship->setSuID('ilnblue.alan@gmail.com')    // EZShip 的帳號
            ->setOrderNo(date('YmdHis'))    // 訂單號碼
            ->setReturnUrl('http://letsgo.local:88/home')    // 重新導回的頁面
            ->setName('姓名')    // 買家的姓名
            ->setMobil('0987654321')    // 買家的手機號碼
            ->setEmail('test@test.com')   // 買家的 EMail
            ->setAmount(0)     // 付款金額，如果是 0 就表示純取貨不付款
            ->setWebTemp('')     // 我們送出的資料，純粹我們自己用，可以為空
            ->sendForm();
            
/******************************************************************************
 * 取得買家選擇超商的範例(請記得 use App\Support\Logistics\EZShip;)
 *****************************************************************************/
        $data = $ezship->getFeedBack();
        // 回傳格式
        /*
        [
            'order_no' => '訂單號碼',
            'sn_id'    => 'EZShip 的序號',
            'st_cate'  => '便利商店的代號。TFM-全家超商；TLF-萊爾富超商；TOK-OK超商',
            'st_code'  => '門市的代號',
            'st_name'  => '門市的名稱',
            'st_addr'  => '門市的地址',
            'st_tel'   => '門市的電話',
            'webtemp'  => '我們送出的 webTemp 資料',
        ]
        */
