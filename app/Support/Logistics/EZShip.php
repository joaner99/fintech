<?php
namespace App\Support\Logistics;

use App\Support\Logistics\Interfaces\LogisticsInterface;
use Request;

class EZShip implements LogisticsInterface
{
    protected static $config;
    
    public static function setup($config)
    {
        $defaultConfig = [
            'su_id' => ''
        ];
        
        $config       = array_merge($defaultConfig, $config);
        self::$config = $config;  
    }
    
    public static function sendForm($order)
    {
        $defaultOrder = [
            'rturl'    => '',
            'webtemp'  => '',
            'order_id' => '',
            'amount'   => '',
            'name'     => '',
            'email'    => '',
            'mobil'    => '',
        ];
        
        $order          = array_merge($defaultOrder, $order);
        $order['email'] = empty($order['email']) ? 'admin@sunstar-dt.com' : '';
        $html = "
            <html>
                <head><meta charset='utf-8'></head>
                <body>
                    <form method='GET' action='https://map.ezship.com.tw/ezship_map_web.jsp' id='ecshipForm'>
                        <input type='hidden' name='suID' value='".trim(self::$config['su_id'])."' />
                        <input type='hidden' name='rtURL' value='".$order['finish_store_url']."' />
                        <input type='hidden' name='webPara' value='".$order['webtemp']."' />
                        <input type='hidden' name='processID' value='".$order['order_id']."' />
                    </form>
                    <script>document.getElementById('ecshipForm').submit();</script>
                </body>
            </html>
        ";
        //ob_clean();
        header("Content-Type:text/html;charset=utf-8");
        echo $html;
        exit;
        //return mb_convert_encoding($html, 'BIG5', 'UTF-8');
        //return $html;
    }
    
    public static function sendStoreShipForm($order)
    {
        $defaultOrder = [
            'rturl'    => '',
            'webtemp'  => '',
            'order_id' => '',
            'amount'   => '',
            'name'     => '',
            'email'    => '',
            'mobil'    => '',
        ];
        $order          = array_merge($defaultOrder, $order);
        $order['email'] = empty($order['email']) ? 'admin@sunstar-dt.com' : $order['email'];
        $orderType      = ($order['amount'] > 0) ? 1 : 3;
        $html = "
            <html>
                <head><meta charset='utf-8'></head>
                <body>
                    <form method='POST' action='https://www.ezship.com.tw/emap/ezship_request_order_api_ex.jsp' id='ecshipForm'>
                        <input type='hidden' name='su_id' value='".trim(self::$config['su_id'])."' />
                        <input type='hidden' name='order_id' value='".trim($order['order_id'])."' />
                        <input type='hidden' name='order_status' value='A02' />
                        <input type='hidden' name='order_type' value='".$orderType."' />
                        <input type='hidden' name='order_amount' value='".$order['amount']."' />
                        <input type='hidden' name='rv_name' value='".trim($order['name'])."' />
                        <input type='hidden' name='rv_email' value='".trim($order['email'])."' />
                        <input type='hidden' name='rv_mobile' value='".trim($order['mobile'])."' />
                        <input type='hidden' name='st_code' value='".trim($order['st_cate'].$order['st_code'])."' />
                        <input type='hidden' name='rtn_url' value='".trim($order['rturl'])."' />
                        <input type='hidden' name='web_para' value='".trim($order['webtemp'])."' />
                    </form>
                    <script>document.getElementById('ecshipForm').submit();</script>
                </body>
            </html>
        ";
        //ob_clean();
        header("Content-Type:text/html;charset=utf-8");
        echo $html;
        exit;
    }
    
    public static function getFeedback()
    {
        $data = [
            'order_id'     => Request::get('order_id'),
            'sn_id'        => Request::get('sn_id'),
            'order_status' => Request::get('order_status'),
            'webtemp'      => Request::get('webPara'),
        ];
        
        return $data;
    }
    
    public static function getStores()
    {
        return [];
    }
    
    public static function gotoSelectStore($order) 
    {
        return null;
    }
    
    public static function getSelectStoreFeedback()
    {
        return [
            'order_no'          => Request::get('processID'),
            'sn_id'             => '',
            'store_type'        => '',
            'st_cate'           => Request::get('stCate'),
            'st_code'           => Request::get('st_code'),
            'st_name'           => Request::get('stName'),
            'st_addr'           => Request::get('stAddr'),
            'st_tel'            => Request::get('stTel'),
            'webtemp'           => Request::get('webPara'),
            'store_ship_no'     => '',
            'cvs_payment_no'    => '',
            'cvs_validation_no' => '',
        ];
    }
    
    public static function answerC2CReply($result = false)
    {
        return $result ? '1|OK' : '0|ERROR';
    }
    
    public static function getFeedBackErrorMessage($feedBack)
    {
        switch ($feedBack['order_status']) {
            case 'E00':
                return '參數傳遞內容有誤或欄位短缺';
            case 'E01':
                return 'ezShip 物流帳號不存在';
            case 'E02':
                return '此賣家帳號無法提供超商付款';
            case 'E03':
                return '此賣家帳號無可用之 輕鬆袋 或 迷你袋';
            case 'E04':
                return '取件門市有誤';
            case 'E05':
                return '金額有誤';
            case 'E06':
                return 'E-Mail 格式有誤';
            case 'E07':
                return '手機格式有誤';
            case 'E08':
                return '內容有誤 或 為空值';
            case 'E09':
                return '內容有誤 或 為空值';
            case 'E10':
                return '收件人姓名內容有誤 或 為空';
            case 'E11':
                return '收件地址內容有誤 或 為空值 ';
            case 'E98':
                return '系統發生錯誤無法載入';
            case 'E99':
                return '系統錯誤';
        }   
    }
    
    public static function queryOrder($shipNo)
    {
        return null;    
    }
    
    public static function printOrder($shipNo, $store_ship_no, $type)
    {
        return null;
    }
}