<?php
namespace App\Support\Logistics;

use App\Support\Logistics\Interfaces\LogisticsInterface;
use Request;

class EZShip implements LogisticsInterface
{
    protected static $config;
    
    public static function setup($config)
    {
        $defaultConfig = [
            'su_id' => ''
        ];
        
        $config       = array_merge($defaultConfig, $config);
        self::$config = $config;  
    }
    
    public static function sendForm($order)
    {
        $defaultOrder = [
            'rturl'    => '',
            'webtemp'  => '',
            'order_id' => '',
            'amount'   => '',
            'name'     => '',
            'email'    => '',
            'mobil'    => '',
        ];
        
        $order = array_merge($defaultOrder, $order);
        $html = "
            <html>
                <head><meta charset='big5'></head>
                <body>
                    <form method='POST' action='https://www.ezship.com.tw/emap/rv_request_web.jsp' id='ecshipForm'>
                        <input type='hidden' name='su_id' value='".trim(self::$config['su_id'])."' />
                        <input type='hidden' name='rturl' value='".$order['rturl']."' />
                        <input type='hidden' name='webtemp' value='".$order['webtemp']."' />
                        <input type='hidden' name='order_id' value='".$order['order_id']."' />
                        <input type='hidden' name='rv_amount' value='".$order['amount']."' />
                        <input type='hidden' name='rv_name' value='".$order['name']."' />
                        <input type='hidden' name='rv_email' value='".$order['email']."' />
                        <input type='hidden' name='rv_mobil' value='".$order['mobil']."' />
                    </form>
                    <script>document.getElementById('ecshipForm').submit();</script>
                </body>
            </html>
        ";
        //ob_clean();
        header("Content-Type:text/html;charset=big5");
        echo mb_convert_encoding($html, 'BIG5', 'UTF-8');
        exit;
        //return mb_convert_encoding($html, 'BIG5', 'UTF-8');
        //return $html;
    }
    
    public static function sendStoreShipForm($order)
    {
        return null;
    }
    
    public static function getFeedback()
    {
        $data = [
            'order_no' => Request::get('order_id'),
            'sn_id'    => Request::get('sn_id'),
            'st_cate'  => Request::get('st_cate'),
            'st_code'  => Request::get('st_code'),
            'st_name'  => mb_convert_encoding(Request::get('st_name'), 'UTF-8', 'BIG5'),
            'st_addr'  => mb_convert_encoding(Request::get('st_addr'), 'UTF-8', 'BIG5'),
            'st_tel'   => Request::get('st_tel'),
            'webtemp'  => Request::get('webtemp'),
        ];
        
        return $data;
    }
    
    public static function getStores()
    {
        return [];
    }
    
    public static function gotoSelectStore($order) 
    {
        return null;
    }
    
    public static function getSelectStoreFeedback()
    {
        return null;   
    }
    
    public static function answerC2CReply($result = false)
    {
        return $result ? '1|OK' : '0|ERROR';
    }
}