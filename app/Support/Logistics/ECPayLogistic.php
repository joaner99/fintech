<?php
namespace App\Support\Logistics;

use App\Support\Logistics\Interfaces\LogisticsInterface;
use Request;
use View;

class ECPayLogistic implements LogisticsInterface
{
    protected static $config;
    
    public static function setup($config)
    {
        $defaultConfig = [
            'merchant_id' => '',
            'hash_key'    => '',
            'hash_iv'     => '',
            'use_b2c'     => 'N',
        ];
        
        $config       = array_merge($defaultConfig, $config);
        self::$config = $config;  
    }
    
    public static function sendForm($order)
    {
        return redirect()->route('shopping.selectStore', ['orderNo' => $order['order_id'], 'type' => 'store']);
    }
    
    public static function gotoSelectStore($order)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Logistics.Integration.php');
        
        $logisticsSubType = \LogisticsSubType::UNIMART_C2C;
        switch ($order['store_type']) {
            case 'UNIMARTC2C':
                $logisticsSubType = \LogisticsSubType::UNIMART_C2C;
                break;
            case 'FAMIC2C':
                $logisticsSubType = \LogisticsSubType::FAMILY_C2C;
                break;
            case 'HILIFEC2C':
                $logisticsSubType = \LogisticsSubType::HILIFE_C2C;
                break;
            case 'UNIMART':
                $logisticsSubType = \LogisticsSubType::UNIMART;
                break;
            case 'FAMI':
                $logisticsSubType = \LogisticsSubType::FAMILY;
                break;
            case 'HILIFE':
                $logisticsSubType = \LogisticsSubType::HILIFE;
                break;
        }
        $device = \Device::PC;
       
        // 判斷使用者是電腦還是手機
        $useragent = $_SERVER['HTTP_USER_AGENT'];
         if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
            $device = \Device::MOBILE;    
        }
        $AL       = new \ECPayLogistics();
        $AL->Send = array(
            'MerchantID'       => trim(self::$config['merchant_id']),
            'MerchantTradeNo'  => $order['order_id'],
            'LogisticsSubType' => $logisticsSubType,
            'IsCollection'     => ($order['amount'] > 0) ? \IsCollection::YES : \IsCollection::NO,
            'ServerReplyURL'   => $order['rturl'],
            'ExtraData'        => $order['webtemp'],
            'Device'           => $device
        );

        $html = $AL->CvsMap('');
        return $html;
    }
    
    public static function sendStoreShipForm($order)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Logistics.Integration.php');
        $serviceUrl = env('PAYMENT_IS_TEST', true) ? 'https://logistics-stage.ecpay.com.tw/Express/Create' : 'https://logistics.ecpay.com.tw/Express/Create';
        $logisticsSubType = \LogisticsSubType::UNIMART_C2C;
        switch ($order['store_type']) {
            case 'UNIMARTC2C':
                $logisticsSubType = \LogisticsSubType::UNIMART_C2C;
                break;
            case 'FAMIC2C':
                $logisticsSubType = \LogisticsSubType::FAMILY_C2C;
                break;
            case 'HILIFEC2C':
                $logisticsSubType = \LogisticsSubType::HILIFE_C2C;
                break;
            case 'UNIMART':
                $logisticsSubType = \LogisticsSubType::UNIMART;
                break;
            case 'FAMI':
                $logisticsSubType = \LogisticsSubType::FAMILY;
                break;
            case 'HILIFE':
                $logisticsSubType = \LogisticsSubType::HILIFE;
                break;
        }
        $AL = new \ECPayLogistics();
        $AL->HashKey = trim(self::$config['hash_key']);
        $AL->HashIV  = trim(self::$config['hash_iv']);
        $AL->Send    = array(
            'MerchantID'           => trim(self::$config['merchant_id']),
            'MerchantTradeNo'      => $order['order_id'],
            'MerchantTradeDate'    => date('Y/m/d H:i:s'),
            'LogisticsType'        => 'CVS',
            'LogisticsSubType'     => $logisticsSubType,
            'GoodsName'            => str_replace('*','x',str_replace('#', '', mb_substr($order['goods_name'], 0, 25))),
            'GoodsAmount'          => ($order['amount'] > 0) ? $order['amount'] : $order['goods_amount'],
            'CollectionAmount'     => $order['amount'],
            'IsCollection'         => ($order['amount'] > 0) ? \IsCollection::YES : \IsCollection::NO,
            'SenderName'           => $order['sender_name'],
            'SenderCellPhone'      => $order['sender_phone'],
            'LogisticsC2CReplyURL' => $order['c2c_reply_url'],
            'ReceiverName'         => $order['name'],
            'ReceiverPhone'        => $order['mobile'],
            'ReceiverCellPhone'    => $order['mobile'],
            'ReceiverEmail'        => $order['email'],
            'TradeDesc'            => $order['webtemp'],
            'ServerReplyURL'       => $order['notify_url'],
            'ClientReplyURL'       => $order['rturl'],
            'Remark'               => '',
            'PlatformID'           => '',
        );
        $AL->SendExtend = [
            'ReceiverStoreID'   => $order['st_code'],
        ];
        return $AL->CreateShippingOrder('');
    }
    
    public static function getFeedback()
    {
        $data = [
            'order_id'     => Request::get('MerchantTradeNo', ''),
            'sn_id'        => Request::get('AllPayLogisticsID', ''),
            'order_status' => Request::get('RtnCode', ''),
            'webtemp'      => Request::get('ExtraData', ''),
        ];
        
        return $data;
    }
    
    public static function getStores()
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Logistics.Integration.php');
        if (self::$config['use_b2c'] != 'Y') {
            return [
                ['name' => '統一超商', 'value' => \LogisticsSubType::UNIMART_C2C],
                ['name' => '全家超商', 'value' => \LogisticsSubType::FAMILY_C2C],
                ['name' => '萊爾富超商', 'value' => \LogisticsSubType::HILIFE_C2C],
            ];
        }
        return [
            ['name' => '統一超商', 'value' => \LogisticsSubType::UNIMART],
            ['name' => '全家超商', 'value' => \LogisticsSubType::FAMILY],
            ['name' => '萊爾富超商', 'value' => \LogisticsSubType::HILIFE],
        ];
    }
    
    public static function getSelectStoreFeedback()
    {
        $data = [
            'order_no'          => Request::get('MerchantTradeNo'),
            'sn_id'             => Request::get('MerchantTradeNo'),
            'store_type'        => Request::get('LogisticsSubType'),
            'st_cate'           => '',
            'st_code'           => Request::get('CVSStoreID'),
            'st_name'           => Request::get('CVSStoreName'),
            'st_addr'           => Request::get('CVSAddress'),
            'st_tel'            => Request::get('CVSTelephone'),
            'store_ship_no'     => Request::get('CVSPaymentNo').Request::get('CVSValidationNo'),
            'webtemp'           => Request::get('ExtraData'),
            'cvs_payment_no'    => Request::get('CVSPaymentNo'),
            'cvs_validation_no' => Request::get('CVSValidationNo'),
        ];
        
        return $data;
    }
    
    public static function answerC2CReply($result = false)
    {
        return $result ? '1|OK' : '0|ERROR';
    }
    
    public static function getFeedBackErrorMessage($feedBack)
    {
        return '';
    }
    
    public static function queryOrder($shipNo)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Logistics.Integration.php');
        $AL = new \ECPayLogistics();
        $AL->HashKey = trim(self::$config['hash_key']);
        $AL->HashIV  = trim(self::$config['hash_iv']);
        $AL->Send    = array(
            'MerchantID'        => trim(self::$config['merchant_id']),
            'AllPayLogisticsID' => $shipNo,
            'TimeStamp'         => time()
        );
        try{
            $callback = $AL->QueryLogisticsInfo();
        } catch (\Exception $e) {
            return null;
        }
        return [
          'sn_id'           => $callback['AllPayLogisticsID'],
          'price'           => $callback['GoodsAmount'],
          'products'        => $callback['GoodsName'],
          'handling_charge' => $callback['HandlingCharge'],
          'status'          => $callback['LogisticsStatus'],
          'type'            => $callback['LogisticsType'],
          'store_ship_no'   => $callback['ShipmentNo'],
          'trade_date'      => $callback['TradeDate'],
        ];
    }

    public static function printOrder($shipNo,$store_ship_no,$type)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Logistics.Integration.php');
        $storeType=explode('_', $type);
        switch ($storeType[1]) {
            case 'UNIMART':
                $store_ship_no_check=substr($store_ship_no,-4);
                $store_ship_no= substr($store_ship_no,0,-4);
                $printTpye='UNIMART';
                break;
            case 'UNIMARTC2C':
                $store_ship_no_check=substr($store_ship_no,-4);
                $store_ship_no= substr($store_ship_no,0,-4);
                $printTpye='UNIMART';
                break;
            case 'FAMI':
                $store_ship_no_check=$shipNo;
                $store_ship_no=$shipNo;
                $printTpye='FAMI';
                break;
            case 'FAMIC2C':
                $store_ship_no_check=$shipNo;
                $store_ship_no=$shipNo;
                $printTpye='FAMI';
                break;
            case 'HILIFE':
                $store_ship_no_check=$shipNo;
                $store_ship_no=$shipNo;
                $printTpye='HILIFE';
                break;
            case 'HILIFEC2C':
                $store_ship_no_check=$shipNo;
                $store_ship_no=$shipNo;
                $printTpye='HILIFE';
                break;
        }
        $AL = new \ECPayLogistics();
        $AL->HashKey = trim(self::$config['hash_key']);
        $AL->HashIV  = trim(self::$config['hash_iv']);
        $AL->Send    = array(
            'MerchantID'        => trim(self::$config['merchant_id']),
            'AllPayLogisticsID' => $shipNo,
            'CVSPaymentNo'         => $store_ship_no,
            'CVSValidationNo'         => $store_ship_no_check
        );
        try{
            switch ($printTpye) {
                case 'UNIMART': $callback = $AL->PrintUnimartC2CBill(); break;
                case 'FAMI': $callback = $AL->PrintFamilyC2CBill(); break;
                case 'HILIFE': $callback = $AL->PrintHiLifeC2CBill(); break;
            }
            
        } catch (\Exception $e) {
            return null;
        }
        return $callback;
    }
    public static function printOrderB2C($shipNo)
    {
        include_once(dirname(__FILE__).'/Sdk/ECPay.Logistics.Integration.php');
        $AL = new \ECPayLogistics();
        $AL->HashKey = trim(self::$config['hash_key']);
        $AL->HashIV  = trim(self::$config['hash_iv']);
        $AL->Send    = array(
            'MerchantID'        => trim(self::$config['merchant_id']),
            'AllPayLogisticsID' => $shipNo,
        );
        try{
            return $AL->PrintTradeDoc();
        } catch (\Exception $e) {
            return null;
        }
        return $callback;
    }
}