<?php

namespace App\Support;
use Session;

class SSEncrypt
{
    protected static $hashKey = 'SunStar';
    protected static $useCode = [];
    const LENGTH              = 6;
    const CODE                = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    
    protected static function init()
    {
        if (strlen(self::$hashKey) < self::LENGTH - 1) {
            while (strlen(self::$hashKey) < self::LENGTH - 1) {
                self::$hashKey .= self::$hashKey;        
            }    
        }
        
        $len = strlen(self::CODE);
        for ($i = 0; $i < self::LENGTH - 1; $i++) {
            $base              = ord(self::$hashKey[$i]) % $len; 
            $tmp               = self::CODE;
            self::$useCode[$i] = '';
            while (strlen(self::$useCode[$i]) != $len) {
                $code               = substr($tmp, $base, 1);
                self::$useCode[$i] .= $code;
                $tmp                = str_replace($code, '', $tmp);
                if (empty($tmp)) {
                    break;
                }
                $base = ($base + ord($code)) % strlen($tmp); 
            }
        }
    }
    
    public static function encode($num)
    {
        /*
        if ($num >= 1 && $num <= 1025) {
            return $num;
        }
        */
        
        if (count(self::$useCode) < self::LENGTH - 1) {
            self::init();
        }
        
        $result = '';
        $len    = strlen(self::CODE);
        foreach (self::$useCode as $index => $useCode) {
            $mod    = $num % $len;
            $num    = floor($num / $len);
            $code   = substr(self::$useCode[$index], $mod, 1);
            $result = $result.$code;
        }

        $check = substr(md5($result.self::$hashKey), 0, 1);
        return $result.$check;
    }
    
    public static function decode($code)
    {
        /*
        if ($code >= 1 && $code <= 1025 && is_numeric($code)) {
            return $code;
        }
        */
        
        if (count(self::$useCode) < self::LENGTH - 1) {
            self::init();
        }
        if (strlen($code) != self::LENGTH) {
            return null;
        }
        $encode = substr($code, 0, self::LENGTH - 1);
        $check  = substr($code, -1, 1);
        if (substr(md5($encode.self::$hashKey), 0, 1) != $check) {
            return null;    
        }

        $len    = strlen(self::CODE);
        $result = 0;
        for ($i = 0; $i < self::LENGTH - 1; $i++) {
            $result += strpos(self::$useCode[$i], $encode[$i]) * pow($len, $i);    
        }
        return $result;
    }
}
