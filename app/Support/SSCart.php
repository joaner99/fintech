<?php

namespace App\Support;
use Session;

class SSCart
{
    protected static $cart = null;
    protected $shopId      = null;
    protected $products    = [];
    protected $total       = 0;
    protected $freight     = 0;
    
    // 初始化購物車
    public static function get($shopId)
    {
        $cart = Session::get('SSCart', []);
        if (!isset($cart[$shopId])) {
            $cart[$shopId] = new self($shopId);
            Session::put('SSCart', $cart);
            Session::save();
        }
        
        self::$cart = $cart;
        return $cart[$shopId];
    }
    
    // 取得 pid
    public static function getPid($product)
    {
        if ($product['type'] == 'increase') {
            return md5('increase_'.$product['id'].'_'.$product['size']);    
        }
        
        return md5($product['id'].'_'.$product['size']);    
    }
    
    // 釋放購物車
    public static function flush()
    {
        self::$cart = null;
        Session::forget('SSCart');
        Session::save();
    }
    
    // 建立的初始動作
    public function __construct($shopId)
    {
        $this->shopId = $shopId;    
    }
    
    // 新增商品
    public function add($product)
    {
        $pid = SSCart::getPid($product);
        if (!isset($this->products[$pid])) {
            $this->products[$pid] = $product;    
        } else {
            $this->products[$pid]['qty'] += $product['qty'];
        }
        
        SSCart::$cart[$this->shopId] = $this;
        Session::put('SSCart', SSCart::$cart);
        Session::save();
        
        return $this;
    }
    
    // 刪除商品
    public function remove($pid)
    {
        if (isset($this->products[$pid])) {
            unset($this->products[$pid]);
            SSCart::$cart[$this->shopId] = $this;
            Session::put('SSCart', SSCart::$cart);
            Session::save();
        }

        return $this;
    }
    
    // 更改商品數量
    public function change($pid, $qty)
    {
        if (isset($this->products[$pid])) {
            $this->products[$pid]['qty'] = $qty;
            SSCart::$cart[$this->shopId] = $this;
            Session::put('SSCart', SSCart::$cart);
            Session::save();
        }

        return $this; 
    }
    
    // 取得所有商品
    public function getProducts()
    {
        return $this->products;
    }
    
    // 結算
    public function settlement($freight = 0, $freeFreight = 0)
    {
        $this->total = 0;
        $needCold    = false;
        $noStore     = false;
        foreach ($this->products as $product) {
            $this->total += $product['qty'] * $product['price'];
            $needCold     = $needCold || $product['need_cold'];
            $noStore      = $needCold || $product['no_store'];
        }    
        
        if (!empty($freeFreight) && $this->total >= $freeFreight) {
            $freight = 0;    
        }
        
        return [
            'total'    => $this->total,
            'freight'  => $freight,
            'products' => $this->products,
            'needCold' => $needCold,
            'noStore'  => $noStore,
            'count'    => $this->getCount()
        ];
    }
    
    // 釋放指定小官網的購物車
    public function clear()
    {
        if (isset(SSCart::$cart[$this->shopId])) {
            unset(SSCart::$cart[$this->shopId]);
            Session::put('SSCart', SSCart::$cart);
            Session::save();
        } 
    }
    
    // 取得購物車的數量
    public function getCount($countItemNumber = false)
    {
        if ($countItemNumber) {
            return count($this->products);
        }
        
        $count = 0;
        foreach ($this->products as $product) {
            $count += $product['qty'];
        }
        return $count;
    }
    
    // 檢查有沒有主商品
    public function hasMainProduct($excludePid = null)
    {
        foreach ($this->products as $pid => $product) {
            if (!is_null($excludePid)) {
                if ($excludePid == $pid) {
                    continue;
                }    
            }
            if ($product['type'] == 'main' || $product['type'] == 'package') {
                return true;
            }
        }
        return false;   
    }
    
    // 檢查有無超過上限
    public function checkOverLimit($productId, $qty, $maxAmount = null)
    {
        $amount = $qty;
        foreach ($this->products as $pid => $product) {
            if ($product['id'] == $productId) {
                $amount += $product['qty'];
            }    
        }
        
        return (($amount > $maxAmount) && !is_null($maxAmount));
    }
    
    // 檢查有無超過上限
    public function checkOverLimitInCart($pid, $qty)
    {
        if (isset($this->products[$pid])) {
            $qty = $qty - $this->products[$pid]['qty'];
            return $this->checkOverLimit($this->products[$pid]['id'], $qty, $this->products[$pid]['max_amount']);
        }
        
        return false;
    }
}
