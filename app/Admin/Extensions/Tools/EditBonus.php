<?php

namespace App\Admin\Extensions\Tools;

use Admin;
use App\Customer;
class EditBonus 
{
    protected $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    protected function script()
    {
        return <<<SCRIPT

        $('.grid-check-row').on('click', function () {

            // Your code.
            console.log($(this).data('id'));

        });

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());
        $customer=Customer::find($this->id);
        return '<button type="button" class="btn btn-primary " data-toggle="modal" data-target="#modal'.$this->id.'">修改紅利</button>
        <div class="modal fade" id="modal'.$this->id.'" tabindex="-1" role="dialog" aria-labelledby="favoritesModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="favoritesModalLabel">修改會員'.$customer->name.'的紅利 </h4>
              </div>
              <form id="form" method="POST" action="BonusEdit">
                  <div class="modal-body">
                    <p>
                        目前紅利'.$customer->bonus.'
                    </p>
                        '.csrf_field().'
                        <input type="hidden" name="id" value="'.$this->id.'">
                        <label>調整後點數</label> <input type="number" name="bonus" value="0" min="0">
                  </div>
                  <div class="modal-footer">
                    <span class="">
                      <button type="submit" class="btn btn-primary">
                        確認
                      </button>
                    </span>
                    <button type="button" class="btn btn-default" data-dismiss="modal">關閉</button>
                  </div>
                </form>

            </div>
          </div>
        </div>';
    }

    public function __toString()
    {
        return $this->render();
    }
}