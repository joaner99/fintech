<?php

namespace App\Admin\Extensions\Tools;

use Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;
use App\AdminRole;
use Session;
use Auth;
class Adds extends AbstractTool
{
    protected function script()
    {

    }

    public function render()
    {
    	$user=new AdminRole;
    	if(Admin::user()->id!=1){
    		$user->find(Admin::user()->id);
    	}else{
    		$user=$user->all();
    	}
        return view('tools.adds',['user'=>$user]);
    }
}