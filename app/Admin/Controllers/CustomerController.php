<?php

namespace App\Admin\Controllers;

use App\Customer;
use App\CustomerClass;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Bonus;
use App\Order;
use App\Admin\Extensions\Tools\Adds;
use DB;
use App\Admin\Extensions\Tools\EditBonus;
class CustomerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('會員清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('會員資料修改')
            ->description('description')
            ->body($this->form($id)->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Customer);
        $grid->model()->select('*','id as c_id','id as bonus_id');
        $grid->id('id');
        $grid->name('會員名稱');
        $grid->email('Email');
        $grid->customer_class_id('類別')->display(function($customer_class_id) {
            $name=CustomerClass::where('id',$customer_class_id)->first();
            if(!$name){
                return '';
            }
            return $name->name; 
        });
        $grid->phone('電話');
        $grid->bonus_id('紅利')->display(function($bonus_id) {
               return'<form class="form-horizontal" role="form" method="get" action="'.asset('admin/Bonus').'">  '.
               '<input type="hidden" name="customer_id" value="'.$bonus_id.'">'.
               '<button type="submit" class="btn btn-primary">
                                    連結
                                </button></form>';
        });
        $grid->c_id('訂單')->display(function($c_id) {
               return'<form class="form-horizontal" role="form" method="get" action="'.asset('admin/Order').'">  '.
               '<input type="hidden" name="customer_id" value="'.$c_id.'">'.
               '<button type="submit" class="btn btn-primary">連結</button></form>';
        });
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->append(new EditBonus($actions->getKey()));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Customer::findOrFail($id));

        $show->oauth_type('Oauth type');
        $show->oauth_id('Oauth id');
        $show->name('Name');
        $show->email('Email');
        $show->city('City');
        $show->address('Address');
        $show->phone('Phone');
        $show->bonus('Bonus');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id=0)
    {
        $form = new Form(new Customer);
        $form->text('name', '會員名稱');
        $form->select('customer_class_id','會員類別')->options( CustomerClass::whereNull('deleted_at')->pluck('name', 'id'));
        $form->text('member_id', '會員編號');
        $form->email('email', 'Email');
        $form->text('city', '城市');
        $form->text('address', '地址');
        $form->mobile('phone', '電話');
        $form->text('bonus', '紅利')->readonly();
        return $form;
    }
}
