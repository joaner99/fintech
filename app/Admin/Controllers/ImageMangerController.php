<?php

namespace App\Admin\Controllers;

use App\ImageManger;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ImageMangerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('圖床清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('圖床修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new ImageManger);
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->whereHas('user', function ($filter) {
                $filter->where('name', 'like', "%{$this->input}%");
            });
        });
        $grid->filename()->display(function($filename) {
            $user_id=ImageManger::where('filename','=',$filename)->first();
            if(empty($user_id->user_id)){
                $user_id="不存在";
            }else{
                $user_id=$user_id->user_id;
            }
            return s3Asset('gallery/'.getGalleryId($user_id).'/'.$filename);
        })->lightbox();
        $grid->user()->name('使用者姓名');
        $grid->size('Size')->display(function($size) {
            return $size / 1000 .'kb';
        });
        $grid->deleted_at('停權時間');
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableEdit();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(ImageManger::findOrFail($id));

        $show->id('Id');
        $show->filename('Filename');
        $show->type('Type');
        $show->user_id('User id');
        $show->size('Size');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new ImageManger);

        $form->number('type', 'Type');
        $form->number('user_id', 'User id');
        $form->number('size', 'Size');

        return $form;
    }
}
