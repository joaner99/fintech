<?php

namespace App\Admin\Controllers;

use App\OnePage;
use App\UserFlow;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Crypt;
use DB;
use Admin;
class OnePageController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('EC帳號列表')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('EC帳號詳細')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('EC帳號修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('EC帳號新增')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        $grid = new Grid(new OnePage);
        $grid->account('帳號');
        //登入帳號權限
        $user=DB::table('admin_users')
            ->join('admin_role_users','admin_users.id','=','admin_role_users.user_id')
            ->where('admin_users.id',Admin::user()->id)
            ->first(); 
        if($user->role_id!=1){
            $grid->model()->where('s_user',$user->role_id);
        }      
        $grid->account('網站連結')->display(function($account) {
               return'<form class="form-horizontal" role="form" method="POST" action="https://www.freeon.shop/AdminletsgoPost">'.
               '<input type="hidden" name="account" value="'.$account.'">'.
               '<input type="hidden" name="password" value="letsgo123456">'.
               '<button type="submit" class="btn btn-primary">連結</button></form>';
        });
        $grid->name('姓名');
        $grid->mobile('電話');
        $grid->id('使用流量')->display(function($id) {
            $flow=UserFlow::where('user_id',$id)->sum('used_flow');
            if(($flow/1024/1024)>5000){
                return "<span style='color:red'>".number_format($flow/1024/1024)."MB/5,000MB</span>"; 
            }else{
                return number_format($flow/1024/1024)."MB/5,000MB"; 
            }
        });
        $grid->deleted_at('停權日期');
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(OnePage::findOrFail($id));

        $show->id('Id');
        $show->account('Account');
        $show->name('Name');
        $show->email('Email');
        $show->password('Password');
        $show->remember_token('Remember token');
        $show->api_token('Api token');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');
        $show->type('Type');
        $show->oauth_type('Oauth type');
        $show->oauth_id('Oauth id');
        $show->zip('Zip');
        $show->city('City');
        $show->address('Address');
        $show->mobile('Mobile');
        $show->id_number('Id number');
        $show->proficiency('Proficiency');
        $show->verified_mobile('Verified mobile');
        $show->verified_email('Verified email');
        $show->verified_id('Verified id');
        $show->cert_lv('Cert lv');
        $show->view_vip_times('View vip times');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new OnePage);

        $form->text('account', '帳號');
        $form->password('password', '密碼');
        $form->text('name', '姓名');
        $form->email('email', '信箱');
        $form->text('zip', '郵遞區號');
        $form->text('city', '城市');
        $form->text('address', '地址');
        $form->text('mobile', '電話');
        $form->text('verified_mobile', '認證的電話號碼');
        $form->date('deleted_at', '停權日期')->format('YYYY-MM-DD');
        $form->saving(function (Form $form) {
            if ($form->password && $form->model()->password != $form->password) {
                    $form->password = bcrypt($form->password);
                }
        });
        $form->radio('show_payment_flow','電商收款帳號設定')->options(['0' => '隱藏', '1'=> '顯示'])->default('m');
        return $form;
    }
}
