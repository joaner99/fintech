<?php

namespace App\Admin\Controllers;

use App\UsersConfig;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UsersConfigController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('運費管理清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('運費管理修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UsersConfig);
        $grid->user()->account('帳號');
        $grid->config('運費設定')->display(function($config) {
            if(!empty($config['freight']['home'])){
            return "<span>宅配運費：".$config['freight']['home']['price']."</span><br>"."<span>宅配免運金額：".$config['freight']['home']['free']."</span>"."<br><span>冷凍宅配運費：".$config['freight']['home_cold']['price']."</span><br><span>冷凍宅配免運金額：".$config['freight']['home_cold']['free']."</span>"."<br><span>離島運費：".$config['freight']['out']['price']."</span>"."<br><span>離島免運金額：".$config['freight']['out']['free']."</span>"."<br><span>自取運費：".$config['freight']['self_pickup']['price']."</span>"."<br><span>自取免運金額：".$config['freight']['self_pickup']['free']."</span>";
            }else{
                return "用戶尚未建立";
            }
        });



        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UsersConfig::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->config('Config');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UsersConfig);
        $form->hidden('array_type')->default('123');
        $form->embeds('config','設定檔', function ($form) {
            $form->embeds('payments','金流', function ($form) {
                $form->embeds('ATM','銀行戶', function ($form) {
                    $form->text('bank_no','銀行代號');
                    $form->text('bank_account','銀行帳號');
                });
            });
            $form->embeds('freight','運費', function ($form) {
                $form->embeds('home','宅配', function ($form) {
                    $form->text('price','運費');
                    $form->text('free','免運金額');
                });
                $form->embeds('home_cold','冷凍宅配', function ($form) {
                    $form->text('price','運費');
                    $form->text('free','免運金額');
                });
                $form->embeds('out','離島', function ($form) {
                    $form->text('price','運費');
                    $form->text('free','免運金額');
                });         
                $form->embeds('self_pickup','自取', function ($form) {
                    $form->text('price','運費');
                    $form->text('free','免運金額');
                });

            });
        });
        return $form;
    }
}
