<?php

namespace App\Admin\Controllers;

use App\VipOrder;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class VipOrderController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('繳費紀錄清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('繳費紀錄修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new VipOrder);

        $grid->id('Id');
        $grid->merchant_trade_no('金流編號');
        $grid->company_title('公司名稱');
        $grid->ein('統編');
        $grid->user()->name('會員名稱');
        $grid->email('Email');
        $grid->price('金額');
        $grid->vip_name('方案');
        $grid->days('Days');
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(VipOrder::findOrFail($id));

        $show->id('Id');
        $show->status('Status');
        $show->pay_status('Pay status');
        $show->merchant_trade_no('Merchant trade no');
        $show->payment_method('Payment method');
        $show->bank_no('Bank no');
        $show->bank_account('Bank account');
        $show->email('Email');
        $show->ein_type('Ein type');
        $show->company_title('Company title');
        $show->ein('Ein');
        $show->city('City');
        $show->address('Address');
        $show->user_vip_id('User vip id');
        $show->user_id('User id');
        $show->order_time('Order time');
        $show->effective_time('Effective time');
        $show->price('Price');
        $show->vip_plans_id('Vip plans id');
        $show->vip_type('Vip type');
        $show->vip_name('Vip name');
        $show->vip_level('Vip level');
        $show->one_page_limit('One page limit');
        $show->fb_page_limit('Fb page limit');
        $show->days('Days');
        $show->flow('Flow');
        $show->own_domain('Own domain');
        $show->email_notify('Email notify');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new VipOrder);

        $form->display('pay_status', '付款狀態');
        $form->display('merchant_trade_no', '金流編號');
        $form->display('payment_method', '付款方式');
        $form->display('email', 'Email');
        $form->display('company_title', '公司名稱');
        $form->display('ein', '統編');
        $form->display('city', '城市');
        $form->display('address', '地址');
        $form->display('user.name', '名稱');
        $form->display('order_time', '購買時間')->default(date('Y-m-d H:i:s'));
        $form->display('price', '單價');
        $form->display('vip_name', '方案名稱');
        $form->display('days', '天數');

        return $form;
    }
}
