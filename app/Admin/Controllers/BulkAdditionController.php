<?php

namespace App\Admin\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Admin;

class BulkAdditionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('公司簡介')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('公司簡介')
            ->description('詳細')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('公司簡介')
            ->description('修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('公司簡介')
            ->description('新建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new About);

        $grid->id('Id');
        $grid->content('公司簡介');
        $grid->img1('圖片')->gallery(['zooming' => true]);
        $grid->disableCreateButton();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        $grid->disableRowSelector();
        $grid->disableExport();
        $grid->updated_at('更新時間');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(About::findOrFail($id));

        $show->id('Id');
        $show->title1('Title1');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        
    }
    public function store()
    {
        $array=array();
        for ($i=1; $i <=$_POST['quantity'] ; $i++) {
            $account=substr(md5(uniqid(rand(), true)),0,30).'@gmail.com'; 
            $array[]=array(
                'account'=>$account,
                'name'=>'預設使用者',
                'email'=>$account,
                'password'=>'$2y$10$zKb2/Uqm/LlAdqwHoEOzcenRNFcTWG5SLEXreL3oias7kYdoTIW62',
                'oauth_type'=>'user',
                'type'=>'user',
                's_user'=>$_POST['s_user'],
                'program'=>$_POST['program'],
                'fintech_id'=>Admin::user()->id
            );
        }
        User::insert($array);
        return redirect()->back();
    }
}
