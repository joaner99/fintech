<?php

namespace App\Admin\Controllers;

use App\Banner;
use App\ZoneOption;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Order;
use App\Bonus;
use Admin;
use App\Customer;

class BonusEditController  extends Controller
{
    use HasResourceActions;
    public function edit()
    {
        $data=$_POST;
        $customer=Customer::find($data['id']);
        $order=[
            'no'               =>rand('111111111','999999999'),
            'shop_id'          =>'1',
            'payment_method'   =>'',
            'merchant_trade_no'=>rand('11111','99999'),
            'status'           =>'',
            'delivery_type'    =>'',
            'total_price'      =>0 ,
            'freight'          =>0 ,
            'freight'          =>'',
            'recipient_name'   =>'',
            'phone'            =>'',
            'zip'              =>'',
            'city'             =>'',
            'address'          =>'',
            'email'            =>'',
            'remark'           =>'',
            'email_notify'     =>'',
            'resend_times'     =>'',
            'process_finish'   =>'',
            'print_type'       =>0,
            'bonus_type'       =>'',
            'pay_status'       =>0,
            'status'           =>0 ,
            'freight'          =>0 ,
            'email_notify'     =>0 ,
            'resend_times'     =>0 ,
            'process_finish'   =>1 ,
            'customer_id'      =>$data['id']
        ];
        $id=Order::create($order)->id;
        $bonus=$data['bonus']-$customer->bonus;
        $Detail=[
            'order_id'=>$id,
            'filename'=>'',
            'prod_name'=>'調整紅利點數',
            'qty'=>0,
            'price'=>0,
            'size'=>'',
            'prod_id'=>0,
            'parent_prod'=>0,
            'desc'=>0,
            'marketing_type'=>0,
            'bonus'=>$bonus,
            'editer'=>Admin::user()->id
        ];
        Bonus::create($Detail);
        Customer::where('id',$data['id'])->update(['bonus'=>$data['bonus']]);
        return redirect()->to('admin/Customer');
    }
}
