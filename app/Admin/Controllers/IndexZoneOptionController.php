<?php

namespace App\Admin\Controllers;

use App\IndexZoneOption;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\ZoneOption;
use App\Shop;


class IndexZoneOptionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('首頁專區廣告版位設定')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('首頁專區廣告版位設定')
            ->description('詳細')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('首頁專區廣告版位設定')
            ->description('修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('首頁專區廣告版位設定')
            ->description('新增')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new IndexZoneOption);

        $grid->id('Id');
        $grid->class_id('首頁專區類別');
        $grid->file1('縮圖')->lightbox();
        $grid->title('標題');
        $grid->title2('小標');
        $grid->url('連結');
        $grid->updated_at('更新時間');
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        $grid->disableRowSelector();
        $grid->disableExport();
        $grid->disableCreateButton();
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(IndexZoneOption::findOrFail($id));

        $show->id('Id');
        $show->zone_option_id('Zone option id');
        $show->file('File');
        $show->title('Title');
        $show->url('Url');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new IndexZoneOption);
        $form->tab('一頁商店', function ($form) {
            $form->select('url', '一頁商店連結')->options(Shop::whereNull('deleted_at')->pluck('title', 'short_url'));

        })->tab('自訂義', function ($form) {
            $form->text('url2', '自訂連結');

        })->tab('順序及專區', function ($form) {
            $form->select('class_id', '首頁專區類別')->options(['活動企劃'=>'活動企劃','特色商品'=>'特色商品','推薦新品'=>'推薦新品','精選商店'=>'精選商店'])->rules('required');
            $form->image('file1', '縮圖');
            $form->text('title', '標題')->rules('required');
            $form->text('title2', '小標');
        });
        $form->footer(function ($footer) {

            // 去掉`继续编辑`checkbox
            $footer->disableEditingCheck();

            // 去掉`继续创建`checkbox
            $footer->disableCreatingCheck();

        });


        return $form;
    }
}
