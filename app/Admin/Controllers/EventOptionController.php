<?php

namespace App\Admin\Controllers;

use App\EventOption;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\ZoneOption;
use App\Shop;

class EventOptionController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('專區廣告版位設定')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('專區廣告版位設定')
            ->description('詳細')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('專區廣告版位設定')
            ->description('修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('專區廣告版位設定')
            ->description('新增')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new EventOption);
        $grid->model()->orderby('zone_option_id','asc')->orderby('sort','asc');
        $grid->id('Id');
        $grid->ZoneOption()->name('專區名稱');
        $grid->file('縮圖')->lightbox();
        $grid->title('標題');
        $grid->url('一頁商店連結');
        $grid->url2('自訂連結');
        $grid->sort('順序');
        $grid->updated_at('更新時間');
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(EventOption::findOrFail($id));

        $show->id('Id');
        $show->zone_option_id('Zone option id');
        $show->file('File');
        $show->title('Title');
        $show->url('Url');
        $show->url2('Url2');
        $show->sort('Sort');
        $show->created_at('Created at');
        $show->updated_at('Updated at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new EventOption);


        $form->tab('一頁商店', function ($form) {
            $form->select('url', '一頁商店連結')->options(Shop::whereNull('deleted_at')->pluck('title', 'short_url'));
        })->tab('自訂義', function ($form) {
            $form->text('url2', '自訂連結');
        })->tab('順序與專區', function ($form) {
            $form->select('zone_option_id', '首頁專區類別')->options(ZoneOption::all()->pluck('name', 'id'))->rules('required');
            $form->number('sort', '順序')->default('999');
            $form->text('title', '標題')->rules('required');
            $form->image('file', '縮圖');
            

        });


       
        return $form;
    }
}
