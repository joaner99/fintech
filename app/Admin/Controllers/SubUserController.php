<?php

namespace App\Admin\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Session;
use Admin;
use App\Admin\Extensions\Tools\Adds;
use App\AdminUsers;
use App\AdminRole;
use DB;
class SubUserController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('帳號管理')
            ->description('列表')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('帳號管理')
            ->description('明細')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('帳號管理')
            ->description('修改')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('帳號管理')
            ->description('新建')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);
        //登入帳號的權限
        $user=DB::table('admin_users')
            ->join('admin_role_users','admin_users.id','=','admin_role_users.user_id')
            ->where('admin_users.id',Admin::user()->id)
            ->first();
        //
        if($user->role_id!=1){
             $grid->model()->where('s_user',$user->role_id);
        }
        $grid->id('Id');
        $grid->s_user('管理員')->display(function($s_user) {
            $name=AdminRole::where('id',$s_user)->first();
            if(!$name){
                return '';
            }
            return $name->name; 
        });
        $grid->name('Name');
        $grid->account('帳號');
        $grid->program('方案');
        $grid->email('Email');
        $grid->disableCreateButton();
        $grid->tools(function ($tools) {
            $tools->append(new Adds());
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));
        $show->id('Id');
        $show->name('姓名');
        $show->email('Email');
        $show->tel('電話');
        $show->address('地址');
        $show->password('Password');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);
        $form->text('name', '姓名');
        $form->email('email', 'Email');
        $form->radio('show_payment_flow','電商收款帳號設定')->options(['0' => '隱藏', '1'=> '顯示'])->default('m');
        // $form->password('password', 'Password');
        
        return $form;
    }
}
