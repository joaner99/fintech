<?php

namespace App\Admin\Controllers;

use App\Order;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OrderController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('訂單清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('訂單修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Order);
        if(isset($_GET['customer_id'])){
            $grid->model()->where('customer_id',$_GET['customer_id']);
        }
        $grid->no('No');
        $grid->shop()->title('商店名稱')->limit(30);
        $grid->recipient_name('收件人');
        $grid->merchant_trade_no('金流編號');
        $grid->payment_method('付款方式');
        $grid->total_price('總金額');
        $grid->phone('電話');
        $grid->city('城市');
        $grid->address('地址');
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Order::findOrFail($id));

        $show->id('Id');
        $show->no('No');
        $show->shop_id('Shop id');
        $show->payment_method('Payment method');
        $show->merchant_trade_no('Merchant trade no');
        $show->status('Status');
        $show->pay_status('Pay status');
        $show->delivery_type('Delivery type');
        $show->trade_no('Trade no');
        $show->total_price('Total price');
        $show->freight('Freight');
        $show->recipient_name('Recipient name');
        $show->phone('Phone');
        $show->zip('Zip');
        $show->city('City');
        $show->address('Address');
        $show->email('Email');
        $show->remark('Remark');
        $show->ship_no('Ship no');
        $show->ship_name('Ship name');
        $show->ship_address('Ship address');
        $show->store_type('Store type');
        $show->st_code('St code');
        $show->email_notify('Email notify');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');
        $show->customer_id('Customer id');
        $show->atm_last_code('Atm last code');
        $show->note('Note');
        $show->delivery_time('Delivery time');
        $show->type('Type');
        $show->bank_no('Bank no');
        $show->bank_account('Bank account');
        $show->resend_times('Resend times');
        $show->ship_phone('Ship phone');
        $show->line_id('Line id');
        $show->process_finish('Process finish');
        $show->cvs_payment_no('Cvs payment no');
        $show->cvs_validation_no('Cvs validation no');
        $show->print_type('Print type');
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Order);

        $form->display('no', '訂單編號');
        $form->display('shop.title', '商店名稱');
        $form->display('payment_method', '付款方式');
        $form->display('merchant_trade_no', '金流編號');
        $form->display('pay_status', '付款狀態(100為付款完成)');
        $form->display('delivery_type', '運送方式');
        $form->display('trade_no', '物流單號');
        $form->display('total_price', '總金額');
        $form->display('freight', '運費');
        $form->display('recipient_name', '收件人');
        $form->display('phone', '電話');
        $form->display('zip', '郵遞區號');
        $form->display('city', '城市');
        $form->display('address', '地址');
        $form->email('email', 'Email');
        $form->display('remark', '備註');
        return $form;
    }
}
