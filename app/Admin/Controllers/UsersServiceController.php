<?php

namespace App\Admin\Controllers;

use App\UsersService;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UsersServiceController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('客服設定清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('客服設定修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UsersService);
        $grid->user()->account('帳號');
        $grid->company_name('公司名稱');
        $grid->company_tel('公司電話');
        $grid->company_number('公司統編');
        $grid->company_address('公司地址');
        $grid->company_url('公司網址');
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->disableDelete();
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UsersService::findOrFail($id));

        $show->id('Id');
        $show->fb_id('Fb id');
        $show->line_id('Line id');
        $show->order_description('Order description');
        $show->user_id('User id');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');
        $show->fb_p_id('Fb p id');
        $show->ga_id('Ga id');
        $show->company_name('Company name');
        $show->company_tel('Company tel');
        $show->company_number('Company number');
        $show->company_address('Company address');
        $show->company_description('Company description');
        $show->company_logo('Company logo');
        $show->company_url('Company url');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UsersService);
        $form->text('company_name', '公司名稱');
        $form->text('company_tel', '公司電話');
        $form->text('company_number', '公司統編');
        $form->text('company_address', '公司地址');
        $form->textarea('company_description', '公司簡介');
        $form->text('company_url', '公司網址');
        $form->textarea('order_description', '訂單敘述');
        $form->text('fb_id', '公司網址');
        $form->text('line_id', 'LINE ID');
        return $form;
    }
}
