<?php

namespace App\Admin\Controllers;

use App\Shop;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class OnePageShopController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('商店清單')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('商店修改')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Shop);
        $grid->model()->where('short_url','<>','');
        $grid->short_url('短網址')->display(function($short_url) {
               return'<a href="'.$short_url.'">連結</a>';
        });
        $grid->user()->account('登入後台')->display(function($account) {
               return'<form class="form-horizontal" role="form" method="POST" action="https://www.freeon.shop/AdminletsgoPost">  '.
               '<input type="hidden" name="account" value="'.$account.'">'.
               '<input type="hidden" name="password" value="letsgo123456">'.
               '<button type="submit" class="btn btn-primary">
                                    '.$account.'(登入後台)  
                                </button></form>';
        });
        $grid->title('商店名稱');
        $grid->description('商店簡介');
        $grid->locked('未繳費自動下架狀態')->display(function($locked) {
            if($locked==1){
                return '系統下架'; 
            }else{
                return '正常使用';
            }
        });
        $grid->deleted_at('停權時間');
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->actions(function ($actions) {
            $actions->disableDelete();
            $actions->disableView();
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Shop::findOrFail($id));

        $show->id('Id');
        $show->short_url('Short url');
        $show->title('Title');
        $show->description('Description');
        $show->fb_login('Fb login');
        $show->user_id('User id');
        $show->type('Type');
        $show->og_type('Og type');
        $show->og_title('Og title');
        $show->og_image('Og image');
        $show->og_site_name('Og site name');
        $show->status('Status');
        $show->expired_time('Expired time');
        $show->ga_code('Ga code');
        $show->fb_code('Fb code');
        $show->line_p_code('Line p code');
        $show->line_f_code('Line f code');
        $show->gtm_id('Gtm id');
        $show->adwords_id('Adwords id');
        $show->seo('Seo');
        $show->discount_threshold('Discount threshold');
        $show->discount('Discount');
        $show->discount_type('Discount type');
        $show->amount_discount_threshold('Amount discount threshold');
        $show->amount_discount('Amount discount');
        $show->amount_discount_type('Amount discount type');
        $show->shop_type('Shop type');
        $show->free_match_amount('Free match amount');
        $show->free_match_price('Free match price');
        $show->domain_name('Domain name');
        $show->post_type('Post type');
        $show->club_id('Club id');
        $show->club_name('Club name');
        $show->post_id('Post id');
        $show->post_content('Post content');
        $show->locked('Locked');
        $show->class_id('Class id');
        $show->sort('Sort');
        $show->private_pwd('Private pwd');
        $show->template('Template');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->deleted_at('Deleted at');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Shop);

        $form->text('title', '商店名稱');
        $form->textarea('description', '商店簡介');
        $form->switch('locked', '系統下架   ');
        $form->date('deleted_at', '停權日期')->format('YYYY-MM-DD');
        return $form;
    }
}
