<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();
Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('indexbanner', IndexBannerController::class);
    $router->resource('ZoneOption', ZoneOptionController::class);
    $router->resource('Marquee', MarqueeController::class);
    $router->resource('Banner', BannerController::class);
    $router->resource('IndexZoneOption', IndexZoneOptionController::class);
    $router->resource('EventOption', EventOptionController::class);
    $router->resource('OnePage', OnePageController::class);
    $router->resource('Customer', CustomerController::class);
    $router->resource('CustomerClass', CustomerClassController::class);
    $router->resource('Order', OrderController::class);
    $router->resource('VipOrder', VipOrderController::class);
    $router->resource('UsersConfig', UsersConfigController::class);
    $router->resource('UsersService', UsersServiceController::class);
    $router->resource('ImageManger', ImageMangerController::class);
    $router->resource('OnePageShop', OnePageShopController::class);
    $router->resource('About', AboutController::class);
    $router->resource('Contact', ContactController::class);
    $router->resource('BonusConfig', BonusConfigController::class);
    $router->resource('ServiceItem', ServiceItemController::class);
    $router->resource('Industry', IndustryController::class);
    $router->resource('IndustryDetail', IndustryDetailController::class);
    $router->resource('SubUser', SubUserController::class);
    $router->resource('BulkAddition', BulkAdditionController::class);
    $router->resource('Bonus', BonusController::class);
    $router->post('BonusEdit', 'BonusEditController@edit');
});



