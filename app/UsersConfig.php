<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersConfig extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'users_config';

    public function setConfigAttribute($config)
	{
	    if (is_array($config)) {
	        $this->attributes['config'] = json_encode($config);
	    }
	}
	public function getConfigAttribute($config)
	{
	    return json_decode($config, true);
	}

	public function User()
    {
        return $this->belongsTo(User::class);
    }
	
}
