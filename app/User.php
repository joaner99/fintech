<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','tel','address'
    ];
    protected $connection = 'mysql_admin_for_letsgo';
    protected $table = 'users';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function ImageManger()
    {
        return $this->hasOne(ImageManger::class);
    }
    public function VipOrder()
    {
        return $this->hasOne(VipOrder::class);
    }

    public function UserConfig()
    {
        return $this->hasOne(UserConfig::class);
    }
    public function UserService()
    {
        return $this->hasOne(UserService::class);
    }

    public function Shop()
    {
        return $this->hasOne(Shop::class);
    }

}
