<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZoneOption extends Model
{
    public function Banner()
    {
        return $this->hasMany(Banner::class);
    }

    public function EventOption()
    {
        return $this->hasMany(EventOption::class);
    }
    
}
