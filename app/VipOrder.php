<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VipOrder extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'vip_order';

	public function User()
    {
        return $this->belongsTo(User::class);
    }
}
