<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'customer';
}
