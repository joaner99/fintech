<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'order';
	protected $fillable = [
        	'no'               ,
            'shop_id'          ,
            'payment_method'   ,
            'merchant_trade_no',
            'status'           ,
            'delivery_type'    ,
            'total_price'      ,
            'freight'          ,
            'recipient_name'   ,
            'phone'            ,
            'zip'              ,
            'city'             ,
            'address'          ,
            'email'            ,
            'remark'           ,
            'email_notify'     ,
            'resend_times'     ,
            'process_finish'   ,
            'print_type'       ,
            'bonus_type'       ,
            'pay_status'	   ,
            'pay_status'       ,
            'status'           ,
            'email_notify'     ,
            'resend_times'     ,
            'process_finish'   ,
            'customer_id'

    ];
	public function Bonus()
    {
        return $this->belongsTo(Bonus::class,'id','order_id');
    }
} 	
