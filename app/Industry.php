<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
	// protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'admin_industry';
	
	public function setShopAttribute($shop)
	{
	    if (is_array($shop)) {
	        $this->attributes['shop'] = json_encode($shop);
	    }
	}
	public function getShopAttribute($shop)
	{
	    return json_decode($shop, true);
	}

	public function User()
    {
        return $this->belongsTo(User::class);
    }
}
