<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'order_products';
	protected $fillable = [
        	'order_id'          ,
            'filename'          ,
            'prod_name'         ,
            'qty'               ,
            'price'             ,
            'size'    			,
            'prod_id'      		,
            'parent_prod'       ,
            'desc'              ,
            'marketing_type'    ,
            'bonus'             ,
            'editer'            
      
    ];
	 public function Order()
    {
        return $this->hasOne(Order::class);
    }
} 	
