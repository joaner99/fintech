<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'shop';
	public function Order()
    {
        return $this->hasOne(Order::class);
    }
    public function User()
    {
        return $this->belongsTo(User::class);
    }
	
}
