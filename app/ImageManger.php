<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImageManger extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'users_gallery';

    public function User()
    {
        return $this->belongsTo(User::class);
    }

}
