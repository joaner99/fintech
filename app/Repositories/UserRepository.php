<?php
namespace App\Repositories;

use App\User;
use App\Models\User\UserPayment;
use App\Models\User\UserConfig;
use App\Models\User\UserLogistics;
use App\Models\User\UserService;
use App\Models\User\UserUsedFlow;
use DB;
use Hash;

class UserRepository
{
    const NEWEST_CONFIG_VERSION = 5;
    
    // 取得使用者的服務設定
    public static function getService($user_id)
    {
        $service = UserService::where('user_id', $user_id)
            ->first();
        if (!$service) {
            $service = new UserService;
            $service->user_id = $user_id;
            $service->save();
        }
        
        return $service;
    }
    
    // 檢查使用者有沒有有效的金流設定
    public static function hasValidPayments($user_id, $shop_type = null)
    {
        $config = self::getConfig($user_id);
        $config = json_decode($config->config, true);
        
        $validPayments = [];
        foreach ($config['payment_used'] as $used) {
            if (empty($used)) {
                continue;
            }  
            
            if (!isset($config['payments'][$used])) {
                continue;    
            }
            $pass = true;
            if (is_array($config['payments'][$used])) {
                foreach ($config['payments'][$used] as $value) {
                    $pass &= !empty($value);        
                }  
            } else {
                $pass = !empty($config['payments'][$used]);
            }
            
            if ($pass) {
                $validPayments[] = $used;
            }
        }

        if (count($validPayments) == 1) {
            if ($validPayments[0] == 'COD' || $validPayments[0] == 'InPerson' || $validPayments[0] == 'CCOD') {
                $user = User::find($user_id);
                return ($user->verified_mobile == $user->mobile && !empty($user->verified_mobile)) || ($user->cert_lv == 5);
            }
        }
        return count($validPayments) > 0;
    }
    
    // 取得有效的付款方式和物流方式
    public static function getValidPaymentsAndLogistics($user_id, $shop_type = null)
    {
        $config = self::getConfig($user_id);
        $config = json_decode($config->config, true);
        
        $valid = [
            'payments'  => [],
            'logistics' => [],
        ];
        
        $hasHome = false;
        foreach ($config['payment_used'] as $type => $class) {
            if (empty($class)) {
                continue;
            }  
            switch ($type) {
                case 'atm':
                case 'credit':
                case 'staging':
                    $pass = true;
                    if (!empty($config['payments'][$class])) {
                        foreach ($config['payments'][$class] as $value) {
                            $pass &= !empty($value);        
                        }  
                    } else {
                        $pass = false;
                    }
                    if ($pass) {
                        $valid['payments'][] = $type;
                        //$hasHome             = true;
                    }
                    break;
                case 'cod':
                    if (!empty($config['payments']['COD']) && !empty($config['payment_used']['cod'])) {
                        $valid['payments'][] = 'cod';  
                        $hasHome             = true;
                    }
                    break;
                case 'ccod':
                    if (!empty($config['payments']['CCOD']) && !empty($config['payment_used']['ccod'])) {
                        $valid['payments'][] = 'ccod';  
                        $hasHome             = true;
                    }
                    break;
                case 'storepay':
                    if (!empty($config['payments']['StorePay']) && !empty($config['payment_used']['storepay'])) {
                        $valid['payments'][] = 'storepay'; 
                    }    
                    break;
                case 'in_person':
                    if (!empty($config['payments']['InPerson']) && !empty($config['payment_used']['in_person'])) {
                        $valid['payments'][] = 'in_person'; 
                    } 
                    break;
            }
        }
        
        foreach ($config['logistic_used'] as $type => $class) {
            if (empty($class)) {
                continue;
            }  
            switch ($type) {
                case 'store':
                    $pass = true;
                    if (!empty($config['logistics'][$class])) {
                        $c = 0;
                        if (in_array('cod', $valid['payments'])) {
                            $c++;
                        }
                        if (in_array('ccod', $valid['payments'])) {
                            $c++;
                        }
                        if (in_array('in_person', $valid['payments'])) {
                            $c++;
                        }
                        if (
                            (count($valid['payments']) == 1 && ($valid['payments'][0] == 'cod' || $valid['payments'][0] == 'in_person') || $valid['payments'][0] == 'ccod') ||
                            (count($valid['payments']) == $c)
                           ) {
                            $pass = false;
                        } else {
                            foreach ($config['logistics'][$class] as $value) {
                                $pass &= !empty($value);        
                            }  
                        }
                    } else {
                        $pass = false;
                    }
                    if ($pass) {
                        $valid['logistics'][] = 'store';
                    }
                    break;
                case 'home':
                    if (count($valid['payments']) == 1) {
                        $hasHome |= $valid['payments'][0] != 'storepay';  
                        $hasHome |= $valid['payments'][0] != 'in_person';
                    } elseif (count($valid['payments']) == 2) {
                        $hasHome |= !in_array('storepay', $valid['payments']);  
                        $hasHome |= !in_array('in_person', $valid['payments']);
                    } else {
                        $hasHome = true;    
                    }
                    break;
                case 'self_pickup':
                    if (!empty($config['logistics']['SelfPickup']) && !empty($config['logistic_used']['self_pickup']) && in_array('in_person', $valid['payments'])) {
                        $valid['logistics'][] = 'self_pickup'; 
                    } 
                    break;
            }
        }
        
        $hasHome |= (count($valid['logistics']) == 0);
        if ($hasHome) {
            $valid['logistics'][] = 'home';
            $valid['logistics'][] = 'home_cold';
        }

        return $valid;
    }

    // 取得有效的付款設定和物流設定
    public static function getValidThirdPaymentsAndThirdLogistics($user_id)
    {
        $config = self::getConfig($user_id);
        $config = json_decode($config->config, true);
        
        $valid = [
            'payments'  => [],
            'logistics' => [],
        ];
        
        foreach ($config['payments'] as $type => $data) {
            if (empty($data)) {
                continue;    
            }   
            if (!in_array($type, ['COD', 'StorePay', 'InPerson', 'CCOD'])) {
                $pass = true;
                foreach ($data as $param) {
                    $pass &= !empty($param);
                }
                if ($pass) {
                    $valid['payments'][] = $type;
                }
            } else {
                $valid['payments'][] = $type;
            }
        }

        foreach ($config['logistics'] as $type => $data) {
            if (empty($data)) {
                continue;    
            }    
            $pass = true;
            foreach ($data as $param) {
                $pass &= !empty($param);
            }
            if ($pass) {
                $valid['logistics'][] = $type;
            }
        }

        return $valid;
    }
    
    
    // 檢查使用者的服務設定是否完整
    public static function serviceIsCompleted($user_id)
    {
        $service = UserService::where('user_id', $user_id)
            ->first();
        
        if (!$service) {
            return false;
        } 
        
        return !empty($service->order_description);
    }
    
    // 取得使用者使用的流量
    public static function getUsedFlow($user_id, $startTime = null, $endTime = null)
    {
        if (is_null($startTime)) {
            $startTime = date('Y-m-01 00:00:00');  
        }
        
        if (is_null($endTime)) {
            $endTime = date('Y-m-t 23:59:59');  
        }
        
        return UserUsedFlow::where('user_id', $user_id)
            ->whereBetween('used_time', [$startTime, $endTime])
            ->select(DB::raw('IFNULL(SUM(used_flow), 0) AS used_flow'))
            ->first();
    }
    
    // 儲存服務設定
    public static function saveService($user_id, $record)
    {
        $service = UserService::where('user_id', $user_id)
            ->first();

        if (!$service) {
            $service          = new UserService;
            $service->user_id = $user_id;
        }
        
        foreach ($record as $key => $value) {
            $service->$key = $value;  
        }      
        
        if (!$service->save()) {
            return false;
        }
        
        return true;
    }
    
    // 儲存金流設定
    public static function savePayments($user_id, $records, $usedPayment = null, $others = null)
    {
        $userConfig = self::getConfig($user_id);
        $config     = json_decode($userConfig->config, true);

        foreach ($records as $type => $value) {
            $config['payments'][$type] = $value;    
        }

        if (!is_null($usedPayment)) {
            foreach ($usedPayment as $type => $value) {
                $config['payment_used'][strtolower($type)] = $value;    
            }    
        }
        
        if (!is_null($others)) {
            foreach ($others as $type => $value) {
                $config['payment_others'][strtolower($type)] = $value;    
            }    
        }
        
        $userConfig->config = json_encode($config);
        
        return $userConfig->save();
    }
    
    // 取得使用者的物流設定
    public static function getLogistics($user_id)
    {
        $freight = UserLogistics::where('user_id', $user_id)
            ->first();
        
        return $freight;
    }
    
    // 儲存運費設定
    public static function saveFreight($user_id, $freight, $freight_free)
    {
        $userConfig = self::getConfig($user_id);
        $config     = json_decode($userConfig->config, true);

        $config['freight']      = $freight;
        $config['freight_free'] = $freight_free;
        
        $userConfig->config = json_encode($config);
        
        return $userConfig->save();
    }
    
    // 儲存物流設定
    public static function saveLogistics($user_id, $records, $usedLogistic = null)
    {
        $userConfig = self::getConfig($user_id);
        $config     = json_decode($userConfig->config, true);

        foreach ($records as $type => $value) {
            $config['logistics'][$type] = $value;    
        }

        if (!is_null($usedLogistic)) {
            foreach ($usedLogistic as $type => $value) {
                $config['logistic_used'][strtolower($type)] = $value;    
            }    
        }
        $userConfig->config = json_encode($config);
        
        return $userConfig->save();
    }
    
    // 儲存個人設定
    public static function saveProfile($user_id, $record)
    {
        $user = User::find($user_id);
        
        foreach ($record as $key => $value) {
            if ($key == 'password') {
                $value = Hash::make($value);
            }
            $user->$key = $value;  
        } 
        
        if (!$user->save()) {
            return false;
        }
        
        return true;
    }
    
    // 儲存使用者使用的流量
    public static function saveUsedFlow($user_id, $record)
    {
        $usedFlow          = new UserUsedFlow;
        $usedFlow->user_id = $user_id;
        
        foreach ($record as $key => $value) {
            $usedFlow->$key = $value;  
        }      
        
        if (!$usedFlow->save()) {
            return false;
        }

        return true;
    }
    
    // 更新熟練度
    public static function updateProficiency($userId, $proficiency)
    {
        $user = User::find($userId);
        if ($user) {
            if ($user->proficiency < $proficiency) {
                $user->proficiency = $proficiency;  
                $user->save();
            }
        }
    }
    
    // 取得指定的金流設定
    public static function getSpecifyPayment($user_id, $type)
    {
        $config = self::getConfig($user_id);
        if (!$config) {
            return null;
        }
        $config = json_decode($config->config);
        if (!isset($config->payments->$type)) {
            return null;
        }

        return $config->payments->$type;
    }
    
    // 取得使用者設定
    public static function getConfig($user_id)
    {
        $config = UserConfig::where('user_id', $user_id)->first();
        if (!$config) {
            return self::newConfig($user_id);
        }
        
        if ($user_id == 1342) {
            return $config;
        }
        
        $configArray = json_decode($config->config, true);
        if (!self::checkConfigIsNewest($configArray)) {
            $config->config = json_encode(self::convertConfigFormat($configArray));
            $config->save();
        }
        return $config;
    }
    
    // 新增使用者設定
    public static function newConfig($user_id)
    {
        $defaultConfig = [
            'version'       => UserRepository::NEWEST_CONFIG_VERSION,
            'payments'      => [
                'COD'      => 'Enable',
                'StorePay' => 'Enable',
                'InPerson' => 'Enable',
                'CCOD'     => 'Enable',
            ],
            'logistics'     => [],
            'payment_used'  => [
                'atm'         => '',
                'credit'      => '',
                'staging'     => '',
                'cod'         => '',
                'in_person'   => '',
                'ccod'        => '',
            ],
            'logistic_used' => [
                'store'       => '',
                'home'        => '',
                'home_cold'   => '',
                'self_pickup' => '',
            ],
            'freight'      => [
                'home'        => ['price' => 0, 'free' => 0], 
                'store'       => ['price' => 0, 'free' => 0], 
                'home_cold'   => ['price' => 0, 'free' => 0], 
                'self_pickup' => ['price' => 0, 'free' => 0], 
            ],
            'payment_others' => [
                'staging_limit'  => 0,
                'staging_number' => '3,6'
            ]
        ];

        $config          = new UserConfig;
        $config->user_id = $user_id;
        $config->config  = json_encode($defaultConfig);
        $config->save();
        
        return $config;
    }
    
    // 檢查現在的設定檔是不是最新版本
    protected static function checkConfigIsNewest($config)
    {
        if (!isset($config['version'])) {
            return false;
        }    
        return ($config['version'] == UserRepository::NEWEST_CONFIG_VERSION);
    }
    
    // 將舊的使用者設定轉為新的設定格式
    protected static function convertConfigFormat($config)
    {
        $format = [];

        // 檢查設定檔的版本，並轉換成新版格式
        if (!isset($config['version'])) {
            // 還沒版本正規化的設定，先改成版本 1
            $hasStorePay = false;
            if (!empty($config['logistic_used']['store'])) {
                if (isset($config['logistics'][$config['logistic_used']['store']]['use_storepay'])) {
                    $hasStorePay = ($config['logistics'][$config['logistic_used']['store']]['use_storepay'] == 'Y');    
                }   
            }
            $hasStaging = false;
            if (!empty($config['payment_used']['third'])) {
                if (isset($config['payments'][$config['payment_used']['third']]['use_staging'])) {
                    $hasStaging = ($config['payments'][$config['payment_used']['third']]['use_staging'] == 'Y');   
                }    
            }
            
            // 金流
            $format['version']              = 1;
            $format['payments']             = $config['payments'];
            $format['payments']['COD']      = 'Enable';
            $format['payments']['StorePay'] = 'Enable';
            $format['payment_used'] = [
                'atm'      => 'ATM',
                'credit'   => !empty($config['payment_used']['third']) ? $config['payment_used']['third'] : '',
                'cod'      => !empty($config['usecashondelivery']) ? 'COD' : '',
                'storepay' => $hasStorePay ? 'StorePay' : '',
                'staging'  => $hasStaging ? $config['payment_used']['third'] : ''
            ];
            $format['payment_others'] = [
                'staging_limit'  => isset($config['payment_others']['staging_limit']) ? $config['payment_others']['staging_limit'] : null,
                'staging_number' => '3,6'
            ];
                            
            // 物流
            $format['logistics']     = $config['logistics'];
            $format['logistic_used'] = $config['logistic_used'];
            $format['freight']       = [
                'home'  => [
                    'price' => empty($config['freight']) ? 0 : $config['freight'],
                    'free'  => empty($config['freight_free']) ? 0 : $config['freight_free'],
                ], 
                'store' => [
                    'price' => empty($config['freight']) ? 0 : $config['freight'],
                    'free'  => empty($config['freight_free']) ? 0 : $config['freight_free'],
                ], 
            ];
            
            return self::convertConfigFormat($format);
        } else {
            // 已經版本正規化後，依不同版本調整設定欄位
            switch ($config['version']) {
                case 1:
                    $config['version']                    = 2;
                    $config['logistic_used']['home_cold'] = '';
                    $config['freight']['home_cold']       = [
                        'price' => $config['freight']['home']['price'],
                        'free'  => $config['freight']['home']['free'],
                    ];
                    return self::convertConfigFormat($config);
                    break;
                case 2:
                    $config['version'] = 3;
                    
                    // 物流
                    $format['logistics']['SelfPickup'] = [
                        'address' => '',
                        'phone'   => '',
                    ];
                        
                    // 付款方式
                    $config['payments']['InPerson'] = 'Enable';
                    
                    // 金流預設設定
                    $config['payment_used']['in_person'] = '';
                    
                    // 物流預設設定
                    $config['logistic_used']['self_pickup'] = '';
                    
                    // 運費
                    $config['freight']['self_pickup'] = [
                        'price' => 0,
                        'free'  => 0,
                    ];

                    return self::convertConfigFormat($config);
                    break;
                case 3:
                    $config['version'] = 4;
                    
                    if (isset($config['logistics']['ECPayLogistic'])) {
                        $config['logistics']['ECPayLogistic']['use_b2c'] = 'N';
                    }
                    
                    return self::convertConfigFormat($config);
                    break;
                case 4:
                    $config['version'] = 5;
                    
                    $config['payments']['CCOD']     = 'Enable';
                    $config['payment_used']['ccod'] = '';
                    return self::convertConfigFormat($config);
                    break;
                case 5:
                    return $config;
                    break;
            }    
        }
    }
    
    // 更新使用者的使用流量
    public static function updateUsedFlow()
    {
        $query = DB::table('users_used_flow')
            ->select(DB::raw('MAX(used_time) AS lastest'))
            ->first();
        
        if (empty($query->lastest)) {
            // 找不到時一律從本月的第一天開始
            $lastUpdate = date('Y-m-t 23:59:59', strtotime('-1 month'));
        } else {
            $lastUpdate = $query->lastest;    
        }

        $userUsed  = [];

        $viewLogs = DB::table('shop_view_log')
            ->join('shop', 'shop.id', '=', 'shop_view_log.shop_id')
            ->where('shop_view_log.view_time', '>', $lastUpdate)
            ->select('shop_view_log.shop_id', DB::raw('SUM(shop_view_log.flow_size) AS flow_size'), 'shop.user_id', DB::raw('MAX(shop_view_log.view_time) AS used_time'))
            ->groupBy('shop_view_log.shop_id')
            ->get();

        foreach ($viewLogs as $log) {
            if (!isset($userUsed[$log->user_id])) {
                $userUsed[$log->user_id] = [
                    'used_flow' => 0,
                    'used_time' => null,
                    'user_id'   => $log->user_id
                ];
            }

            $userUsed[$log->user_id]['used_flow'] += $log->flow_size;
            $userUsed[$log->user_id]['used_time']  = ($log->used_time > $userUsed[$log->user_id]['used_time']) ? $log->used_time : $userUsed[$log->user_id]['used_time'];
        }

        DB::beginTransaction();
        foreach ($userUsed as $user_id => $data) {
            if (!DB::table('users_used_flow')->insert($data)) {
                DB::rollBack();
                return false;
            }    
        }
        
        DB::commit();
        return true;
    }
    
    // 取得使用者的會員等級
    public static function getUserVip($user_id)
    {    
        $userVip           = new \StdClass();
        $userVip->vip_type = 'package';
        if (!env('START_CHARGING', false)) {
            $userVip->flow           = 50 * 1024 * 1024 * 1024;    // 50GB
            $userVip->vip_name       = '公開測試';
            $userVip->fb_page_limit  = -1;
            $userVip->one_page_limit = -1;
            $userVip->expired_date   = null;
            $userVip->bonus          = [];
            $userVip->vip_level      = 999;
            $userVip->own_domain     = 0;
            $userVip->vip_type       = 'package';
            return $userVip;
        }
        
        // 免費方案
        $userVip->one_page_limit = 1;
        $userVip->fb_page_limit  = 10;
        $userVip->flow           = 1 * 1024 * 1024 * 1024;    // 1GB
        $userVip->expired_date   = null;
        $userVip->vip_name       = '免費方案';
        $userVip->bonus          = [];
        $userVip->vip_level      = 0;
        $userVip->own_domain     = 0;
        
        $package = DB::table('user_vip')
            ->where('user_id', $user_id)
            ->where('expired_date', '>=', date('Y-m-d', strtotime('-3 days')))
            ->where('vip_type', 'package')
            ->select('one_page_limit', 'fb_page_limit', 'flow', 'expired_date', 'vip_level', 'vip_name', 'own_domain', 'vip_type')
            ->orderBy('expired_date', 'DESC')
            ->first();

        $bonusList = DB::table('user_vip')
            ->where('user_id', $user_id)
            ->where('expired_date', '>=', DB::raw('CURDATE()'))
            ->where('vip_type', 'bonus')
            ->select('one_page_limit', 'fb_page_limit', 'flow', 'expired_date', 'vip_level', 'vip_name', 'own_domain', 'vip_type')
            ->orderBy('expired_date', 'DESC')
            ->get();
        
        // 處理方案
        if ($package) {
            $userVip->one_page_limit = $package->one_page_limit;
            $userVip->fb_page_limit  = $package->fb_page_limit;
            $userVip->flow           = $package->flow;
            $userVip->expired_date   = $package->expired_date;
            $userVip->vip_name       = $package->vip_name;
            $userVip->vip_level      = $package->vip_level;
            $userVip->own_domain     = $package->own_domain;
            
            if ($userVip->expired_date < date('Y-m-d')) {
                $userVip->vip_type = 'buffer_package';    
            }
        } else {
            $freePackage = DB::table('user_vip')
                ->where('user_id', $user_id)
                ->where('vip_type', 'free_package')
                ->select('one_page_limit', 'fb_page_limit', 'flow', 'expired_date', 'vip_level', 'vip_name', 'own_domain', 'vip_type')
                ->first();
            if ($freePackage && $freePackage->expired_date >= date('Y-m-d')) {
                $userVip->one_page_limit = $freePackage->one_page_limit;
                $userVip->fb_page_limit  = $freePackage->fb_page_limit;
                $userVip->flow           = $freePackage->flow;
                $userVip->expired_date   = $freePackage->expired_date;
                $userVip->vip_name       = $freePackage->vip_name;
                $userVip->vip_level      = $freePackage->vip_level;
                $userVip->own_domain     = $freePackage->own_domain;
                $userVip->vip_type       = $freePackage->vip_type;
            } elseif (!$freePackage) {
                $oldVip = DB::table('user_vip')
                    ->where('user_id', $user_id)
                    ->where('vip_type', 'package')
                    ->select('one_page_limit', 'fb_page_limit', 'flow', 'expired_date', 'vip_level', 'vip_name', 'own_domain', 'vip_type')
                    ->first();
                
                if (!$oldVip) {    
                    $userVip->one_page_limit = -1;
                    $userVip->fb_page_limit  = -1;
                    $userVip->flow           = 1 * 1024 * 1024 * 1024;    // 1GB
                    $userVip->expired_date   = date('Y-m-d', strtotime('+ 14days'));
                    $userVip->vip_name       = '豪華試用方案';
                    $userVip->bonus          = [];
                    $userVip->vip_level      = 2;
                    $userVip->own_domain     = 0;
                    $userVip->vip_type       = 'free_package';

                    $data            = (array) $userVip;
                    $data['user_id'] = $user_id;
                    unset($data['bonus']);
                    DB::table('user_vip')
                        ->insert($data);
                }
            }
        }
        
        // 處理加值
        foreach ($bonusList as $bonus) {
            $userVip->bonus[] = [
                'name'           => $bonus->vip_name,
                'one_page_limit' => $bonus->one_page_limit,
                'fb_page_limit'  => $bonus->fb_page_limit,
                'flow'           => $bonus->flow,
                'expired_date'   => $bonus->expired_date
            ];
            if ($userVip->one_page_limit != -1) {
                $userVip->one_page_limit += $bonus->one_page_limit; 
            }
            if ($userVip->fb_page_limit != -1) {
                $userVip->fb_page_limit += $bonus->fb_page_limit; 
            }
            if ($userVip->flow != -1) {
                $userVip->flow += $bonus->flow; 
            }
            if ($userVip->own_domain == 1) {
                $userVip->own_domain = 1; 
            }
        }
        
        return $userVip;
    }
}