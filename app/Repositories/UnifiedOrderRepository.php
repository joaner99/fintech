<?php
namespace App\Repositories;

use App\Models\Order\UnifiedOrder;
use App\Models\Order\UnifiedOrderProducts;
use DB;
use Auth;

class UnifiedOrderRepository
{
    // 訂單狀態
    const STATUS_FINISH_UNIFIED = 0;
    const STATUS_SET_UP_ORDER   = 10;
    const STATUS_FINISH_ORDER   = 20;
    const STATUS_DELETE         = -1;

    // 取得訂單
    public static function getOrder($id)
    {
        $order = UnifiedOrder::find($id);

        return $order;
    }
    
    // 查詢訂單
    public static function getOrdersByCommentIds($comment_ids, $shop_id = null, $statusFilter = [0, 10, 20])
    {
        $orders = UnifiedOrder::whereIn('comment_id', $comment_ids)
            ->whereIn('status', $statusFilter)
            ->whereNull('deleted_at');
        if (!is_null($shop_id)) {
            $orders->where('shop_id', $shop_id);
        }
        $orders = $orders->get();

        return $orders;
    }
    
    // 查詢訂單
    public static function getOrders($customer_id, $shop_id = null, $statusFilter = [0, 10, 20])
    {
        $orders = UnifiedOrder::where('customer_id', $customer_id)
            ->whereIn('status', $statusFilter)
            ->whereNull('deleted_at');
        if (!is_null($shop_id)) {
            $orders->where('shop_id', $shop_id);
        }
        $orders = $orders->get();

        return $orders;
    }
    
    // 查詢訂單(依文章 ID)
    public static function getOrdersByPostId($post_id, $shop_id = null, $statusFilter = [0, 10, 20])
    {
        $orders = UnifiedOrder::where('post_id', $post_id)
            ->whereIn('status', $statusFilter)
            ->whereNull('deleted_at');
        if (!is_null($shop_id)) {
            $orders->where('shop_id', $shop_id);
        }
        $orders = $orders->get();

        return $orders;
    }
    
    // 查詢訂單(依店家)
    public static function getOrdersByShop($shop_id, $statusFilter = [0, 10, 20])
    {
        $orders = UnifiedOrder::where('shop_id', $shop_id)
            ->whereIn('status', $statusFilter)
            ->whereNull('deleted_at')
            ->get();

        return $orders;
    }
    
    // 查詢訂單(依留言 ID)
    public static function getOrdersByComment($comment_id, $shop_id = null, $statusFilter = [0, 10, 20])
    {
        $order = UnifiedOrder::where('comment_id', $comment_id)
            ->whereIn('status', $statusFilter)
            ->whereNull('deleted_at');
        if (!is_null($shop_id)) {
            $order->where('shop_id', $shop_id);
        }
        $order = $order->first();

        return $order;
    }
    
    // 取得理單的商品
    public static function getProducts($id)
    {
        $products = DB::table('unified_order_products')
            ->join('product', 'product.id', '=', 'unified_order_products.prod_id')
            ->where('unified_order_products.unified_order_id', $id)
            ->whereNull('unified_order_products.deleted_at')
            ->where('unified_order_products.qty', '>', 0)
            ->select('unified_order_products.*',
                     'product.low_temperature_home_delivery',
                     'product.super_merchandise')
            ->get();

        return $products;
    }
    
    // 新訂單
    public static function newUnifiedOrder($order)
    {
        $order['status'] = self::STATUS_FINISH_UNIFIED;
        
        if (!($newOrder = UnifiedOrder::create($order))) {
            return false;
        }

        return $newOrder;
    }
    
    // 新商品
    public static function newUnifiedProduct($product)
    {
        if (!($newProduct = UnifiedOrderProducts::create($product))) {
            return false;
        }

        return $newProduct;
    }
    
    // 取得客戶訂單
    public static function getCustomersOrders($customer_id, $last, $limit = 20)
    {
        $orders = DB::table('unified_order')
            ->join('shop', 'unified_order.shop_id', '=', 'shop.id')
            ->where('unified_order.from_id', $customer_id)
            ->whereIn('unified_order.status', [10])
            ->select('unified_order.*', 'shop.title', 'shop.short_url', 'shop.shop_type')
            ->orderBy('unified_order.created_at', 'DESC')
            ->limit($limit);
        
        if (!empty($last)) {
            $orders->where('unified_order.created_at', '<', $last);
        }
        
        return $orders->get();
    }
    
    // 取得理單訂單數
    public static function getUnifiedOrderCount($shop_id)
    {
        $count = DB::table('unified_order')
            ->where('shop_id', $shop_id)
            ->whereIn('unified_order.status', [0, 10])
            ->select(DB::raw('count(id) AS cnt'))
            ->first();
        
        return $count->cnt;
    }
}