<?php
namespace App\Repositories;

use App\Models\Customer\Customer;
use DB;
use Hash;

class CustomerRepository
{
    // 取得使用者的服務設定
    public static function getCustomerByOAuthId($oauth_type, $oauth_id)
    {
        $customer = Customer::where('oauth_id', $oauth_id)
            ->where('oauth_type', $oauth_type)
            ->first();
        
        return $customer;
    }
}