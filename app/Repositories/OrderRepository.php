<?php
namespace App\Repositories;

use App\Models\Order\Order;
use App\Models\Order\OrderProducts;
use DB;
use Auth;
use Crypt;
use Mail;

class OrderRepository
{
    // 訂單狀態
    const STATUS_NEW_ORDER         = 0;
    const STATUS_PROCESSING        = 10;
    const STATUS_SHIPPED           = 20;
    const STATUS_APPLY_RETURN      = 100;
    const STATUS_RETURN_PROCESSING = 110;
    const STATUS_FINISH_RETURN     = 120;
    const STATUS_DELETE            = -1;
    // 付款狀態
    const PAY_STATUS_NOT_PAY = 0;
    const PAY_STATUS_PAID    = 10;

    // 查詢訂單
    public static function searchOrder($no, $phone)
    {
        $order = Order::where('phone', $phone)
            ->where(function($where) use ($no) {
                $where->where('no', $no);
                $where->orWhere('merchant_trade_no', $no);
            })
            ->first();
                
        if (!$order) {
            return false;
        }
        
        $order->products = OrderProducts::where('order_id', $order->id)->orderBy('id', 'ASC')->get();

        return $order;
    }
    
    // 取得user訂單
    public static function getOrdersByUserId($userId, $paginate = null)
    {
        $orders = DB::table('order')
            ->join('shop', 'shop.id', '=', 'order.shop_id')
            ->where('shop.user_id', $userId)
            ->select('order.*')
            ->orderBy('order.created_at', 'DESC')
            ->where('order.status', '!=', '-1')
            ->where('shop.user_id',Auth::user()->id);

        if (is_null($paginate)) {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate($paginate);
        }
        
        return $orders;
    }

    // 取得商店訂單
    public static function getOrdersByShopId($shopId, $paginate = null, $statusFilter = null)
    {
        $orders = Order::where('shop_id', $shopId)->orderBy('created_at', 'DESC');
        if (!is_null($statusFilter)) {
            if (is_array($statusFilter)) {
                $orders->whereNotIn('status', $statusFilter);   
            } else {
                $orders->where('status', '!=', $statusFilter); 
            }  
        }
        if (is_null($paginate)) {
            $orders = $orders->get();
        } else {
            $orders = $orders->paginate($paginate);
        }
        
        return $orders;
    }
    
    // 取得商店訂單(從訂單ID取得)
    public static function getOrder($orderId)
    {
        $order = Order::find($orderId);
        
        return $order;
    }
    
    // 取得客戶訂單
    public static function getCustomersOrders($customer_id, $status, $last, $limit = 20)
    {
        $orders = DB::table('order')
            ->join('shop', 'order.shop_id', '=', 'shop.id')
            ->where('order.customer_id', $customer_id)
            ->select('order.*', 'shop.title', 'shop.short_url', 'shop.shop_type')
            ->orderBy('order.created_at', 'DESC')
            ->limit($limit);
        
        switch($status) {
            case 'not_pay':
                $orders->where('order.status', '!=', OrderRepository::STATUS_DELETE);
                $orders->where('order.pay_status', OrderRepository::PAY_STATUS_NOT_PAY);
                break;
            case 'processing':
                $orders->whereIn('order.status', [OrderRepository::STATUS_PROCESSING,OrderRepository::STATUS_NEW_ORDER]);
                break;
            case 'paid':
                $orders->where('order.status', '!=', OrderRepository::STATUS_DELETE);
                $orders->where('order.pay_status', OrderRepository::PAY_STATUS_PAID);
                break;
            case 'shipped':
                $orders->where('order.status', OrderRepository::STATUS_SHIPPED);
                break;
            case 'delete':
                $orders->where('order.status', OrderRepository::STATUS_DELETE);
                break;
            case 'apply_return':
                $orders->whereIn('order.status', [OrderRepository::STATUS_APPLY_RETURN,OrderRepository::STATUS_RETURN_PROCESSING,OrderRepository::STATUS_FINISH_RETURN]);
                break;
        }
        
        if (!empty($last)) {
            $orders->where('order.created_at', '<', $last);
        }
        
        return $orders->get();
    }
    
    // 取得商店訂單產品
    public static function getOrderProducts($orderId, $realProduct = false)
    {
        $order = OrderProducts::where('order_id', $orderId);
        if (!$realProduct) {
            $order->where('parent_prod', '=', 0);    
        } else {
            $order->where(function($where) {
                $where->where('prod_id', '!=', 0); 
                $where->orWhere('marketing_type', '!=', 3);    // 非自由搭配
            });
        }
        return $order->orderBy('id')->get();
    }
    
    // 新訂單
    public static function newOrder($order, $products)
    {
        $order['no']         = self::getNewOrderNo($order['shop_id']);
        $order['status']     = self::STATUS_NEW_ORDER;
        $order['pay_status'] = self::PAY_STATUS_NOT_PAY;
        
        if (!($newOrder = Order::create($order))) {
            return false;
        }
        
        foreach ($products as $product) {
            $product['order_id'] = $newOrder->id;
            $children            = isset($product['children']) ? $product['children'] : [];
            unset($product['children']);
            if (!($newProduct = OrderProducts::create($product))) {
                return false;
            }
            foreach ($children as $child) {
                $child['order_id']    = $newOrder->id;
                $child['parent_prod'] = $newProduct->id;
                if (!OrderProducts::create($child)) {
                    return false;
                }
            }
        }
        
        return $newOrder;
    }
    
    // 取得新的訂單號碼
    public static function getNewOrderNo($shopId)
    {
        $code       = [
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y', 'Z'
        ];

        $lastOrder  = DB::table('order')
            ->where(DB::raw('DATE(created_at)'), DB::raw('CURDATE()'))
            ->where('shop_id', $shopId)
            ->select(DB::raw('MAX(no) AS newestNo'))
            ->first();
        
        $prefixNo   = '';
        $prefixBase = $shopId;
        while(strlen($prefixNo) < 6) {
            if (strlen($prefixNo) >= 3) {
                $prefixNo   = $code[$prefixBase % 36].$prefixNo; 
                $prefixBase = floor($prefixBase / 36);
            } else {
                $prefixNo   = $code[$prefixBase % 10].$prefixNo; 
                $prefixBase = floor($prefixBase / 10);
            }
        }
        
        if (is_null($lastOrder->newestNo)) {
            $orderNo = $prefixNo.date('ymd').sprintf('%05d', 1);
        } else  {
            $orderNo = $prefixNo.date('ymd').sprintf('%05d', substr($lastOrder->newestNo, -5) + 1);
        }
        return $orderNo;
    }
    
    // 取得新訂單的數量
    public static function getNewOrderNumber($shopId)
    {
        $num = DB::table('order')
            ->where('shop_id', $shopId)
            ->where('status', OrderRepository::STATUS_NEW_ORDER)
            ->select(DB::raw('COUNT(*) AS num'))
            ->first();
        
        return is_null($num->num) ? 0 : $num->num;
    }
    
    // 取得訂單
    public static function getOrderByMerchantTradeNo($merchantTradeNo)
    {
        $order = Order::where('merchant_trade_no', '=', $merchantTradeNo)->first();
        return $order;
    }
    
    // 取得訂單
    public static function getOrderByNo($orderNo)
    {
        $order = Order::where('no', '=', $orderNo)->first();
        return $order;
    }
    
    // 更新訂單狀態
    public static function updateOrderStatus($orderId, $status)
    {
        if (!self::checkDefineValueIsExist('STATUS_', $status)) {
            return false;
        }
        $order = Order::find($orderId);

        if (!$order) {
            return false;
        }

        $order->status = $status;
        return $order->save();
    }
    
    // 更新訂單紅利狀態
    public static function updateOrderBonus($orderId)
    {
        $order = Order::find($orderId);

        if (!$order) {
            return false;
        }

        $order->bonus_type = 1;
        return $order->save();
    }
    // 更新訂單付款狀態
    public static function updateOrderPayStatus($orderId, $status)
    {
        if (!self::checkDefineValueIsExist('PAY_STATUS_', $status)) {
            return false;
        }
        $order = Order::find($orderId);

        if (!$order) {
            return false;
        }

        $order->pay_status = $status;
        return $order->save();
    }
    
    protected static function checkDefineValueIsExist($prefix = '', $value = '')
    {
        $refl = new \ReflectionClass(__CLASS__);
        
        $pass = false;
        foreach ($refl->getConstants() as $key => $constValue) {
            if (!empty($prefix)) {
                if (substr($key, 0, strlen($prefix)) != $prefix) {
                    continue;
                }
            }
            
            if ($value == $constValue) {
                $pass = true;
                break;
            }
        }
        
        return $pass;
    }

    public static function updateOrderStatusForEcapy()
    {
        set_time_limit(1500);
        $userConfig    = json_decode(UserRepository::getConfig(Auth::user()->id)->config);
        $logisticClass = 'App\\Support\\Logistics\\'.$userConfig->logistic_used->store;
        $third         = $userConfig->logistic_used->store;
        $logisticClass::setup((array) $userConfig->logistics->$third);
        $orderDatas=
        db::table('shop')->
        join('order','order.shop_id','=','shop.id')->
        where('shop.user_id',Auth::user()->id)->
        where('order.delivery_type','store')->
        where('ship_no','<>','')->
        where('order.status','<>',20)->
        where('order.status','<>',999)->
        where('order.status',0)->
        whereNotNull('ship_no')->
        get();
        foreach ($orderDatas as $key => $value) {
            $status=$logisticClass::queryOrder($value->ship_no);
            $order = Order::find($value->id);
            if (!$order || !$status) {
                return false;
            }

            switch ($status['status']) {
                case 2067:
                case 3022:
                $order->pay_status=OrderRepository::PAY_STATUS_PAID;
                case 2030:
                case 3024:
                case 3032:
                case 3018:
                case 2073:
                case 2063:
                $order->status=OrderRepository::STATUS_SHIPPED;
                $order->save();
                break;
            }
        }
        return true;
    }
    
    // 寄送訂單 E-Mail
    public static function sendOrderNotify($start = null, $end = null)
    {
        if (is_null($start)) {
            $start = date('Y-m-d 00:00:00', strtotime('-1 days'));
        }
        
        if (is_null($end)) {
            $end = date('Y-m-d H:i:s');
        }
        
        set_time_limit(0);
        
        //$orderIds = [];
        $owners   = [];
        $shops    = [];
        $services = [];
        
        $orders = DB::table('order')
            ->where('email_notify', 0)
            ->where('process_finish', 1)
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end)
            ->get();

        $ids = [];
        foreach ($orders as $order) {
            $ids[] = $order->id;
        }

        DB::table('order')
            ->whereIn('id', $ids)
            ->update([
                'email_notify' => 1
            ]);
        
        $count = 0;
        foreach ($orders as $order) {
            if (!isset($shops[$order->shop_id])) {
                $shops[$order->shop_id] = DB::table('shop')
                    ->where('id', $order->shop_id)
                    ->first();
            }

            if (!isset($owners[$shops[$order->shop_id]->user_id])) {
                $owners[$shops[$order->shop_id]->user_id]   = ShopRepository::getShopOwner($order->shop_id);
                $services[$shops[$order->shop_id]->user_id] = DB::table('users_service')
                    ->where('user_id', $shops[$order->shop_id]->user_id)
                    ->first();
            }

            $shop             = $shops[$order->shop_id];
            $mail['order']    = $order;
            $mail['products'] = DB::table('order_products')
                ->where('order_id', $order->id)
                ->get();

            $mail['shop_name']    = $shop->title;
            $mail['custom_forms'] = DB::table('order_form')
                ->where('order_id', $order->id)
                ->whereNull('deleted_at')
                ->get();
 
            if (!empty($mail['order']->email)) {
                $service = $services[$shops[$order->shop_id]->user_id];
                if (!empty($service->company_logo)) {
                    $mail['logo'] = public_path().'/gallery/'.getGalleryId($shop->user_id).'/'.$service->company_logo;    
                } else {
                    $mail['logo'] = public_path().'/assets/img/LetsGO.png';
                }
                $mail['crypt'] = Crypt::encrypt($mail['order']->no.'|'.$mail['order']->phone);
                $mail['service']=$service;
                Mail::send('mail.new_order', ['mail' => $mail], function ($message) use ($mail, $shop) {
                    $message->from('admin@sunstar-dt.com', '全國軍公教福利中心');
                    $message->to($mail['order']->email)->subject('全國軍公教福利中心訂單通知-您有一筆來自『'.$shop->title.'』的新訂單');
                });
            }

            $shopOwner = $owners[$shops[$order->shop_id]->user_id];
            if (filter_var($shopOwner->email, FILTER_VALIDATE_EMAIL)) {
                Mail::send('mail.new_order_to_shop', ['mail' => $mail], function ($message) use ($mail, $shop, $shopOwner) {
                    $message->from('admin@sunstar-dt.com', '全國軍公教福利中心');
                    $message->to($shopOwner->email)->subject('全國軍公教福利中心訂單通知-您有一筆來自『'.$shop->title.'』的新訂單');
                });
            }
            //$orderIds[] = $order->id;
            
            DB::table('order')
                ->where('id', $order->id)
                ->update([
                    'email_notify' => 2
                ]);
            
            $count++;
            if ($count >= 7) {
                sleep(1);
                $count = 0;
            }
        }
    }
}