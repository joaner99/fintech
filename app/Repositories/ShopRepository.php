<?php
namespace App\Repositories;

use App\User;
use App\Models\Shop\Shop;
use App\Models\Shop\ShopViewLog;
use App\Models\Shop\ShopFlowSize;
use App\Models\Shop\UserGallery;
use App\Models\Shop\ShopDetail;
use App\Models\Shop\ShopNouns;
use App\Models\Product\Product;
use App\Models\Product\ProductSize;
use App\Models\Marketing\Marketing;
use App\Support\SSEncrypt;
use DB;
use Session;

class ShopRepository
{
    // 商店的發佈狀態
    const TYPE_NEW_SHOP             = 0;
    const TYPE_COMPLETED_SHOP_INFO  = 10;
    const TYPE_COMPLETED_PRODUCTS   = 20;
    const TYPE_COMPLETED_PAGE       = 30;
    const TYPE_COMPLETED_BASIC_INFO = 35;
    const TYPE_PUBLISHED            = 100;
    
    // 商店區塊的類型
    const SHOP_DETAIL_TYPE_IMAGE = 0;
    
    public static function getShop($shopId)
    {
        return Shop::find($shopId);
    }
    
    // 複製商店
    public static function cloneShop($shopId)
    {
        $shop = self::getShop($shopId);
        
        switch ($shop->shop_type) {
            case 1:
            case 2:
                $cloneShop            = $shop->replicate();
                $cloneShop->title     = $cloneShop->title.'(複製)';
                $cloneShop->short_url = '';
                $cloneShop->save();
                
                // 複製商品的圖片
                copyS3Folder('shop/'.$shop->id, 'shop/'.$cloneShop->id);
                
                $products = Self::getShopProducts($shopId);
                foreach ($products as $product) {
                    $cloneProduct          = $product->replicate();
                    $cloneProduct->shop_id = $cloneShop->id;
                    $cloneProduct->save();
                    
                    $productSizes = ProductSize::where('product_id', $product->id)
                        ->get();
                    foreach ($productSizes as $size) {
                        $cloneSize             = $size->replicate();
                        $cloneSize->product_id = $cloneProduct->id;
                        $cloneSize->save();
                    }
                }
                
                $details = ShopDetail::where('shop_id', $shopId)->get();
                foreach ($details as $detail) {
                    $cloneDetail          = $detail->replicate();
                    $cloneDetail->shop_id = $cloneShop->id;
                    $cloneDetail->save();
                }
                
                $marketinActions = Marketing::where('shop_id', $shopId)->get();
                foreach ($marketinActions as $action) {
                    $cloneAction          = $action->replicate();
                    $cloneAction->shop_id = $cloneShop->id;
                    $cloneAction->save();
                }
                break;
        }
    }
    
    public static function hasPublishedShop($user_id)
    {
        $shop = Shop::where('user_id', $user_id)
            ->where('type', ShopRepository::TYPE_PUBLISHED)
            ->first();
        if ($shop) {
            return true;
        }
        
        return false;
    }
    
    public static function getShopFirstProduct($shopId)
    {
        return Product::where('shop_id', $shopId)
            ->orderBy('sort', 'ASC')
            ->first();
    }
    
    // 取得商店的所有人
    public static function getShopOwner($shopId)
    {   
        return db::table('shop')->where('shop.id', $shopId)
            ->join('users', 'users.id', '=', 'shop.user_id')
            ->select('users.*')
            ->first();    
    }
    
    // 取得商店販售的商品和促銷啟用狀況(依促銷分類)
    public static function getShopPromotionList($shopId, $shop = null, $shopCount = 1)
    {
        $list       = [
            1 => [],
            2 => [],
            3 => [],
            4 => false,
            5 => false,
        ];
        
        $enable     = [];
        $shop       = is_null($shop) ? Self::getShop($shopId) : $shop;
        $products   = Self::getShopProducts($shopId);
        $promotions = DB::table('marketin_action')
            ->where('shop_id', $shopId)
            ->where('start', 1)
            ->get();
        
        foreach ($promotions as $promotion) {
            $enable[$promotion->marketing_type] = true;   
        } 
        
        // 處理商品
        foreach ($products as $product) {
            
            // 依商店的類別，放入規格和設定最大值
            if ($shop->shop_type == 2) {
                if($shopCount<=50){
                    $product->sizes = DB::table('product_size')
                        ->whereNull('deleted_at')
                        ->where('product_id', $product->id)
                        ->where('name', '!=', '')
                        ->get();

                    $product->max_amount = ($product->max_amount === 0) ? null : $product->max_amount;
                    $product->has_stock  = true;
                }
            } else {
                $product->sizes     = [];
                $product->has_stock = $product->stock > 0;
                if ($product->max_amount > $product->stock || empty($product->max_amount)) {
                    $product->max_amount = $product->stock;
                }
            }
            
            // 優惠商品
            if (!isset($enable[1])) {
                $product->bargain_price = null;
            }
            
            // 加價購和一般商品
            if (isset($enable[2])) {
                if ($product->increase_the_purchase_price_type == 1) {
                    if ($product->max_amount > $product->increase_the_purchase_price_amount || empty($product->max_amount)) {
                        $product->max_amount = ($product->increase_the_purchase_price_amount === 0) ? null : $product->increase_the_purchase_price_amount;    
                    }
                    $list[2][] = $product;
                } else {
                    $list[1][] = $product;
                }
            } else {
                $list[1][] = $product;
            }
            
            // 自由搭配
            if (isset($enable[3])) {
                if ($product->free_match == 1) {
                    $list[3][] = $product; 
                }
            }
        }
        
        // 滿額折扣
        if (isset($enable[4])) {
            $list[4] = true;    
        }
        
        // 任選件數
        if (isset($enable[5])) {
            $list[5] = true;    
        }
        return $list;
    }
    
    // 取得商店販售的商品和促銷啟用狀況(依促銷分類)
    public static function getWebsitePromotionList($shopId)
    {
        $linkShops = DB::table('website_shops')
            ->whereNull('deleted_at')
            ->where('shop_id', $shopId)
            ->get();
        
        $list = [
            1 => [],
            2 => [],
            3 => [],
            4 => false,
            5 => false,
        ];
        
        foreach ($linkShops as $shop) {
            $tmp     = self::getShopPromotionList($shop->link_shop_id); 
            $list[1] = array_merge($list[1], $tmp[1]);
            $list[2] = array_merge($list[2], $tmp[2]);
            $list[3] = array_merge($list[3], $tmp[3]);
        }

        return $list;
    }
    
    // 取得商店販售的商品和促銷啟用狀況(依促銷分類)
    public static function getShopMarketinActions($shopId)
    {
        $enable     = [];
        $promotions = DB::table('marketin_action')
            ->where('shop_id', $shopId)
            ->where('start', 1)
            ->get();
        
        foreach ($promotions as $promotion) {
            $enable[] = $promotion->marketing_type;   
        }
        
        return $enable;
    }
    
    // 取得使用者已上架的商店數
    public static function getVaildShopCountByUserId($userId, $shop_types = [1, 2, 3])
    {
        $count = Shop::where('user_id', $userId)
            ->whereIn('shop_type', $shop_types)
            ->select(DB::raw('count(id) AS cnt'))
            ->first();
        
        return $count->cnt;
    }

    
    // 更新商店的流量
    public static function refreshShopFlowSize($shopId)
    {
        $imgs = DB::table('shop')
            ->join('shop_detail', function($join) {
                $join->on('shop_detail.shop_id', '=', 'shop.id');
            })
            ->join('users_gallery', function($join) {
                $join->on('users_gallery.filename', '=', 'shop_detail.filename'); 
                $join->on('users_gallery.user_id', '=', 'shop.user_id');
            })
            ->where('shop.id', $shopId)
            ->where('shop_detail.type', ShopRepository::SHOP_DETAIL_TYPE_IMAGE)
            ->whereNull('shop_detail.deleted_at')
            ->whereNull('users_gallery.deleted_at')
            ->select('users_gallery.size')
            ->get();
        
        $totalSize = 0;
        
        foreach ($imgs as $img) {
            $totalSize += $img->size;    
        }
        
        ShopFlowSize::create([
            'shop_id'   => $shopId,
            'flow_size' => $totalSize
        ]);
    }
    
    // 取得商店最新的流量
    public static function getShopFlowSize($shopId)
    {
        $flow = ShopFlowSize::where('shop_id', $shopId)
            ->orderBy('created_at', 'DESC')
            ->first();
        
        if (!$flow) {
            self::refreshShopFlowSize($shopId);
            $flow = ShopFlowSize::where('shop_id', $shopId)
                ->orderBy('created_at', 'DESC')
                ->first();
        }
        
        return $flow->flow_size;
    }
    
    // 紀錄新的瀏覽紀錄
    public static function newViewLog($shopId, $agent, $tag = null)
    {
        if (!self::getShop($shopId)) {
            return false;    
        }
        return ShopViewLog::create([
            'view_time'  => date('Y-m-d H:i:s'),
            'shop_id'    => $shopId,
            'session_id' => Session::getId(),
            'agent'      => $agent,
            'flow_size'  => self::getShopFlowSize($shopId),
            'tag'        => $tag,
        ]);
    }
    
    // 取得所有商店的流量
    public static function getAllShopFlowSizeByUserId($userId)
    {
        $shops = Shop::where('user_id', $userId)
            ->get();
        
        $size = 0;
        foreach ($shops as $shop) {
            $flow = ShopFlowSize::where('shop_id', $shop->id)
            ->orderBy('created_at', 'DESC')
            ->first();
            
            if ($flow) {
                $size += $flow->flow_size;
            }
        }
        
        return $size;
    }
    
    // 取得所有商店的產品
    public static function getShopProducts($shopId)
    {
        return Product::where('shop_id', $shopId)->orderBy('sort')->get();
    }
    
    // 取得商店完成進度狀況
    public static function getShopCompleteness($shopId)
    {
        $type = ShopRepository::TYPE_NEW_SHOP;
        $shop = self::getShop($shopId);
        if (!$shop) {
            return $type;    
        }
        if (empty($shop->title)) {
            return $type;       
        }
        $type = ShopRepository::TYPE_COMPLETED_SHOP_INFO; 
        
        $products = self::getShopProducts($shopId);
        if (count($products) == 0) {
            return $type;    
        }
        $type = ShopRepository::TYPE_COMPLETED_PRODUCTS;
        
        $details = ShopDetail::where('shop_id', $shopId)->get();
        if (count($details) == 0) {
            return $type;    
        }
        $type = ShopRepository::TYPE_COMPLETED_PAGE;
        
        return $type;
    }
    
    // 取得商店狀態的
    public static function getTypeName($type, $shopType = null)
    {
        // 判斷商店類型是不是 FB+1
        switch ($shopType) {
            case 1:
            case 2:
                switch ($type) {
                    case ShopRepository::TYPE_NEW_SHOP:
                        return '進度:新商店-未發佈';
                    case ShopRepository::TYPE_COMPLETED_SHOP_INFO:
                        return '進度:商店資訊-未發佈';
                    case ShopRepository::TYPE_COMPLETED_PRODUCTS:
                        return '進度:商品建立-未發佈';
                    case ShopRepository::TYPE_COMPLETED_PAGE:
                        return '進度:頁面排版-未發佈';
                    case ShopRepository::TYPE_PUBLISHED:
                        return '已發佈';
                } 
                break;
            case 3:
                switch ($type) {
                    case ShopRepository::TYPE_PUBLISHED:
                        return '開團中';
                    default:
                        return '尚未開團';
                }
                break;
            case 4:
                switch ($type) {
                    case ShopRepository::TYPE_NEW_SHOP:
                        return '進度:新小官網-未發佈';
                    case ShopRepository::TYPE_COMPLETED_SHOP_INFO:
                        return '進度:小官網資訊-未發佈';
                    case ShopRepository::TYPE_COMPLETED_PRODUCTS:
                        return '進度:小官網商品-未發佈';
                    case ShopRepository::TYPE_COMPLETED_PAGE:
                        return '進度:小官網輪撥圖-未發佈';
                    case ShopRepository::TYPE_PUBLISHED:
                        return '已發佈';
                }
                break;
        }
    }
    
    // 取得店家的網域
    public static function convertShopIdByDomain($shopId)
    {
        if (is_null($shopId)) {
            $shop = Shop::where('domain_name', trim($_SERVER['SERVER_NAME']))->first();
            if ($shop) {
                return $shop->id;
            }
        }
        
        return $shopId;
    }
    
    // 依網域取得店家
    public static function getShopByDomain()
    {
        return Shop::where('domain_name', trim($_SERVER['SERVER_NAME']))->first();
    }
    
    public static function change_status($id)
    {
        $Shop=Shop::where('id', $id)->first();
        if($Shop->status==0){
           $Shop->status=1;
        }else{
           $Shop->status=0;
        }  
        $Shop->save();
    }
    
    public static function makeSitemap()
    {
        $xml   = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $shops = Shop::whereIn('shop_type', [1, 2])
            ->where('status', 0)
            ->where('type', 100)
            ->whereNull('deleted_at')
            ->select('id')
            ->get();
        foreach ($shops as $shop) {
          $xml .= '<url><loc>https://www.freeon.shop/shopping/'.SSEncrypt::encode($shop->id).'</loc></url>';
        }
        $xml .= '</urlset>';
        
        file_put_contents(public_path().'/sitemap.xml', $xml);
    }
    
    // 鎖定已滿訂單金額的店家
    public static function lockShops()
    {
        $userVipLevel = [];
        $users        = Shop::whereNull('deleted_at')
            ->where('locked', 0)
            ->groupBy('user_id')
            ->select('user_id')
            ->get();
        
        foreach ($users as $user) {
            if (isset($userVipLevel[$user->user_id])) {
                continue;  
            }    

            $userVipLevel[$user->user_id] = \App\Repositories\UserRepository::getUserVip($user->user_id); 
            
            // 非免費會員就不往下執行
            if ($userVipLevel[$user->user_id]->vip_level != 0) {
                continue;    
            }

            $shops = DB::table('shop')
                ->where('user_id', $user->user_id)
                ->select('id')
                ->get();

            if (count($shops) == 0) {
                continue;
            }
            
            $ids = [];
            foreach ($shops as $shop) {
                $ids[] = $shop->id;    
            }

            $total = DB::table('order')
                ->where('status', '!=', -1)
                ->whereIn('shop_id', $ids)
                ->select(DB::raw('IFNULL(SUM(total_price), 0) AS total_price'))
                ->first();

            if ($total->total_price < 100000) {
                continue;
            }

            DB::table('shop')
                ->whereIn('id', $ids)
                ->update([
                    'locked' => 1 
                ]);
        }
    }
    
    // 關閉過期的店家
    public static function closeExpiredShops()
    {
        $usersVip = [];
        $shops    = Shop::whereNull('deleted_at')
            ->where('status', 0)
            ->orderBy('updated_at', 'desc')
            ->orderBy('user_id', 'asc')
            ->get();
        
        foreach ($shops as $shop) {
            if (!isset($usersVip[$shop->user_id])) {
                $usersVip[$shop->user_id] = \App\Repositories\UserRepository::getUserVip($shop->user_id);   
            }    
            
            // 處理一頁商店
            if (($shop->shop_type == 1 || $shop->shop_type == 2) && ($usersVip[$shop->user_id]->one_page_limit == -1 || $usersVip[$shop->user_id]->one_page_limit > 0)) {
                if ($usersVip[$shop->user_id]->one_page_limit != -1) {
                    $usersVip[$shop->user_id]->one_page_limit--;
                }  
                continue;
            }
            
            // 處理 FB+1 商店
            if ($shop->shop_type == 3 && ($usersVip[$shop->user_id]->fb_page_limit == -1 || $usersVip[$shop->user_id]->fb_page_limit > 0)) {
                if ($usersVip[$shop->user_id]->fb_page_limit != -1) {
                    $usersVip[$shop->user_id]->fb_page_limit--;
                }    
                continue;
            }
            
            // 處理小官網
            if ($shop->shop_type == 4 && $usersVip[$shop->user_id]->vip_level == 2) {  
                continue;
            }
            
            $shop->status = 1;
            $shop->save();
        }
    }
    
    // 取得商店的名詞設定
    public static function getShopNounsByShopId($shopId)
    {
        return ShopNouns::where('shop_id', $shopId)->first();
    }
}