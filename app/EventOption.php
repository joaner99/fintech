<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventOption extends Model
{
    public function ZoneOption()
    {
        return $this->belongsTo(ZoneOption::class);
    }
}
