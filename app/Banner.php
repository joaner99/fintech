<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    public function ZoneOption()
    {
        return $this->belongsTo(ZoneOption::class);
    }
}
