<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerClass extends Model
{
	protected $connection = 'mysql_admin_for_letsgo';
	protected $table = 'customer_class';
}
