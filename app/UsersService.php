<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersService extends Authenticatable
{
    protected $connection = 'mysql_admin_for_letsgo';
    protected $table = 'users_service';
    public function User()
    {
        return $this->belongsTo(User::class);
    }
}
