<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\IndexBanner;
use App\ProdClass;
use App\Prod;
use App\Order;
use App\OrderDetail;
use App\User;
use App\Contactu;
use App\Flow;
use Cart;

use Illuminate\Support\Facades\Input;
use Mail;
use DB;
use Session;
use App;
use Auth;
use Redirect;
class ReceptionController extends Controller
{	
    public function lang(){
        $lang=Session::get('locale','cn');
        if(!$lang){
            $lang='cn';
        }
    	Session::put('locale',Input::get('lang',$lang));
        app()->setlocale(Session::get('locale'));
    	if(Session::get('locale')=='cn'){
    		Session::put('lang','cn');
    	}elseif(Session::get('locale')=='en'){
    		Session::put('lang','en');

    	}elseif(Session::get('locale')=='jp'){
    		Session::put('lang','jp');
    	}    
    	return Session::get('lang');
    }
    
    public function index()
    {
    	$lang=$this->lang();
        $IndexBanner=IndexBanner::get();
        $ProdClasses=ProdClass::select('id','name_'.$lang.' as name')->get();
        $Prod=db::table('prods')
	        ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
	        ->join('prod_classes','prods.class_id','=','prod_classes.id')
	        ->get();
        return view('reception.index',['IndexBanner'=>$IndexBanner,'ProdClasses'=>$ProdClasses,'Prod'=>$Prod]);
    }
    public function record()
    {
        $lang=$this->lang();
        if(Session::get('login_key')==1){
            Session::put('login_key',2);
            return redirect('order_data');
        }else{
            $Order=Order::where('user_id',Auth::user()->id)->get();
            return view('reception.m_record',['Order'=>$Order]); 
        }
        
    }
   public function m_record_detail()
    {
        $lang=$this->lang();
        if(Input::get('id')!=''){
            $Order=Order::where('id',Input::get('id'))->get();
            $OrderDetail=OrderDetail::where('order_id',Input::get('id'))->get();
            return view('reception.m_record_detail',['Order'=>$Order,'OrderDetail'=>$OrderDetail]);
        }
    }
    
    
    function get_folder_url() {
        $url = $_SERVER['REQUEST_URI']; //returns the current URL
        $parts = explode('/',$url);
        $dir = $_SERVER['SERVER_NAME'];
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $dir .= $parts[$i] . "/";
        }
        return 'http://'.$dir;
    }
    public function m_info()
    {
        $lang=$this->lang();
        return view('reception.m_info');  
    }
    public function m_info_update()
    {
        $lang=$this->lang();
        User::where('id',Auth::user()->id)->update(
            [
            'name'=>Input::get('name'),
            'tel'=>Input::get('tel'),
            'address'=>Input::get('address'),
            'email'=>Input::get('email'),
            ]);
        $User=User::find(Auth::user()->id);
        return view('reception.m_info_update',['User'=>$User]);
    }
    public function m_change()
    {
        $lang=$this->lang();
        return view('reception.m_change');
    }
    public function m_change_update()
    {
        $lang=$this->lang();
        if(!Auth::attempt(['email' => Auth::user()->email, 'password' => Input::get('old_password')])){
            return  Redirect::back()->withErrors([ '舊密碼錯誤']);
        }

        if(Input::get('password')<>Input::get('password2')){
            return  Redirect::back()->withErrors(['密碼不相同']);
        }else{


            User::where('id',Auth::user()->id)->update(
            [
            'password'=>bcrypt(Input::get('password')),
            ]);
        }
        return Redirect::back()->withErrors([ '修改成功！！']);

    }
    public function about()
    {
        $lang=$this->lang();
        $companies=db::table('companies')
            ->select('title1_'.$lang.' as title1',
                     'title2_'.$lang.' as title2',
                     'body1_'.$lang.' as body1',
                     'body2_'.$lang.' as body2',
                     'file1 as file1' )->first();
        return view('reception.about',['companies'=>$companies]);
    }
    public function qa()
    {
        $lang=$this->lang();
        $problems=db::table('problems')
            ->select('title1_'.$lang.' as title',
                     'body1_'.$lang.' as body'
                    )->get();
        return view('reception.qa',['problems'=>$problems]);
    }
    public function contanct()
    {
        $lang=$this->lang();
        $contactu_datas=db::table('contactu_datas')
            ->select('name_'.$lang.' as name',
                     'email',
                     'tel')->first();
        return view('reception.contanct',['contactu_datas'=>$contactu_datas]);
    }
    public function contanct_post()
    {
        $lang=$this->lang();

        $Contactu=new Contactu;
        $Contactu->name=Input::get('name');
        $Contactu->email=Input::get('email');
        $Contactu->type=Input::get('type');
        $Contactu->body=Input::get('body');
        $Contactu->theme=Input::get('theme');
        $Contactu->save();
        return Redirect::back()->withErrors([ '將有專人盡快聯繫您！']);
    }
    public function flow()
    {
        $lang=$this->lang();
        $Flow=Flow::OrderBy('sort','asc')->get();
        return view('reception.flow',['flow'=>$Flow]);
    }

    public function shop($id)
    {
        $lang=$this->lang();
        $ProdClasses=ProdClass::select('id','name_'.$lang.' as name')->get();
        $Prod=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id');
        if($id!=0){
            $Prod =$Prod->where('prods.class_id',$id);  
        }   
        $Prod =$Prod->get(); 
        return view('reception.shop',['ProdClasses'=>$ProdClasses,'Prod'=>$Prod,'id'=>$id]);
    }
    public function shop_single($id)
    {
        $lang=$this->lang();
        $Prod=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id')
            ->where('prods.id',$id)
            ->first();
        $Prod_demo=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id')
            ->get(); 
        return view('reception.shop_single',['Prod'=>$Prod,'Prod_demo'=>$Prod_demo]);

    }

    public function design($id)
    {
        $lang=$this->lang();
        $Prod=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id')
            ->where('prod_classes.id',$id)
            ->get();
        return view('reception.design',['Prod'=>$Prod,'id'=>$id]);
    }
    public function select_design()
    {
        $lang=$this->lang();
        $Prod=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id')
            ->where('prod_classes.id','1')
            ->get();
        return view('reception.select_design',['Prod'=>$Prod,'id'=>1]);
    }

    public function select_design2()
    {
        $lang=$this->lang();
        $Prod=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id')
            ->where('prod_classes.id','2')
            ->get();
        return view('reception.select_design',['Prod'=>$Prod,'id'=>2]);
    }
    public function select_design3()
    {
        $lang=$this->lang();
        $Prod=db::table('prods')
            ->select('prods.*','prods.name_'.$lang.' as name','prods.ext_'.$lang.' as ext','prods.body_'.$lang.' as body','prod_classes.name_'.$lang.' as class_name')
            ->join('prod_classes','prods.class_id','=','prod_classes.id')
            ->where('prod_classes.id','2')
            ->get();
        return view('reception.select_design',['Prod'=>$Prod,'id'=>2]);
    }


    public function design2()
    {
        $lang=$this->lang();
        switch (Session::get('size')) {
            case 1:
                $size='';
                break;
            case 2:
                $size='-2';
                break;
            case 3:
                $size='-3';
                break;
            case 4:
                $size='4';
                break;
        }
        return view('reception.design2',['size'=>$size]);
    }


    public function review()
    { 
        return view('reception.review',['key'=>Session::get('key'),'size'=>Session::get('size')]);
    }


    public function list_cars()
    {
        $data=Cart::content();
        return view('reception.cars',['data'=>$data]);
    }
    public function add_cars()
    {
        Session::forget('size');
        $number=rand(10000, 99999);
        Session::put('key',$number);
        $price=explode(',', Input::get('amount'));
        Cart::add(Session::get('key'), Input::get('name'), $price[0], $price[1], ['ext' => Input::get('ext'), 'pd_id'=> Input::get('id') ]);
        Session::put('size',Input::get('size'));
        return redirect('design2');
    }
    public function remove($rowId)
    {
        Cart::remove($rowId);
        return redirect('list_cars');
    }
    public function edit_cars($rowId,$count)
    {
        Cart::update($rowId, $count);
        return redirect('list_cars');
    }
    public function order_data()
    {
      if(Auth::check()){
        $data=Cart::content();
        return view('reception.order_data',['data'=>$data]);
      }else{
        Session::put('login_key',1);
        return redirect('login');
      }
    }

    public function cars_pay()
    {
        $no=rand(10000000,99999999);
        $Order=Order::create(['user_id'=>Auth::user()->id,'no'=>$no,'total'=>Cart::total(),'address'=>Input::get('address'),'tel'=>Input::get('tel'),'email'=>Input::get('email'),'name'=>Input::get('name'),'type'=>Input::get('zipcode'),'status'=>'尚未處理']);
        $data=Cart::content();
        foreach ($data as $key => $value) {
            OrderDetail::create(['order_id'=>$Order->id,'file1'=>'images/product-'.$value->id.'.png','specification'=>$value->options->ext,'price'=>$value->price
                ,'amount'=>$value->qty]);
        }
        //$OrderDetail=OrderDetail::where('order_id',Input::get('id'))->get();
        return redirect('cars_done');

    }
    public function cars_done()
    {
        Cart::destroy();
        $data['no']=Order::orderby('id','decc')->first()->no;
        return view('reception.cars_done',['data'=>$data]);
    }

    public function save_image()
    {
        $base64_str = substr($_POST['base64_image'], strpos($_POST['base64_image'], ",")+1);

        //decode base64 string
        $decoded = base64_decode($base64_str);

        $png_url = "upload/images/product-".Session::get('key').".png";
        //create png from decoded base 64 string and save the image in the parent folder
        $result = file_put_contents($png_url, $decoded);

        //send result - the url of the png or 0
        header('Content-Type: application/json');
        if($result) {
            $png_url = $this->get_folder_url().$png_url;
            echo json_encode(1);
        }
        else {
            echo json_encode(0);
        }

    }


}
