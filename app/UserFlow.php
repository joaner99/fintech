<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFlow extends Model
{
    protected $connection = 'mysql_admin_for_letsgo';
    protected $table = 'users_used_flow';
}